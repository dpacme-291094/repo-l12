<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<display:table name="offers" id="row" requestURI="${uri}" pagesize="5"
	class="displaytag">

	<display:column titleKey="offer.room" sortable="false"
		property="room.title">
	</display:column>
	<display:column titleKey="offer.date" sortable="true"
		property="dateRequest">
	</display:column>

	<display:column titleKey="offer.time" sortable="true" property="time">
	</display:column>

	<display:column titleKey="offer.status" sortable="true">

		<jstl:if test="${row.status == 'OCCUPIED' }">
			<spring:message code="offer.occupied" />
		</jstl:if>
		<jstl:if test="${row.status == 'FREE' }">
			<spring:message code="offer.free" />
		</jstl:if>
	</display:column>

	<display:column>

		<jstl:if test="${row.dateRequest>date}">

			<form:form method="POST" action="${uri}">
				<input type="hidden" name="offerId" value="${row.id}" />
				<input type="submit" name="cancel"
					value="<spring:message code="offer.cancel" />" />&nbsp;
			</form:form>

		</jstl:if>

	</display:column>

</display:table>
<security:authorize access="hasRole('SECRETARY')">
	<a href="offer/secretary/create.do"><spring:message
			code="offer.create" /></a>
</security:authorize>
