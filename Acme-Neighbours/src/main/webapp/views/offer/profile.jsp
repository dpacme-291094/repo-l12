<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<table id="formus">


	<tr>
		<td><b><spring:message code="offer.date" /></b> <br /> <jstl:out
				value="${offer.dateRequest}" /></td>
	</tr>

	<tr>
		<td><b><spring:message code="offer.time" /></b> <br /> <jstl:out
				value="${offer.time}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="offer.status" /></b> <br /> <jstl:if
				test="${offer.status == 'OCCUPIED' }">
				<spring:message code="offer.occupied" />
			</jstl:if> <jstl:if test="${offer.status == 'FREE' }">
				<spring:message code="offer.free" />
			</jstl:if></td>
	</tr>
	<tr>
		<td><jstl:if
				test="${offer.dateRequest>currentDate && isRegistered==true && isDenied==false}">
				<form:form method="POST" action="offer/associate/profile.do">
					<input type="hidden" name="offerId" value="${offer.id}" />
					<input type="submit" name="unregister"
						value="<spring:message code="offer.unregister" />" />&nbsp;
			</form:form>
			</jstl:if> <jstl:if
				test="${offer.dateRequest>currentDate && offer.status =='FREE' && isRegistered==false}">
				<form:form method="POST" action="offer/associate/profile.do">
					<input type="hidden" name="offerId" value="${offer.id}" />
					<input type="submit" name="register"
						value="<spring:message code="offer.register" />" />&nbsp;
			</form:form>
			</jstl:if></td>
	</tr>

</table>
