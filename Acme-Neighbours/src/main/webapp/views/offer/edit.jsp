<%--
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="offer/secretary/create.do" modelAttribute="offer">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="secretary" />
	<form:hidden path="petitions" />
	<form:hidden path="status" />
	<table id="formus">

		<acme:textbox code="offer.date" path="dateRequest" />
		<acme:textbox code="offer.time" path="time" />

		<tr>
			<td><b><spring:message code="offer.room"></spring:message></b></td>
			<td><form:select id="roomDropdown" path="room">

					<jstl:forEach items="${rooms}" var="list">
						<option
							<jstl:if test="${row.room.id==list.id}">selected="selected"</jstl:if>
							value="${list.id}">${list.title}</option>
					</jstl:forEach>
				</form:select></td>
		</tr>
	</table>

	<acme:submit name="save" code="offer.save" />
	<acme:cancel url="offer/secretary/list.do" code="offer.cancel" />
</form:form>



