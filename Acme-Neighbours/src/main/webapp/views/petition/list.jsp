<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<display:table name="petitions" id="row" requestURI="${uri}"
	pagesize="5" class="displaytag">


	<security:authorize access="hasRole('SECRETARY')">
		<display:column titleKey="petition.asso" sortable="false">


			<a href="associate/actor/profile.do?associateId=${row.associate.id}"><jstl:out
					value="${row.associate.name}"></jstl:out> </a>
		</display:column>
	</security:authorize>


	<display:column titleKey="room.title" sortable="false"
		property="offer.room.title">

	</display:column>

	<display:column titleKey="offer.date" sortable="true"
		property="offer.dateRequest">
	</display:column>

	<display:column titleKey="offer.time" sortable="true"
		property="offer.time">
	</display:column>

	<display:column titleKey="petition.description" property="description"
		sortable="false">

	</display:column>

	<display:column>
		<security:authorize access="hasRole('SECRETARY')">
			<jstl:if test="${row.status == 'PENDING' }">

				<form:form method="POST" action="petition/secretary/list.do">
					<input type="hidden" name="petitionId" value="${row.id}" />
					<input type="submit" name="accept"
						value="<spring:message code="petition.accept" />" />&nbsp;
		</form:form>

				<form:form method="POST" action="petition/secretary/list.do">
					<input type="hidden" name="petitionId" value="${row.id}" />
					<input type="submit" name="deny"
						value="<spring:message code="petition.deny" />" />&nbsp;
		</form:form>
			</jstl:if>

			<jstl:if test="${row.status == 'ACCEPTED' }">
				<spring:message code="petition.accepted" />
			</jstl:if>
			<jstl:if test="${row.status == 'DENIED' }">
				<spring:message code="petition.denied" />
			</jstl:if>
		</security:authorize>

		<security:authorize access="hasRole('ASSOCIATE')">

			<jstl:if test="${row.status == 'ACCEPTED' }">
				<spring:message code="petition.accepted" />
			</jstl:if>
			<jstl:if test="${row.status == 'DENIED' }">
				<spring:message code="petition.denied" />
			</jstl:if>
			<jstl:if test="${row.status == 'PENDING' }">
				<spring:message code="petition.pending" />
			</jstl:if>


		</security:authorize>
	</display:column>

	<security:authorize access="hasRole('ASSOCIATE')">
		<display:column>
			<jstl:if
				test="${row.offer.dateRequest>currentDate && (row.status == 'PENDING' || row.status == 'ACCEPTED')}">

				<form:form method="POST" action="offer/associate/profile.do">
					<input type="hidden" name="offerId" value="${row.offer.id}" />
					<input type="submit" name="unregister"
						value="<spring:message code="offer.unregister" />" />&nbsp;
			</form:form>

			</jstl:if>
		</display:column>
	</security:authorize>




</display:table>