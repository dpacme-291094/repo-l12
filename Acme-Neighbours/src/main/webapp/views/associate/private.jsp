<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="associate/private.do" modelAttribute="associate">


	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="userAccount.Authorities" />
	<form:hidden path="messagesIncoming" />
	<form:hidden path="messagesOutgoing" />
	<form:hidden path="petitions" />
	<form:hidden path="requests" />
	<form:hidden path="comments" />
	<form:hidden path="lastPayment" />
	<form:hidden path="creditCard.holderName" />
	<form:hidden path="creditCard.brandName" />
	<form:hidden path="creditCard.number" />
	<form:hidden path="creditCard.expirationMonth" />
	<form:hidden path="creditCard.expirationYear" />
	<form:hidden path="creditCard.codeCVV" />
	<table id="formus">
		<tr>
			<td><u>
					<h3>
						<spring:message code="associate.personal" />
					</h3>
			</u></td>
		</tr>
		<acme:textbox code="associate.name" path="name" />
		<acme:textbox code="associate.surname" path="surname" />
		<acme:textbox code="associate.email" path="email" />
		<acme:textbox code="associate.phone" path="phone" />
		<acme:textbox code="associate.DNI" path="DNI" />


	</table>
	<acme:submit name="save" code="associate.save" />

	<acme:cancel url="welcome/index.do" code="associate.cancel" />


</form:form>

<table id="formus">
	<tr>

		<td><u>
				<h3>
					<spring:message code="ass.payment" />
				</h3>
		</u></td>

	</tr>
	<tr>
		<td><spring:message code="ass.yourult2" /> ${fee} Euros.</td>
	</tr>
	<tr>
		<td><spring:message code="ass.yourult" /> ${months} <spring:message
				code="ass.yourult1" />.</td>
	</tr>
	<tr>
		<td><spring:message code="ass.topay" /> <jstl:out
				value="${associate.totalFee}">
			</jstl:out> Euros.</td>
	</tr>

	<jstl:if test="${associate.totalFee>0}">
		<tr>
			<td><br /> <form:form method="POST"
					action="associate/private.do">
					<input type="hidden" name="associateId" value="${associate.id}" />
					<input type="submit" name="pay"
						value="<spring:message code="ass.pay" />"
						onclick="return confirm('<spring:message code="activity.confirm.pay" />${associate.totalFee} Euros. <spring:message code="activity.confirm" />')" />&nbsp;
			</form:form></td>
		</tr>
	</jstl:if>
</table>
<form:form method="POST" action="associate/creditCard.do">
	<table id="formus">
		<tr>
			<td><u>
					<h3>
						<spring:message code="cc" />

					</h3>
			</u></td>
		</tr>



		<tr>
			<td><b><spring:message code="cc.holderName" /> : </b> <jstl:out
					value="${associate.creditCard.holderName}" /></td>
		</tr>
		<tr>
			<td><b><spring:message code="cc.brandName" /> : </b> <jstl:out
					value="${associate.creditCard.brandName}" /></td>
		</tr>
		<tr>
			<td><b><spring:message code="cc.expirationYear" /> : </b> <jstl:out
					value="${associate.creditCard.expirationYear}" /></td>
		</tr>
		<tr>
			<td><b><spring:message code="cc.expirationMonth" /> : </b> <jstl:out
					value="${associate.creditCard.expirationMonth}" /></td>
		</tr>
		<tr>
			<td><b><spring:message code="cc.number" /> : </b> <jstl:out
					value="${cc}" /></td>
		</tr>
		<tr>
			<td><b><spring:message code="cc.codecvv" /> : </b> <jstl:out
					value="${associate.creditCard.codeCVV}" /></td>
		</tr>

	</table>
	<td><input type="submit" name="edit2"
		value="<spring:message code="associate.newcc" />" />&nbsp;
</form:form>