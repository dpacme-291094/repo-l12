<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<h2>
	<spring:message code="actor.rol.president" />
</h2>
<display:table name="president" id="row" requestURI="${uri}"
	pagesize="5" class="displaytag">

	<display:column property="name" titleKey="associate.name"
		sortable="true" />
	<display:column property="surname" titleKey="associate.surname"
		sortable="false" />
	<display:column property="email" titleKey="associate.email"
		sortable="true" />
	<display:column property="phone" titleKey="associate.phone"
		sortable="false" />
	<display:column>
		<form:form method="POST" action="message/actor/create.do">
			<input type="hidden" name="id" value="${row.id}" />

			<input type="submit" name="create"
				value="<spring:message code="associate.message.create" />" />
		</form:form>
	</display:column>
</display:table>


<h2>
	<spring:message code="actor.rol.secretaries" />
</h2>
<display:table name="secretaries" id="row" requestURI="${uri}"
	pagesize="5" class="displaytag">

	<display:column property="name" titleKey="associate.name"
		sortable="true" />
	<display:column property="surname" titleKey="associate.surname"
		sortable="false" />
	<display:column property="email" titleKey="associate.email"
		sortable="true" />
	<display:column property="phone" titleKey="associate.phone"
		sortable="false" />
	<display:column>
		<form:form method="POST" action="message/actor/create.do">
			<input type="hidden" name="id" value="${row.id}" />

			<input type="submit" name="create"
				value="<spring:message code="associate.message.create" />" />
		</form:form>
	</display:column>
</display:table>


<h2>
	<spring:message code="actor.rol.treasurers" />
</h2>
<display:table name="treasurers" id="row" requestURI="${uri}"
	pagesize="5" class="displaytag">

	<display:column property="name" titleKey="associate.name"
		sortable="true" />
	<display:column property="surname" titleKey="associate.surname"
		sortable="false" />
	<display:column property="email" titleKey="associate.email"
		sortable="true" />
	<display:column property="phone" titleKey="associate.phone"
		sortable="false" />
	<display:column>
		<form:form method="POST" action="message/actor/create.do">
			<input type="hidden" name="id" value="${row.id}" />

			<input type="submit" name="create"
				value="<spring:message code="associate.message.create" />" />
		</form:form>
	</display:column>
</display:table>
