<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<display:table name="associates" id="row" requestURI="${uri}"
	pagesize="5" class="displaytag">

		

	
	<display:column titleKey="associate.asNumber" property="associateNumber"
		sortable="false">

	</display:column>

	<display:column titleKey="associate.name" sortable="false">
			<a href="associate/actor/profile.do?associateId=${row.id}"><jstl:out
					value="${row.name}" /></a>
		
	</display:column>
	<display:column titleKey="associate.surname" property="surname"
		sortable="false">

	</display:column>
	<display:column titleKey="associate.email" property="email"
		sortable="false">

	</display:column>
	<display:column titleKey="associate.phone" property="phone"
		sortable="false">

	</display:column>
	
	
</display:table>

