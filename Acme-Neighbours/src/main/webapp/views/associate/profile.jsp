<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<table id="formus">
	<tr>
		<td><u>
				<h3>
					<spring:message code="associate.personal" />
				</h3>
		</u></td>
	</tr>

	<tr>
		<td><b><spring:message code="associate.asNumber" /></b> <br />
			<jstl:out value="${associate.associateNumber}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="associate.name" /></b> <br /> <jstl:out
				value="${associate.name}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="associate.surname" /></b> <br /> <jstl:out
				value="${associate.surname}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="associate.email" /></b> <br /> <jstl:out
				value="${associate.email}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="associate.DNI" /></b> <br /> <jstl:out
				value="${associate.DNI}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="associate.phone" /></b> <br /> <jstl:out
				value="${associate.phone}" /></td>
	</tr>

</table>

<form:form method="POST" action="message/actor/create.do">
	<input type="hidden" name="id" value="${associate.id}" />

	<input type="submit" name="create"
		value="<spring:message code="associate.message.create" />" />
</form:form>
