<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<display:table name="suggestions" id="row"
	requestURI="suggestion/treasurer/list.do" pagesize="5"
	class="displaytag">


	<display:column titleKey="suggestion.title" property="title"
		sortable="false">

	</display:column>
	<display:column titleKey="suggestion.description"
		property="description" sortable="false">

	</display:column>

	<display:column property="moment" titleKey="suggestion.moment"
		sortable="true" />


	<display:column>
		<form:form method="POST" action="answer/treasurer/create.do">
			<input type="hidden" name="suggestionId" value="${row.id}" />
			<input type="submit" name="answer"
				value="<spring:message code="suggestion.answer" />" />&nbsp;
		</form:form>
	</display:column>

	<display:column>
		<form:form method="POST" action="suggestion/treasurer/list.do">
			<input type="hidden" name="suggestionId" value="${row.id}" />
			<input type="submit" name="delete"
				value="<spring:message code="suggestion.delete" />" />&nbsp;
		</form:form>
	</display:column>
</display:table>