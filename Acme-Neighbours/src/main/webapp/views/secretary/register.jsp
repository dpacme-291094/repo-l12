<%--
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="secretary/president/register.do" modelAttribute="formSecretary">


	<form:hidden path="userAccount.Authorities" />
	
	

	<table id="formus">
		<tr>
			<td><u>
					<h3>
						<spring:message code="secretary.personal" />
					</h3>
			</u></td>
		</tr>
		<acme:textbox code="secretary.name" path="name" />
		<acme:textbox code="secretary.surname" path="surname" />
		<acme:textbox code="secretary.email" path="email" />
		<acme:textbox code="secretary.phone" path="phone" />
		<acme:textbox code="secretary.DNI" path="DNI" />
		
		
		<tr>
			<td><u>
					<h3>
						<spring:message code="secretary.userac" />
					</h3>
			</u></td>
		</tr>
		<acme:textbox code="secretary.userAccount" path="userAccount.username" />

		<acme:password code="secretary.password" path="userAccount.password" />
		<tr>
			<td><b><spring:message code="secretary.repassword" /></b></td>
			<td><input type="password" name="repassword" /></td>
		</tr>


	</table>

	<input type="checkbox" name="terms">

	<spring:message code="secretary.terms" />
	<br />
	<br />

	<acme:submit name="save" code="secretary.register" />
	<acme:cancel url="welcome/index.do" code="secretary.cancel" />
</form:form>



