<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="volunteer/private.do" modelAttribute="volunteer">


	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="userAccount.Authorities" />
	<form:hidden path="messagesIncoming" />
	<form:hidden path="messagesOutgoing" />
	<form:hidden path="courses" />
	
	<table id="formus">
		<tr>
			<td><u>
					<h3>
						<spring:message code="volunteer.personal" />
					</h3>
			</u></td>
		</tr>
		<acme:textbox code="volunteer.name" path="name" />
		<acme:textbox code="volunteer.surname" path="surname" />
		<acme:textbox code="volunteer.email" path="email" />
		<acme:textbox code="volunteer.phone" path="phone" />
		<acme:textbox code="volunteer.DNI" path="DNI" />


	</table>
	<acme:submit name="save" code="volunteer.save" />

	<acme:cancel url="welcome/index.do" code="volunteer.cancel" />

</form:form>
