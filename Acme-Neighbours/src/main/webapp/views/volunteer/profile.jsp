<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<table id="formus">
	<tr>
		<td><u>
				<h3>
					<spring:message code="volunteer.personal" />
				</h3>
		</u></td>
	</tr>


	<tr>
		<td><b><spring:message code="volunteer.name" /></b> <br /> <jstl:out
				value="${volunteer.name}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="volunteer.surname" /></b> <br /> <jstl:out
				value="${volunteer.surname}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="volunteer.email" /></b> <br /> <jstl:out
				value="${volunteer.email}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="volunteer.DNI" /></b> <br /> <jstl:out
				value="${volunteer.DNI}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="volunteer.phone" /></b> <br /> <jstl:out
				value="${volunteer.phone}" /></td>
	</tr>

</table>

<form:form method="POST" action="message/actor/create.do">
	<input type="hidden" name="id" value="${volunteer.id}" />

	<input type="submit" name="create"
		value="<spring:message code="volunteer.message.create" />" />
</form:form>
