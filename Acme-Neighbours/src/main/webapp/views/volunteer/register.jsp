<%--
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="volunteer/register.do" modelAttribute="formVolunteer">

	<form:hidden path="userAccount.Authorities" />
	

	<table id="formus">
		<tr>
			<td><u>
					<h3>
						<spring:message code="volunteer.personal" />
					</h3>
			</u></td>
		</tr>
		<acme:textbox code="volunteer.name" path="name" />
		<acme:textbox code="volunteer.surname" path="surname" />
		<acme:textbox code="volunteer.email" path="email" />
		<acme:textbox code="volunteer.phone" path="phone" />
		<acme:textbox code="volunteer.DNI" path="DNI" />
		
		
		<tr>
			<td><u>
					<h3>
						<spring:message code="volunteer.userac" />
					</h3>
			</u></td>
		</tr>
		<acme:textbox code="volunteer.userAccount" path="userAccount.username" />

		<acme:password code="volunteer.password" path="userAccount.password" />
		<tr>
			<td><b><spring:message code="volunteer.repassword" /></b></td>
			<td><input type="password" name="repassword" /></td>
		</tr>


	</table>

	<input type="checkbox" name="terms">

	<spring:message code="volunteer.terms" />
	<br />
	<br />

	<acme:submit name="save" code="volunteer.register" />
	<acme:cancel url="welcome/index.do" code="volunteer.cancel" />
</form:form>



