<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<table id="formus">


	<tr>
		<td><img src="${room.picture}" height="200" width="150"></td>
	</tr>
	<tr>
		<td></td>
	</tr>

	<tr>
		<td><b><spring:message code="room.title" /></b> <br /> <jstl:out
				value="${room.title}" /></td>
	</tr>

	<tr>
		<td><b><spring:message code="room.description" /></b> <br /> <jstl:out
				value="${room.description}" /></td>
	</tr>

</table>


<table id="formus">
	<tr>
		<td><u>
				<h3>
					<spring:message code="offer.personal" />
				</h3>
		</u></td>
	</tr>
</table>
<display:table name="offers" id="row" requestURI="${uri}" pagesize="5"
	class="displaytag">

	<jstl:choose>
		<jstl:when
			test="${row.dateRequest<currentDate || row.status=='OCCUPIED'}">

			<display:column class="greyout" titleKey="offer.date"
				sortable="false" property="dateRequest">
			</display:column>

			<display:column class="greyout" titleKey="offer.time" sortable="true"
				property="time">
			</display:column>

			<display:column class="greyout" titleKey="offer.status"
				sortable="true">

				<jstl:if test="${row.status == 'OCCUPIED' }">
					<spring:message code="offer.occupied" />
				</jstl:if>
				<jstl:if test="${row.status == 'FREE' }">
					<spring:message code="offer.free" />
				</jstl:if>
			</display:column>

			<display:column class="greyout">

				<a href="offer/associate/profile.do?offerId=${row.id}"><spring:message
						code="offer.profile"></spring:message> </a>

			</display:column>
		</jstl:when>
		<jstl:otherwise>
			<display:column class="highlight" titleKey="offer.date"
				sortable="false" property="dateRequest">
			</display:column>

			<display:column class="highlight" titleKey="offer.time"
				sortable="true" property="time">
			</display:column>

			<display:column class="highlight" titleKey="offer.status"
				sortable="true">

				<jstl:if test="${row.status == 'OCCUPIED' }">
					<spring:message code="offer.occupied" />
				</jstl:if>
				<jstl:if test="${row.status == 'FREE' }">
					<spring:message code="offer.free" />
				</jstl:if>
			</display:column>

			<display:column class="highlight">

				<a href="offer/associate/profile.do?offerId=${row.id}"><spring:message
						code="offer.profile"></spring:message> </a>

			</display:column>

		</jstl:otherwise>
	</jstl:choose>

</display:table>