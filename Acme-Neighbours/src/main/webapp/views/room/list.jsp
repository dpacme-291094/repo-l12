<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<display:table name="rooms" id="row" requestURI="${uri}" pagesize="5"
	class="displaytag">



	<display:column titleKey="room.picture" sortable="false">
		<img src="${row.picture}" width="100" height="120" />
	</display:column>

	<display:column titleKey="room.title" sortable="false">
		<security:authorize access="hasRole('ASSOCIATE')">
			<a href="room/associate/profile.do?roomId=${row.id}"><jstl:out
					value="${row.title}" /></a>
		</security:authorize>
		<security:authorize access="hasRole('SECRETARY')">
			<jstl:out value="${row.title}" />
		</security:authorize>
	</display:column>
	<display:column titleKey="room.description" property="description"
		sortable="false">

	</display:column>

	<security:authorize access="hasRole('SECRETARY')">

		<display:column>
			<form:form method="POST" action="room/secretary/edit.do">
				<input type="hidden" name="roomId" value="${row.id}" />
				<input type="submit" name="edit"
					value="<spring:message code="room.edit" />" />&nbsp;
		</form:form>
		</display:column>


	</security:authorize>


</display:table>
<security:authorize access="hasRole('SECRETARY')">
	<a href="offer/secretary/create.do"><spring:message
			code="offer.create" /></a>
</security:authorize>
