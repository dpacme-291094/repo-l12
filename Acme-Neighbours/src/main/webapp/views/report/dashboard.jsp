<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- C Queries -->



<!-- A Queries: Treasurer -->

<jstl:if test="${esTesorero==1}">

	<h2>
		<spring:message code="report.A1" />
	</h2>
	<table>
		<tr>
			<th><spring:message code="report.A11" />:</th>
			<th><spring:message code="report.A12" />:</th>
		</tr>
		<tr>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${QueryA11}" /> Euros</td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${QueryA12}" /> Euros</td>
		</tr>

	</table>

	<h2>
		<spring:message code="report.A2" />
	</h2>
	<table>
		<tr>
			<th><spring:message code="report.min" />:</th>
			<th><spring:message code="report.max" />:</th>
			<th><spring:message code="report.avg" />:</th>
		</tr>

		<tr>
			<jstl:forEach items="${QueryA2}" var="c">
				<td><jstl:out value="${c[0]}" /> Euros</td>
				<td><jstl:out value="${c[1]}" /> Euros</td>
				<td><jstl:out value="${c[2]}" /> Euros</td>
			</jstl:forEach>
		</tr>


	</table>

	<h2>
		<spring:message code="report.A3" />
	</h2>
	<table>
		<tr>
			<th><spring:message code="report.nombre" />:</th>
			<th><spring:message code="report.apellido" />:</th>
			<th><spring:message code="report.max" />:</th>
		</tr>

		<tr>
			<jstl:forEach items="${QueryA3}" var="c">
				<td><jstl:out value="${c[0]}" /></td>
				<td><jstl:out value="${c[1]}" /></td>
				<td><jstl:out value="${c[2]}" /> Euros</td>
			</jstl:forEach>
		</tr>


	</table>

	<h2>
		<spring:message code="report.A4" />
	</h2>
	<table>
		<tr>
			<th><spring:message code="report.notans" />:</th>
			<th><spring:message code="report.ans" />:</th>
		</tr>

		<tr>


			<td><jstl:out value="${QueryA42}" /></td>
			<td><jstl:out value="${QueryA41}" /></td>
		</tr>


	</table>


	<h2>
		<spring:message code="report.A5" />
	</h2>
	<table>
		<tr>
			<th><spring:message code="report.nombre" />:</th>
			<th><spring:message code="report.amount" />:</th>
		</tr>



		<jstl:forEach items="${QueryA5}" var="c">
			<tr>
				<td><jstl:out value="${c[0].name}" /></td>
				<td><jstl:out value="${c[1]}" /> Euros</td>
			</tr>

		</jstl:forEach>



	</table>


</jstl:if>