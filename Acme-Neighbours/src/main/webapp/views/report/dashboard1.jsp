<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- C Queries -->

<h2>
	<spring:message code="report.ar" />
	:
</h2>

<table>
		<tr>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${ar}" /></td>
		</tr>
</table>


<h2>
	<spring:message code="report.mamir" />
	:
</h2>

<table>
	<tr>
		<th></th>
		<th><spring:message code="report.associate" />:</th>
		<th><spring:message code="report.reunion" />:</th>
	</tr>

		<tr>
			<td><spring:message code="report.max" /></td>
			<td><jstl:out value="${mar[0].name}" /> <jstl:out value="${mar[0].surname}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${mar[1]}" /></td>
		</tr>
		<tr>
			<td><spring:message code="report.min" /></td>
			<td><jstl:out value="${mir[0].name}" /> <jstl:out value="${mir[0].surname}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${mir[1]}" /></td>
		</tr>

</table>

<h2>
	<spring:message code="report.mamil" />
	:
</h2>

<table>
	<tr>
		<th></th>
		<th><spring:message code="report.associate" />:</th>
		<th><spring:message code="report.reunion" />:</th>
	</tr>

		<tr>
			<td><spring:message code="report.max" /></td>
			<td><jstl:out value="${mal[0].name}" /> <jstl:out value="${mal[0].surname}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${mal[1]}" /></td>
		</tr>
		<tr>
			<td><spring:message code="report.min" /></td>
			<td><jstl:out value="${mil[0].name}" /> <jstl:out value="${mil[0].surname}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${mil[1]}" /></td>
		</tr>

</table>

<h2>
	<spring:message code="report.mamila" />
	:
</h2>

<table>
	<tr>
		<th></th>
		<th><spring:message code="report.leisure" />:</th>
		<th><spring:message code="report.assistant" />:</th>
	</tr>

		<tr>
			<td><spring:message code="report.max" /></td>
			<td><jstl:out value="${mala[0].title}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${mala[1]}" /></td>
		</tr>
		<tr>
			<td><spring:message code="report.min" /></td>
			<td><jstl:out value="${mila[0].title}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="0"
					value="${mila[1]}" /></td>
		</tr>

</table>
<h2>
	<spring:message code="report.mmar" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.min" />:</th>
		<th><spring:message code="report.max" />:</th>
		<th><spring:message code="report.avg" />:</th>
	</tr>

		<tr>
		<jstl:forEach items="${mmar}" var="mma">
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${mma}" /></td>
			</jstl:forEach>
		</tr>
		

</table>

<h2>
	<spring:message code="report.cpa" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.avg" />:</th>
	</tr>

		<tr>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${cpa}" /></td>
		</tr>
		

</table>

<h2>
	<spring:message code="report.mmaspa" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.min" />:</th>
		<th><spring:message code="report.max" />:</th>
		<th><spring:message code="report.avg" />:</th>
	</tr>

		<tr>
		<jstl:forEach items="${mmaspa}" var="mmas">
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${mmas}" /></td>
			</jstl:forEach>
		</tr>
		

</table>

<h2>
	<spring:message code="report.mmarpa" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.min" />:</th>
		<th><spring:message code="report.max" />:</th>
		<th><spring:message code="report.avg" />:</th>
	</tr>

		<tr>
		<jstl:forEach items="${mmarpa}" var="mmar">
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${mmar}" /></td>
			</jstl:forEach>
		</tr>
		

</table>

<h2>
	<spring:message code="report.vmc" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.name" />:</th>
		<th><spring:message code="report.number" />:</th>
	</tr>

		<tr>
		<jstl:forEach items="${vmc}" var="v">
		<td><jstl:out
					value="${v[0].name}" /> <jstl:out
					value="${v[0].surname}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${v[1]}" /></td>
			</jstl:forEach>
		</tr>
		

</table>

<h2>
	<spring:message code="report.marr" />
	:
</h2>

<table>
	<tr>
	<th></th>
		<th><spring:message code="report.name" />:</th>
		<th><spring:message code="report.number" />:</th>
	</tr>
		<tr>
		<td><spring:message code="report.max" />:</td>
		<jstl:forEach items="${marr}" var="ma">
		<td><jstl:out
					value="${ma[0].name}" /> <jstl:out
					value="${ma[0].surname}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${ma[1]}" /></td>
			</jstl:forEach>
		</tr>
		<tr>
		<td><spring:message code="report.min" />:</td>
		<jstl:forEach items="${mirr}" var="mi">
		<td><jstl:out
					value="${mi[0].name}" /> <jstl:out
					value="${mi[0].surname}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${mi[1]}" /></td>
			</jstl:forEach>
		</tr>
</table>

<h2>
	<spring:message code="report.rac" />
	:
</h2>

<table>
	<tr>
		<th></th>
		<th><spring:message code="report.number" />:</th>
	</tr>
		<tr>
		<td><spring:message code="report.aceptadas" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${rac}" /></td>
		</tr>
		<tr>
		<td><spring:message code="report.denegadas" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${rde}" /></td>
		</tr>
		<tr>
		<td><spring:message code="report.pendientes" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${rpe}" /></td>
		</tr>
</table>

<h2>
	<spring:message code="report.cma" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.name" />:</th>
		<th><spring:message code="report.number" />:</th>
	</tr>

		<tr>
		<jstl:forEach items="${cma}" var="c">
		<td><jstl:out
					value="${c[0]}" /></td>
			<td><fmt:formatNumber type="number" maxFractionDigits="2"
					value="${c[1]}" /></td>
			</jstl:forEach>
		</tr>
		

</table>

<h2>
	<spring:message code="report.raf" />
	:
</h2>

<table>
	<tr>
		<th><spring:message code="report.numPendingAct" />:</th>
		<th><spring:message code="report.numFinalAct" />:</th>
	</tr>

		<tr>
		<td><jstl:out
					value="${rap}" /></td>
			<td><jstl:out
					value="${raf}" /></td>
		</tr>
		

</table>