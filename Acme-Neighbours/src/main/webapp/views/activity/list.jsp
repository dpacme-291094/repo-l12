<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<display:table name="activities" id="row" requestURI="${uri}"
	pagesize="5" class="displaytag">

	<jstl:choose>
		<jstl:when
			test="${row.moment<currentDate || row.seats==0 || row.status=='CANCELLED' || row.status =='CLOSED'}">
			<display:column titleKey="activity.title" sortable="false"
				class="greyout">
				<jstl:if test="${isCourse == true}">
					<a href="course/profile.do?courseId=${row.id}"><jstl:out
							value="${row.title}" /></a>
				</jstl:if>
				<jstl:if test="${isLeisure == true}">
					<a href="leisure/profile.do?leisureId=${row.id}"><jstl:out
							value="${row.title}" /></a>
				</jstl:if>
				<jstl:if test="${isReunion == true}">
					<a href="reunion/profile.do?reunionId=${row.id}"><jstl:out
							value="${row.title}" /></a>
				</jstl:if>
			</display:column>
			<display:column class="greyout" titleKey="activity.description"
				property="description" sortable="false">

			</display:column>
			<display:column class="greyout" titleKey="activity.moment"
				property="moment" sortable="true">

			</display:column>
			<display:column class="greyout" titleKey="activity.status"
				sortable="true">

				<jstl:if test="${row.status == 'OPEN' }">
					<spring:message code="activity.open" />
				</jstl:if>
				<jstl:if test="${row.status == 'CLOSED' }">
					<spring:message code="activity.closed" />
				</jstl:if>
				<jstl:if test="${row.status == 'CANCELLED' }">
					<spring:message code="activity.cancelled" />
				</jstl:if>

			</display:column>
			<display:column class="greyout" titleKey="activity.seats"
				property="seats" sortable="false">

			</display:column>
			<security:authorize
				access="isAuthenticated() && !hasRole('VOLUNTEER')">
				<jstl:if test="${isCourse == true}">
					<display:column class="greyout" titleKey="activity.volunteer"
						sortable="false">
						<a
							href="volunteer/actor/profile.do?volunteerId=${row.volunteer.id}"><jstl:out
								value="${row.volunteer.name}" /></a>

					</display:column>


				</jstl:if>
			</security:authorize>
			<security:authorize
				access="hasRole('PRESIDENT') || hasRole('VOLUNTEER')">

				<jstl:if test="${isEditable != true}">
					<display:column class="greyout" titleKey="leisures.modify"
						sortable="false">
						<jstl:if test="${row.status=='OPEN'}">
							<form:form method="POST" action="${uri}">
								<input type="hidden" name="actId" value="${row.id}" />
								<input type="submit" name="edit"
									value="<spring:message code="leisures.modify" />" />&nbsp;
			</form:form>
						</jstl:if>
					</display:column>
				</jstl:if>
			</security:authorize>
			<security:authorize access="hasRole('VOLUNTEER')">
				<display:column class="greyout" titleKey="leisures.delete"
					sortable="false">
					<jstl:if test="${row.status=='CANCELLED'}">
						<form:form method="POST" action="${uri}">
							<input type="hidden" name="actId" value="${row.id}" />
							<input type="submit" name="delete"
								value="<spring:message code="leisures.delete" />" />&nbsp;
			</form:form>
					</jstl:if>
				</display:column>
			</security:authorize>
			<security:authorize access="hasRole('PRESIDENT')">
				<jstl:if test="${isCourse != true }">
					<display:column class="greyout" titleKey="leisures.delete"
						sortable="false">
						<jstl:if test="${row.status=='CANCELLED'}">

							<form:form method="POST" action="${uri}">
								<input type="hidden" name="actId" value="${row.id}" />
								<input type="submit" name="delete"
									value="<spring:message code="leisures.delete" />" />&nbsp;
			</form:form>
						</jstl:if>
					</display:column>
				</jstl:if>
				<jstl:if test="${isCourse == true }">
					<display:column class="greyout" titleKey="activity.cancel"
						sortable="false">
						<jstl:if test="${row.status =='OPEN' && row.moment>ispossible}">

							<form:form method="POST" action="${uri}">
								<input type="hidden" name="actId" value="${row.id}" />
								<input type="submit" name="delete"
									value="<spring:message code="activity.cancel" />" />&nbsp;
			</form:form>

						</jstl:if>
					</display:column>
				</jstl:if>
			</security:authorize>
		</jstl:when>
		<jstl:otherwise>
			<display:column titleKey="activity.title" sortable="false"
				class="highlight">
				<jstl:if test="${isCourse == true}">
					<a href="course/profile.do?courseId=${row.id}"><jstl:out
							value="${row.title}" /></a>
				</jstl:if>
				<jstl:if test="${isLeisure == true}">
					<a href="leisure/profile.do?leisureId=${row.id}"><jstl:out
							value="${row.title}" /></a>
				</jstl:if>
				<jstl:if test="${isReunion == true}">
					<a href="reunion/profile.do?reunionId=${row.id}"><jstl:out
							value="${row.title}" /></a>
				</jstl:if>
			</display:column>
			<display:column class="highlight" titleKey="activity.description"
				property="description" sortable="false">

			</display:column>
			<display:column class="highlight" titleKey="activity.moment"
				property="moment" sortable="true">

			</display:column>
			<display:column class="highlight" titleKey="activity.status"
				sortable="true">

				<jstl:if test="${row.status == 'OPEN' }">
					<spring:message code="activity.open" />
				</jstl:if>
				<jstl:if test="${row.status == 'CLOSED' }">
					<spring:message code="activity.closed" />
				</jstl:if>
				<jstl:if test="${row.status == 'CANCELLED' }">
					<spring:message code="activity.cancelled" />
				</jstl:if>

			</display:column>
			<display:column class="highlight" titleKey="activity.seats"
				property="seats" sortable="false">

			</display:column>
			<security:authorize
				access="isAuthenticated() && !hasRole('VOLUNTEER')">
				<jstl:if test="${isCourse == true}">
					<display:column class="highlight" titleKey="activity.volunteer"
						sortable="false">
						<a
							href="volunteer/actor/profile.do?volunteerId=${row.volunteer.id}"><jstl:out
								value="${row.volunteer.name}" /></a>

					</display:column>


				</jstl:if>
			</security:authorize>

			<security:authorize
				access="hasRole('PRESIDENT') || hasRole('VOLUNTEER')">

				<jstl:if test="${isEditable != true}">
					<display:column class="highlight" titleKey="leisures.modify"
						sortable="false">
						<jstl:if test="${row.status=='OPEN'}">
							<form:form method="POST" action="${uri}">
								<input type="hidden" name="actId" value="${row.id}" />
								<input type="submit" name="edit"
									value="<spring:message code="leisures.modify" />" />&nbsp;
			</form:form>
						</jstl:if>
					</display:column>
				</jstl:if>
			</security:authorize>
			<security:authorize access="hasRole('VOLUNTEER')">
				<display:column class="highlight" titleKey="leisures.delete"
					sortable="false">
					<jstl:if test="${row.status=='CANCELLED'}">
						<form:form method="POST" action="${uri}">
							<input type="hidden" name="actId" value="${row.id}" />
							<input type="submit" name="delete"
								value="<spring:message code="leisures.delete" />" />&nbsp;
			</form:form>
					</jstl:if>
				</display:column>
			</security:authorize>
			<security:authorize access="hasRole('PRESIDENT')">
				<jstl:if test="${isCourse != true }">
					<display:column class="highlight" titleKey="leisures.delete"
						sortable="false">
						<jstl:if test="${row.status=='CANCELLED'}">

							<form:form method="POST" action="${uri}">
								<input type="hidden" name="actId" value="${row.id}" />
								<input type="submit" name="delete"
									value="<spring:message code="leisures.delete" />" />&nbsp;
			</form:form>
						</jstl:if>
					</display:column>
				</jstl:if>
				<jstl:if test="${isCourse == true }">
					<display:column class="highlight" titleKey="activity.cancel"
						sortable="false">
						<jstl:if test="${row.status =='OPEN' && row.moment>ispossible}">

							<form:form method="POST" action="${uri}">
								<input type="hidden" name="actId" value="${row.id}" />
								<input type="submit" name="delete"
									value="<spring:message code="activity.cancel" />" />&nbsp;
			</form:form>

						</jstl:if>
					</display:column>
				</jstl:if>
			</security:authorize>
		</jstl:otherwise>
	</jstl:choose>



</display:table>
<security:authorize access="hasRole('PRESIDENT')">
	<jstl:if test="${isLeisure==true}">
		<a href="leisure/president/edit.do"> <spring:message
				code="leisure.create" />
		</a>
	</jstl:if>
	<jstl:if test="${isReunion==true}">
		<a href="reunion/president/edit.do"> <spring:message
				code="reunion.create" />
		</a>
	</jstl:if>
</security:authorize>
<security:authorize access="hasRole('VOLUNTEER') ">
	<jstl:if test="${isCourse==true}">
		<a href="course/volunteer/edit.do"> <spring:message
				code="course.create" />
		</a>
	</jstl:if>
</security:authorize>

