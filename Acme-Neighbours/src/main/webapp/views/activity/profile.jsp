<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<table id="formus">

	<jstl:if test="${isLeisure==true || isCourse==true}">

		<tr>
			<td><img src="${activity.image}" height="200" width="150"></td>
		</tr>
	</jstl:if>

	<tr>
		<td><b><spring:message code="activity.title" /></b> <br /> <jstl:out
				value="${activity.title}" /></td>
	</tr>

	<tr>
		<td><b><spring:message code="activity.description" /></b> <br />
			<jstl:out value="${activity.description}" /></td>
	</tr>

	<tr>
		<td><b><spring:message code="activity.moment" /></b> <br /> <jstl:out
				value="${activity.moment}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="activity.totalDuration" /></b> <br />
			<jstl:out value="${activity.totalDuration}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="activity.seats" /></b> <br /> <jstl:out
				value="${activity.seats}" /></td>


	</tr>
	<jstl:if test="${isLeisure==true}">

		<tr>
			<td><b><spring:message code="activity.price" /></b> <br /> <jstl:out
					value="${activity.price} Euros" /></td>
		</tr>
	</jstl:if>

	<jstl:if test="${isCourse==true}">
		<security:authorize access="hasRole('VOLUNTEER')">
			<tr>
				<td><b><a
						href="associate/volunteer/courseList.do?courseId=${activity.id}"><spring:message
								code="activity.attendees" /></a></b> <br /></td>
			</tr>


		</security:authorize>
	</jstl:if>
	<jstl:if test="${isLeisure==true}">
		<tr>
			<security:authorize access="hasRole('PRESIDENT')">
				<td><b><a
						href="associate/president/leisureList.do?leisureId=${activity.id}"><spring:message
								code="activity.attendees" /></a></b> <br /></td>
			</security:authorize>
		</tr>

	</jstl:if>

	<jstl:if test="${activity.status=='OPEN' && activity.moment > fecha }">

		<jstl:if test="${activity.status=='OPEN'}">
			<tr>
				<td><br /> <security:authorize access="hasRole('ASSOCIATE')">

						<jstl:if test="${hasRequest==true && activity.seats>0}">
							<form:form method="POST" action="activity/associate/profile.do">
								<input type="hidden" name="activityId" value="${activity.id}" />
								<jstl:choose>
									<jstl:when test="${isLeisure==true}">
										<input type="submit" name="register"
											value="<spring:message code="activity.register" />"
											onclick="return confirm('<spring:message code="activity.confirm.pay" />${activity.price} Euro(s) <spring:message code="activity.perseat" /> <spring:message code="activity.confirm" />')" />
									</jstl:when>
									<jstl:otherwise>
										<input type="submit" name="register"
											value="<spring:message code="activity.register" />" />

									</jstl:otherwise>
								</jstl:choose>
							</form:form>
						</jstl:if>
						<jstl:if test="${hasRequest==false && requestStatus !='DENIED'}">

							<form:form method="POST" action="activity/associate/profile.do">
								<input type="hidden" name="activityId" value="${activity.id}" />

								<input type="submit" name="unregister"
									value="<spring:message code="activity.unregister" />" />
							</form:form>

						</jstl:if>
					</security:authorize>
			</tr>
		</jstl:if>
	</jstl:if>

	<jstl:if test="${isReunion==true}">


		<security:authorize access="hasRole('PRESIDENT')">
			<tr>
				<td><b><a
						href="associate/president/reunionList.do?reunionId=${activity.id}"><spring:message
								code="activity.attendees" /></a></b> <br /></td>
			</tr>
		</security:authorize>
		<jstl:if test="${activity.status=='CLOSED'}">

			<security:authorize access="hasRole('SECRETARY')">

				<jstl:if test="${activity.record==null}">
					<tr>
						<td><br /> <form:form method="POST"
								action="reunion/secretary/profile.do">
								<input type="hidden" name="reunionId" value="${activity.id}" />

								<input type="submit" name="register"
									value="<spring:message code="record.create" />" />
							</form:form></td>
					</tr>
				</jstl:if>
			</security:authorize>
			<security:authorize access="isAuthenticated()">
				<jstl:if test="${activity.record!=null}">

					<tr>
						<td><jstl:if test="${activity.record.isFinal == false}">

								<form:form method="POST" action="record/secretary/edit.do">
									<input type="hidden" name="recordId"
										value="${activity.record.id}" />
									<input type="submit" name="edit"
										value="<spring:message code="record.edit" />" />&nbsp;
							</form:form>
							</jstl:if></td>
					</tr>
				</jstl:if>
			</security:authorize>

			<security:authorize
				access="hasRole('ASSOCIATE') || hasRole('SECRETARY') || hasRole('PRESIDENT') || hasRole('TREASURER') || hasRole('VOLUNTEER')">
				<jstl:if test="${activity.record.isFinal == true}">
					<tr>
						<td><br /> <form:form method="POST" action="record/view.do">
								<input type="hidden" name="recordId"
									value="${activity.record.id}" />

								<input type="submit" name="view"
									value="<spring:message code="record.see" />" />
							</form:form></td>
					</tr>
				</jstl:if>
			</security:authorize>


		</jstl:if>

	</jstl:if>

	<jstl:forEach items="${activity.comments}" var="comment">
		<table>
			<tr>
				<td><b><spring:message code="comment.moment" /> </b>:&nbsp; <jstl:out
						value="${comment.moment}" /></td>
			</tr>
			<tr>
				<td><b><spring:message code="comment.title" /> </b>:&nbsp; <jstl:out
						value="${comment.title}" /></td>
			</tr>
			<tr>
				<td><b><spring:message code="comment.description" /> </b>:&nbsp;
					<jstl:out value="${comment.description}" /></td>
			</tr>
			<security:authorize
				access="hasRole('ASSOCIATE') || hasRole('PRESIDENT')">
				<tr>
					<td><jstl:if
							test="${comment.associate.id == associateId || isPresident==true}">
							<form:form method="POST" action="comment/actor/delete.do">
								<input type="hidden" name="commentId" value="${comment.id}" />
								<input type="submit" name="delete"
									value="<spring:message code="comment.delete" />" />&nbsp;
				
					
						</form:form>
						</jstl:if></td>
				</tr>
			</security:authorize>

		</table>
	</jstl:forEach>

	<security:authorize access="hasRole('ASSOCIATE')">
		<jstl:if
			test="${activity.status == 'CLOSED' && requestStatus=='ACCEPTED'}">
			<tr>
				<td><br /> <form:form method="POST"
						action="activity/associate/profile.do">
						<input type="hidden" name="activityId" value="${activity.id}" />

						<input type="submit" name="createComment"
							value="<spring:message code="comment.create" />" />
					</form:form></td>
			</tr>

		</jstl:if>
	</security:authorize>

</table>





