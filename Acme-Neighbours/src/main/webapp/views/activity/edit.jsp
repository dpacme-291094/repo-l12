<%--
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="${uri}" modelAttribute="${modelAttribute}">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="comments" />
	<form:hidden path="requests" />
	<input type="hidden" name="isPassed" value="${isPassed}" />
	<table id="formus">
		<jstl:if test="${isPassed==false}">

			<tr>
				<acme:textbox code="leisures.title" path="title" />
			</tr>
			<tr>
				<acme:textarea code="leisures.description" path="description" />
			</tr>
			<tr>
				<acme:textbox code="leisures.moment" path="moment" />
			</tr>

			<tr>
				<acme:textbox code="leisures.totalDuration" path="totalDuration" />
			</tr>
			<tr>
				<acme:textbox code="leisures.seats" path="seats" />
			</tr>
			<jstl:if test="${new1==true }">
				<form:hidden path="status" />
			</jstl:if>
			<jstl:if test="${new1==false }">
				<tr>
					<acme:selectModified code="leisures.status" path="status"
						items="${estado}" id="status" />
				</tr>
			</jstl:if>


			<jstl:if test="${isLeasure==true }">
				<tr>
					<acme:textbox code="leisures.images" path="image" />
				</tr>
				<tr>
					<acme:textbox code="leisures.price" path="price" />
				</tr>
			</jstl:if>
			<jstl:if test="${isCourse==true }">
				<form:hidden path="volunteer" />
				<tr>
					<acme:textbox code="leisures.images" path="image" />
				</tr>
			</jstl:if>
		</jstl:if>

		<jstl:if test="${isPassed==true }">

			<form:hidden path="title" />
			<form:hidden path="description" />
			<form:hidden path="moment" />
			<form:hidden path="seats" />
			<form:hidden path="totalDuration" />

			<jstl:if test="${isCourse==true }">
				<form:hidden path="volunteer" />
				<form:hidden path="image" />
			</jstl:if>
			<tr>
				<acme:selectModified code="leisures.status" path="status"
					items="${estado}" id="status" />
			</tr>



			<jstl:if test="${isLeasure==true }">
				<form:hidden path="image" />
				<form:hidden path="price" />
			</jstl:if>
		</jstl:if>




	</table>

	<acme:submit name="save" code="leisure.save" />
	<acme:cancel url="welcome/index.do" code="leisure.cancel" />
</form:form>



