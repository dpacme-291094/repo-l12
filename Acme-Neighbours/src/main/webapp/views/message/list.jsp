<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="messages" id="row" requestURI="${ruta}" class="displaytag" pagesize="5">
		<display:column>
			<a href="message/actor/read.do?id=${row.id}&l=${whereIAm}"><spring:message code="message.read" /></a>
		</display:column>			
		<display:column titleKey="message.sender">
		${row.emailSender}
		</display:column>
	
	<display:column titleKey="message.recipient">	
		${row.emailRecipient}
		</display:column>
	
	<display:column property="moment" titleKey="message.date" sortable="false" />
	<display:column property="subject" titleKey="message.title" sortable="false" />
	
	<jstl:if test="${whereIAm!=null && whereIAm=='i'}">
	<display:column>
	<form:form method="POST" action="message/actor/send.do">
	<input type="hidden" name="id" value="${row.id}" />
	<input type="hidden" name="whereIAm" value="${whereIAm}" />
	<acme:submit name="reply" code="message.reply" />
	</form:form>
	</display:column>
	</jstl:if>
	
	<jstl:if test="${whereIAm!=null}">
	<display:column>
	<form:form method="POST" action="message/actor/send.do">
	<input type="hidden" name="id" value="${row.id}" />
	<input type="hidden" name="whereIAm" value="${whereIAm}" />
	<acme:submit name="forward" code="message.forward" />
	</form:form>
	</display:column>
	</jstl:if>
	
	<jstl:if test="${whereIAm!=null}">
	<display:column>
	<form:form method="POST" action="message/actor/delete.do">
			<input type="hidden" name="id" value="${row.id}" />
			<input type="hidden" name="whereIAm" value="${whereIAm}" />
			<input type="submit" name="delete"
			value="<spring:message code="message.delete" />"
			onclick="return confirm('<spring:message code="message.confirm.delete" />')" />&nbsp;
	</form:form>
	</display:column>
	</jstl:if>
</display:table>

<div>
	<jstl:if test="${ruta.equals('message/actor/outbox.do')}">
		<a href="message/actor/create.do?l=o"> 
			<spring:message	code="message.create" />
		</a>
	</jstl:if>
	<jstl:if test="${!ruta.equals('message/actor/outbox.do')}">
		<a href="message/actor/create.do"> 
			<spring:message	code="message.create" />
		</a>
	</jstl:if>
		
</div>
