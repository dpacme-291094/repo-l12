<%--
 * edit.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="message/actor/send.do" modelAttribute="formMessage"
	method="POST">
	<jstl:if test="${create != null}">
		<form:hidden path="moment" />
		<form:hidden path="emailSender" />
	</jstl:if>
	<table id="formus">
		<jstl:if test="${create == null}">
			<tr>
				<td><form:label path="emailSender">
						<spring:message code="message.sender" />: </form:label></td>
				<td><jstl:out value="${formMessage.emailSender}" /></td>
			</tr>
		</jstl:if>


		<jstl:if test="${create == null}">
			<tr>
				<td><form:label path="emailRecipient">
						<spring:message code="message.recipient" />: </form:label></td>
				<td><jstl:out value="${formMessage.emailRecipient}" /></td>
			</tr>
		</jstl:if>
		<jstl:if test="${create==true}">
			<acme:textbox code="message.recipient" path="emailRecipient" />
		</jstl:if>

		<tr>
			<td><jstl:if test="${!create}">
					<form:label path="subject">
						<spring:message code="message.title" />:</td>
			<td></form:label> <jstl:out value="${formMessage.subject}" /><br /> </jstl:if> <jstl:if
					test="${create}">
					<acme:textbox code="message.title" path="subject" />
				</jstl:if></td>
		<tr>
			<td><jstl:if test="${!create}">
					<form:label path="text">
						<spring:message code="message.text" />: </td>
			<td></form:label> <jstl:out value="${formMessage.text}" /><br /> </jstl:if> <jstl:if
					test="${create}">
					<acme:textarea code="message.text" path="text" />
				</jstl:if></td>
		</tr>
		<tr>
			<td><jstl:if test="${!create}">
					<tr>
						<td><form:label path="attachments">
								<spring:message code="message.attachments" />:
		</form:label></td>

						<td><jstl:forTokens items="${formMessage.attachments}"
								delims="," var="mySplit">
								<a href='<jstl:out value="${mySplit}"/>'><jstl:out
										value="${mySplit}" /></a>
								<br />
							</jstl:forTokens></td>
					</tr>
				</jstl:if> <jstl:if test="${create}">
					<acme:textbox code="message.attachments" path="attachments" />

				</jstl:if>
	</table>

	<jstl:if test="${create}">
		<acme:submit name="save" code="message.send" />&nbsp; 
	</jstl:if>
	<acme:cancel url='${ruta}' code="message.cancel" />

	<jstl:if test="${create}">
		<br />
		<br />
					
						( <spring:message code="message.note" /> )
						
					
				</jstl:if>
</form:form>