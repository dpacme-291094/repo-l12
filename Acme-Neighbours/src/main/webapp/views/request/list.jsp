<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<display:table name="requests" id="row" requestURI="${uri}" pagesize="5"
	class="displaytag">



	<display:column titleKey="activity.title" sortable="false"
		property="activity.title">

	</display:column>

	<security:authorize access="hasRole('PRESIDENT')">
		<display:column titleKey="request.asso" sortable="false">


			<a href="associate/actor/profile.do?associateId=${row.associate.id}"><jstl:out
					value="${row.associate.name}"></jstl:out> </a>
		</display:column>
	</security:authorize>
	<security:authorize access="hasRole('VOLUNTEER')">
		<display:column titleKey="request.asso" sortable="false">


			<a href="associate/actor/profile.do?associateId=${row.associate.id}"><jstl:out
					value="${row.associate.name}"></jstl:out> </a>
		</display:column>
	</security:authorize>

	<display:column titleKey="request.seats" property="seats"
		sortable="false">

	</display:column>
	<security:authorize access="hasRole('ASSOCIATE')">
		<display:column titleKey="request.status" sortable="true">
			<jstl:if test="${row.status == 'ACCEPTED' }">
				<spring:message code="petition.accepted" />
			</jstl:if>
			<jstl:if test="${row.status == 'DENIED' }">
				<spring:message code="petition.denied" />
			</jstl:if>
			<jstl:if test="${row.status == 'PENDING' }">
				<spring:message code="petition.pending" />
			</jstl:if>
		</display:column>
	</security:authorize>

	<display:column titleKey="request.moment" property="moment"
		sortable="true">

	</display:column>




	<security:authorize access="hasRole('PRESIDENT')">
		<display:column>

			<jstl:if test="${row.status == 'PENDING' }">

				<form:form method="POST" action="${uri}">
					<input type="hidden" name="requestId" value="${row.id}" />
					<input type="submit" name="accept"
						value="<spring:message code="request.accept" />" />&nbsp;
		</form:form>

				<form:form method="POST" action="${uri}">
					<input type="hidden" name="requestId" value="${row.id}" />
					<input type="submit" name="deny"
						value="<spring:message code="request.deny" />" />&nbsp;
		</form:form>
			</jstl:if>

			<jstl:if test="${row.status == 'ACCEPTED' }">
				<spring:message code="request.accepted" />
			</jstl:if>
			<jstl:if test="${row.status == 'DENIED' }">
				<spring:message code="request.denied" />
			</jstl:if>
		</display:column>

	</security:authorize>
	<security:authorize access="hasRole('VOLUNTEER')">
		<display:column>
			<jstl:if test="${row.status == 'PENDING' }">

				<form:form method="POST" action="${uri}">
					<input type="hidden" name="requestId" value="${row.id}" />
					<input type="submit" name="accept"
						value="<spring:message code="request.accept" />" />&nbsp;
		</form:form>

				<form:form method="POST" action="${uri}">
					<input type="hidden" name="requestId" value="${row.id}" />
					<input type="submit" name="deny"
						value="<spring:message code="request.deny" />" />&nbsp;
		</form:form>
			</jstl:if>

			<jstl:if test="${row.status == 'ACCEPTED' }">
				<spring:message code="request.accepted" />
			</jstl:if>
			<jstl:if test="${row.status == 'DENIED' }">
				<spring:message code="request.denied" />
			</jstl:if>



		</display:column>
	</security:authorize>





</display:table>