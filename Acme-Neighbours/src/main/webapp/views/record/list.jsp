<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<display:table name="records" id="row" requestURI="${uri}" pagesize="5"
	class="displaytag">
	<display:column titleKey="record.title" property="title" />
	<display:column titleKey="record.description" property="description" />
	<display:column titleKey="record.reunion" property="reunion.title" />
	<display:column titleKey="record.isFinal">
		<jstl:if test="${row.isFinal == false}">
			<spring:message code="record.no" />
		</jstl:if>
		<jstl:if test="${row.isFinal == true}">
			<spring:message code="record.yes" />
		</jstl:if>
	</display:column>

	<display:column>
		<jstl:if test="${row.isFinal == false}">

			<form:form method="POST" action="record/secretary/edit.do">
				<input type="hidden" name="recordId" value="${row.id}" />
				<input type="submit" name="edit"
					value="<spring:message code="record.edit" />" />&nbsp;
		</form:form>
		</jstl:if>

	</display:column>

	<display:column>
		<jstl:if test="${row.isFinal == false}">
			<form:form method="POST" action="record/secretary/edit.do">
				<input type="hidden" name="recordId" value="${row.id}" />
				<input type="submit" name="delete"
					value="<spring:message code="record.delete" />" />&nbsp;
		</form:form>
		</jstl:if>
	</display:column>

</display:table>

<a href="reunion/list.do"><spring:message code="record.create" /></a>

