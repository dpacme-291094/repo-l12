<%--
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="record/secretary/edit.do" modelAttribute="record">
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="secretary" />
	<form:hidden path="reunion" />
	<form:hidden path="moment" />
	
	<table id="formus">
		<acme:textbox code="record.title" path="title" />
		<acme:textarea code="record.description" path="description" />
		<acme:selectModified code="record.isFinal" path="isFinal"
			items="${itemsFinal}" id="isFinal" />
		
	</table>
	
	<acme:submit name="save" code = "record.save" />
	<acme:cancel url="record/secretary/list.do" code = "record.cancel" />
</form:form>