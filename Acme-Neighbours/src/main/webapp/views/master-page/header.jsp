<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<script type="text/javascript">
	function relativeRedir(loc) {
		var b = document.getElementsByTagName('base');
		if (b && b[0] && b[0].href) {
			if (b[0].href.substr(b[0].href.length - 1) == '/' && loc.charAt(0) == '/')
				loc = loc.substr(1);
			loc = b[0].href + loc;
		}
		window.location.replace(loc);
	}
</script>


<div>

	<center>
		<a href="http://localhost:8080/Acme-Neighbours/"><img
			src="images/logo.png" alt="Acme Neighbours co., Inc." /></a>
	</center>
</div>

<div style="background-color: #322f32;">
	<ul id="jMenu" style="display: flex;">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		<security:authorize
			access="!(hasRole('PRESIDENT') || hasRole('VOLUNTEER'))">
			<li><a class="fNiv"> <spring:message
						code="master.page.activities" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="course/list.do"><spring:message
								code="master.page.courseList" /></a></li>
					<li><a href="leisure/list.do"><spring:message
								code="master.page.leisureList" /></a></li>
					<security:authorize access="isAuthenticated()">
						<li><a href="reunion/list.do"><spring:message
									code="master.page.reunionList" /></a></li>
					</security:authorize>
					<security:authorize access="hasRole('ASSOCIATE')">
						<li><a href="request/associate/list.do"><spring:message
									code="master.page.myRequests" /></a></li>
					</security:authorize>

				</ul></li>
		</security:authorize>
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message
						code="master.page.login" /></a></li>
			<li><a class="fNiv"> <spring:message
						code="master.page.register" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="associate/register.do"><spring:message
								code="master.page.associate" /></a></li>
					<li><a href="volunteer/register.do"><spring:message
								code="master.page.volunteer" /></a></li>


				</ul></li>

		</security:authorize>

		<security:authorize access="hasRole('TREASURER')">


			<li><a class="fNiv" href="configuration/treasurer/edit.do"><spring:message
						code="master.page.config" /></a></li>
		</security:authorize>
		<security:authorize access="hasRole('SECRETARY')">
			<li><a class="fNiv"> <spring:message
						code="master.page.rooms" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="room/secretary/list.do"><spring:message
								code="master.page.list" /></a></li>
					<li><a href="petition/secretary/list.do"><spring:message
								code="master.page.petitionlist" /></a></li>
					<li><a href="offer/secretary/list.do"><spring:message
								code="master.page.offers" /></a></li>
				</ul></li>

			<li><a class="fNiv"> <spring:message
						code="master.page.records" /></a>
				<ul>
					<li class="arrow"></li>

					<li><a href="record/secretary/list.do"><spring:message
								code="master.page.recordList" /></a></li>
				</ul>
		</security:authorize>

		<security:authorize access="hasRole('ASSOCIATE')">
			<li><a class="fNiv"> <spring:message
						code="master.page.rooms" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="room/associate/list.do"><spring:message
								code="master.page.list" /></a></li>
					<li><a href="petition/associate/list.do"><spring:message
								code="master.page.petitionlist" /></a></li>

				</ul></li>

		</security:authorize>


		<security:authorize access="hasRole('TREASURER')">
			<li><a class="fNiv" href="dashboard/treasurer/report.do"> <spring:message
						code="master.page.dashboard" /></a></li>

		</security:authorize>





		<security:authorize access="hasRole('VOLUNTEER')">
			<li><a class="fNiv"> <spring:message
						code="master.page.courses" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="course/volunteer/list.do"><spring:message
								code="master.page.list" /></a></li>
					<li><a href="request/volunteer/list.do"><spring:message
								code="master.page.myRequests" /></a></li>
				</ul>
		</security:authorize>
		<security:authorize access="hasRole('PRESIDENT')">

			<li><a class="fNiv"> <spring:message
						code="master.page.activities" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="leisure/president/list.do"><spring:message
								code="master.page.leisures" /></a></li>
					<li><a href="reunion/president/list.do"><spring:message
								code="master.page.reunions" /></a></li>
					<li><a href="course/president/list.do"><spring:message
								code="master.page.courses" /></a></li>
					<li><a href="request/president/list.do"><spring:message
								code="master.page.myRequests" /></a></li>
				</ul></li>
			<li><a class="fNiv"> <spring:message
						code="master.page.associate" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="associate/president/list.do"><spring:message
								code="master.page.associateList" /></a></li>


				</ul></li>
			<li><a class="fNiv"> <spring:message
						code="master.page.register" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="secretary/president/register.do"><spring:message
								code="master.page.secretary" /></a></li>
					<li><a href="treasurer/president/register.do"><spring:message
								code="master.page.treasurer" /></a></li>

				</ul></li>
			<li><a href="report/president/dashboard.do"><spring:message
						code="master.page.dashboard" /></a></li>
		</security:authorize>

		<security:authorize access="isAuthenticated()">
			<li><a class="fNiv"> <spring:message
						code="master.page.message" />
			</a>
				<ul>
					<li class="arrow"></li>

					<li><a href="message/actor/inbox.do"><spring:message
								code="master.page.inbox" /> </a></li>
					<li><a href="message/actor/outbox.do"><spring:message
								code="master.page.outbox" /> </a></li>
				</ul></li>
			<security:authorize access="hasRole('TREASURER')">
				<li><a class="fNiv" href="suggestion/treasurer/list.do"><spring:message
							code="master.page.myq" /></a></li>
			</security:authorize>
			<li><a class="fNiv"> <spring:message
						code="master.page.profile" /> (<security:authentication
						property="principal.username" />)
			</a>
				<ul>
					<li class="arrow"></li>
					<security:authorize access="hasRole('ASSOCIATE')">
						<li><a href="associate/private.do"><spring:message
									code="master.page.profile" /> </a></li>
						<li><a href="directive/actor/list.do"> <spring:message
									code="master.page.privilegedActorslist" /></a></li>
					</security:authorize>

					<security:authorize access="hasRole('VOLUNTEER')">
						<li><a href="volunteer/private.do"><spring:message
									code="master.page.profile" /> </a></li>
						<li><a href="directive/actor/list.do"> <spring:message
									code="master.page.privilegedActorslist" /></a></li>
					</security:authorize>
					<li><a href="answer/actor/list.do"><spring:message
								code="master.page.qya" /></a></li>
					<li><a href="j_spring_security_logout"><spring:message
								code="master.page.logout" /> </a></li>
				</ul></li>
		</security:authorize>
	</ul>
</div>

<div style="right:100%;">
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>


<!--//BLOQUE COOKIES-->
<div id="barraaceptacion" style="display: block;">
	<div class="inner">
		<spring:message code="master.page.law.solicitud" />
		<a href="javascript:void(0);" class="ok" onclick="PonerCookie();"><b>OK</b></a>
		| <a href="law/cookies.do" target="_blank" class="info"><spring:message
				code="master.page.information" /></a> <a href="law/term.do"
			target="_blank" class="info"><spring:message
				code="master.page.law.term" /></a>
	</div>
</div>

<div id="cookies">
	<script>
		function getCookie(c_name) {
			var c_value = document.cookie;
			var c_start = c_value.indexOf(" " + c_name + "=");
			if (c_start == -1) {
				c_start = c_value.indexOf(c_name + "=");
			}
			if (c_start == -1) {
				c_value = null;
			} else {
				c_start = c_value.indexOf("=", c_start) + 1;
				var c_end = c_value.indexOf(";", c_start);
				if (c_end == -1) {
					c_end = c_value.length;
				}
				c_value = unescape(c_value.substring(c_start, c_end));
			}
			return c_value;
		}

		function setCookie(c_name, value, exdays) {
			var exdate = new Date();
			exdate.setDate(exdate.getDate() + exdays);
			var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
			document.cookie = c_name + "=" + c_value;
		}

		if (getCookie('tiendaaviso') != "1") {
			document.getElementById("barraaceptacion").style.display = "block";
		} else {
			document.getElementById("barraaceptacion").style.display = "none";
		}
		function PonerCookie() {
			setCookie('tiendaaviso', '1', 365);
			document.getElementById("barraaceptacion").style.display = "none";
		}
	</script>
</div>

