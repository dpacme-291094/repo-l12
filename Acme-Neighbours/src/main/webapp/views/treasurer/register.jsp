<%--
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="treasurer/president/register.do"
	modelAttribute="formTreasurer">

	<form:hidden path="userAccount.Authorities" />



	<table id="formus">
		<tr>
			<td><u>
					<h3>
						<spring:message code="treasurer.personal" />
					</h3>
			</u></td>
		</tr>
		<acme:textbox code="treasurer.name" path="name" />
		<acme:textbox code="treasurer.surname" path="surname" />
		<acme:textbox code="treasurer.email" path="email" />
		<acme:textbox code="treasurer.phone" path="phone" />
		<acme:textbox code="treasurer.DNI" path="DNI" />


		<tr>
			<td><u>
					<h3>
						<spring:message code="treasurer.userac" />
					</h3>
			</u></td>
		</tr>
		<acme:textbox code="treasurer.userAccount" path="userAccount.username" />

		<acme:password code="treasurer.password" path="userAccount.password" />
		<tr>
			<td><b><spring:message code="treasurer.repassword" /></b></td>
			<td><input type="password" name="repassword" /></td>
		</tr>


	</table>

	<input type="checkbox" name="terms">

	<spring:message code="treasurer.terms" />
	<br />
	<br />

	<acme:submit name="save" code="treasurer.register" />
	<acme:cancel url="welcome/index.do" code="treasurer.cancel" />
</form:form>



