
package controllers.associate;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AssociateService;
import services.ConfigurationService;
import controllers.AbstractController;
import domain.Associate;
import domain.CreditCard;

@Controller
@RequestMapping("/associate")
public class ProfileAssociateController extends AbstractController {

	@Autowired
	private AssociateService		associateService;

	@Autowired
	private ActorService			actorService;
	@Autowired
	private ConfigurationService	configService;


	public ProfileAssociateController() {
		super();
	}

	@SuppressWarnings("deprecation")
	private boolean fechaValida(final int mes, final int ano) {
		boolean res = false;
		final Date d = new Date();
		d.setSeconds(7 * 86400);
		if (d.getYear() + 1900 < ano)
			res = true;
		else if (d.getYear() + 1900 == ano && d.getMonth() < mes - 1)
			res = true;

		return res;
	}

	@RequestMapping(value = "/creditCard", method = RequestMethod.GET)
	public ModelAndView create() {
		throw new RuntimeException("Oops! You should not be here. Go back to home page.");

	}
	@RequestMapping(value = "/private", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView result;

		final Associate associate = this.associateService.findByPrincipal();

		result = this.createEditModelAndView(associate);

		return result;
	}

	@RequestMapping(value = "/private", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Associate associate, final BindingResult binding) {

		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(associate);
		else
			try {
				if (this.actorService.letraDNI(associate.getDNI()) == false)
					result = this.createEditModelAndView2(associate, "actor.commit.error6");
				else {
					final Associate a = this.associateService.reconstruct(associate, binding);
					final Associate le = this.associateService.edit(a);
					result = new ModelAndView("associate/private");
					result.addObject("associate", le);
					result.addObject("cc", this.associateService.ocultarNumCC(a.getCreditCard().getNumber()));
					result.addObject("months", this.associateService.totalMonths(associate.getLastPayment()));
					result.addObject("fee", this.configService.findAll().getFee());

					result.addObject("message1", "actor.commit.success");
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView2(associate, "actor.commit.error2");
			}
		return result;
	}
	protected ModelAndView createEditModelAndView(final Associate associate) {

		ModelAndView result;

		result = this.createEditModelAndView2(associate, null);

		return result;
	}

	protected ModelAndView createEditModelAndView2(final Associate associate, final String message) {
		ModelAndView result;

		result = new ModelAndView("associate/private");
		result.addObject("associate", associate);
		result.addObject("message", message);
		result.addObject("months", this.associateService.totalMonths(associate.getLastPayment()));
		result.addObject("fee", this.configService.findAll().getFee());
		result.addObject("cc", this.associateService.ocultarNumCC(this.associateService.findByPrincipal().getCreditCard().getNumber()));
		return result;
	}
	// Save CCN ------------------------------------------------------------
	@RequestMapping(value = "/creditCard", method = RequestMethod.POST, params = "edit2")
	public ModelAndView save2() {
		final CreditCard cc = new CreditCard();
		cc.setExpirationMonth(0);
		cc.setExpirationYear(0);
		cc.setCodeCVV(0);
		return this.createEditModelAndView3(cc);
	}

	// Save ----------------------------------------------------------------
	@RequestMapping(value = "/creditCard", method = RequestMethod.POST, params = "saveCreditCard")
	public ModelAndView saveCreditCard(@Valid final CreditCard creditCard, final BindingResult binding) {

		ModelAndView result;

		final Associate p = this.associateService.findByPrincipal();
		if (binding.hasErrors())
			result = this.createEditModelAndView3(creditCard);
		else if (!this.fechaValida(creditCard.getExpirationMonth(), creditCard.getExpirationYear()))
			result = this.createEditModelAndView3(creditCard, "actor.commit.error4");
		else
			try {

				p.setCreditCard(creditCard);
				final Associate aa = this.associateService.edit(p);
				result = new ModelAndView("associate/private");
				result.addObject("associate", aa);
				result.addObject("message1", "actor.commit.success");
				result.addObject("cc", this.associateService.ocultarNumCC(p.getCreditCard().getNumber()));
				result.addObject("months", this.associateService.totalMonths(p.getLastPayment()));
				result.addObject("fee", this.configService.findAll().getFee());

			} catch (final Throwable oops) {
				result = this.createEditModelAndView3(creditCard, "actor.commit.error");
			}

		return result;
	}

	protected ModelAndView createEditModelAndView3(final CreditCard creditCard) {
		ModelAndView result;

		result = this.createEditModelAndView3(creditCard, null);

		return result;
	}

	protected ModelAndView createEditModelAndView3(final CreditCard creditCard, final String message) {
		ModelAndView result;

		result = new ModelAndView("creditCard/edit");
		result.addObject("creditCard", creditCard);
		result.addObject("message", message);
		final Collection<String> brandNames = Arrays.asList("VISA", "MASTERCARD", "DISCOVER", "DINNERS", "AMEX");
		result.addObject("brandNames", brandNames);

		return result;
	}

	@RequestMapping(value = "/private", method = RequestMethod.POST, params = "pay")
	public ModelAndView pay(@Valid final Integer associateId) {
		ModelAndView result;

		final Associate res = this.associateService.findByPrincipal();

		result = new ModelAndView("chorbi/fee");
		if (!this.fechaValida(res.getCreditCard().getExpirationMonth(), res.getCreditCard().getExpirationYear()))
			result = this.createEditModelAndView2(res, "actor.commit.error4");
		else
			try {

				this.associateService.pay(res);
				result = new ModelAndView("redirect: ../../private.do");
			} catch (final Throwable oops) {
				result.addObject("message", "actor.commit.error2");
			}

		return result;
	}

}
