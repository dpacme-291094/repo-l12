
package controllers.associate;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.AssociateService;
import services.OfferService;
import services.PetitionService;
import services.SecretaryService;
import controllers.AbstractController;
import domain.Offer;
import domain.Petition;

@Controller
@RequestMapping("/offer/associate")
public class OfferAssociateController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private OfferService		offerService;
	@Autowired
	private AssociateService	associateService;
	@Autowired
	private SecretaryService	secretaryService;
	@Autowired
	private PetitionService		petitionService;


	// Constructor ---------------------------------------------------------------
	public OfferAssociateController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView list(final Integer offerId) {
		if (offerId == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		final Offer offer = this.offerService.findOne(offerId);
		final ModelAndView result;
		if (offer == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		else {

			result = new ModelAndView("offer/profile");
			result.addObject("offer", offer);
			final Date currentDate = new Date();
			currentDate.setTime(currentDate.getTime() + 86400000);
			result.addObject("currentDate", currentDate);
			if (this.petitionService.findPetitionOffer(offerId, this.associateService.findByPrincipal().getId()) == null) {
				result.addObject("isRegistered", false);
				result.addObject("isDenied", false);
			} else {
				final Petition p = this.petitionService.findPetitionOffer(offerId, this.associateService.findByPrincipal().getId());
				result.addObject("isRegistered", true);
				result.addObject("isDenied", p.getStatus().equals("DENIED"));

			}
		}
		return result;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.POST, params = "register")
	public ModelAndView create(@Valid final Integer offerId) {
		ModelAndView result;
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);

		final Offer offer = this.offerService.findOne(offerId);
		if (this.petitionService.findPetitionOffer(offerId, this.associateService.findByPrincipal().getId()) != null) {
			result = this.list(offerId);
			result.addObject("message", "petition.already");
		} else if (offer.getDateRequest().before(currentDate)) {
			result = this.list(offerId);
			result.addObject("message", "petition.notpast");
		} else {
			final Petition a = this.petitionService.create(offer, this.associateService.findByPrincipal());

			result = new ModelAndView("petition/create");
			result.addObject("petition", a);
		}
		return result;
	}
	@RequestMapping(value = "/profile", method = RequestMethod.POST, params = "unregister")
	public ModelAndView unregister(@Valid final Integer offerId) {
		ModelAndView result;
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		final Petition p = this.petitionService.findPetitionOffer(offerId, this.associateService.findByPrincipal().getId());
		final Offer offer = this.offerService.findOne(offerId);
		if (p == null) {
			result = this.list(offerId);
			result.addObject("message", "petition.already1");
		} else if (offer.getDateRequest().before(currentDate)) {
			result = this.list(offerId);
			result.addObject("message", "petition.notpast1");
		} else {
			this.petitionService.delete(p);
			result = this.list(offerId);
			result.addObject("message", "offer.cancel.ok");
		}
		return result;
	}
}
