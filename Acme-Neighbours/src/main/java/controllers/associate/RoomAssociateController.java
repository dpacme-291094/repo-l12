
package controllers.associate;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.AssociateService;
import services.PetitionService;
import services.RoomService;
import services.SecretaryService;
import controllers.AbstractController;
import domain.Room;

@Controller
@RequestMapping("/room/associate")
public class RoomAssociateController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private RoomService			roomService;
	@Autowired
	private AssociateService	associateService;
	@Autowired
	private SecretaryService	secretaryService;
	@Autowired
	private PetitionService		petitionService;


	// Constructor ---------------------------------------------------------------
	public RoomAssociateController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final Collection<Room> rooms = this.roomService.findAll();

		final ModelAndView result = new ModelAndView("room/list");
		result.addObject("rooms", rooms);
		result.addObject("uri", "room/associate/list.do");

		return result;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView list(final Integer roomId) {
		if (roomId == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		final Room room = this.roomService.findOne(roomId);
		final ModelAndView result;
		if (room == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		else {

			result = new ModelAndView("room/profile");
			result.addObject("room", room);
			result.addObject("offers", room.getOffers());
			result.addObject("uri", "petition/associate/create.do");
			final Date currentDate = new Date();
			currentDate.setTime(currentDate.getTime() + 86400000);
			result.addObject("currentDate", currentDate);

			result.addObject("petitions", this.associateService.findByPrincipal().getPetitions());

		}
		return result;
	}
}
