
package controllers.associate;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.CommentService;
import controllers.AbstractController;
import controllers.CourseController;
import controllers.LeisureController;
import controllers.ReunionController;
import domain.Comment;
import domain.Leisure;
import domain.Reunion;

@Controller
@RequestMapping("/comment/associate")
public class CommentAssociateController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private CommentService		commentService;
	@Autowired
	private ReunionController	reunionService;
	@Autowired
	private CourseController	courseService;
	@Autowired
	private LeisureController	leisureService;


	// Constructor ---------------------------------------------------------------
	public CommentAssociateController() {
		super();
	}

	// Create -------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView create() {
		throw new RuntimeException("Oops! You should not be here. Go back to home page.");

	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView create(@Valid final Comment c, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = new ModelAndView("comment/edit");
			result.addObject("comment", c);
			result.addObject("message", null);
		} else {
			final Comment comment = this.commentService.reconstruct(c, binding);

			try {
				this.commentService.save(comment);
				if (c.getActivity() instanceof Reunion)
					result = this.reunionService.list(c.getActivity().getId());
				else if (c.getActivity() instanceof Leisure)
					result = this.leisureService.list(c.getActivity().getId());
				else
					result = this.courseService.list(c.getActivity().getId());
			} catch (final Throwable oops) {
				result = new ModelAndView("comment/edit");
				result.addObject("comment", c);
				result.addObject("message", "comment.error");
			}
		}
		return result;
	}

}
