
package controllers.associate;

import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.AssociateService;
import services.OfferService;
import services.PetitionService;
import controllers.AbstractController;
import domain.Petition;

@Controller
@RequestMapping("/petition/associate")
public class PetitionAssociateController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private PetitionService				petitionService;
	@Autowired
	private AssociateService			associateService;
	@Autowired
	private OfferService				offerService;
	@Autowired
	private OfferAssociateController	offerAssociateController;


	// Constructor ---------------------------------------------------------------
	public PetitionAssociateController() {
		super();
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		throw new RuntimeException("Oops! You should not be here. Go back to home page.");

	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final Collection<Petition> petitions = this.associateService.findByPrincipal().getPetitions();

		final ModelAndView result = new ModelAndView("petition/list");
		result.addObject("petitions", petitions);
		result.addObject("uri", "petition/associate/list.do");
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		result.addObject("currentDate", currentDate);
		return result;
	}

	// Create -------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@Valid final Petition petition, final BindingResult binding) {
		ModelAndView result;
		final String redirectView;
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);

		if (binding.hasErrors())
			result = this.createEditModelAndView(petition);
		else
			try {
				if (this.petitionService.findPetitionOffer(petition.getOffer().getId(), this.associateService.findByPrincipal().getId()) != null) {
					result = this.offerAssociateController.list(petition.getOffer().getId());
					result.addObject("message", "petition.already");
				} else if (petition.getOffer().getDateRequest().before(currentDate)) {
					result = this.offerAssociateController.list(petition.getOffer().getId());
					result.addObject("message", "petition.notpast");
				} else {
					this.petitionService.save(petition);
					redirectView = "redirect:../../petition/associate/list.do";
					result = new ModelAndView(redirectView);
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(petition, "petition.error");
			}

		return result;
	}
	// Create -------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "cancel")
	public ModelAndView delete(@Valid final Integer petitionId) {
		ModelAndView result;
		final Petition s = this.petitionService.findOne(petitionId);

		try {

			if (s == null) {
				final Collection<Petition> petitions = this.associateService.findByPrincipal().getPetitions();

				result = new ModelAndView("petition/list");
				result.addObject("petitions", petitions);
				result.addObject("uri", "petition/associate/list.do");
				result.addObject("message", "suggestion.deleted");
			} else
				this.petitionService.delete(s);
			result = new ModelAndView("suggestion/list");
			final Collection<Petition> petitions = this.associateService.findByPrincipal().getPetitions();

			result = new ModelAndView("petition/list");
			result.addObject("petitions", petitions);
			result.addObject("uri", "petition/associate/list.do");
		} catch (final Throwable oops) {

			final Collection<Petition> petitions = this.associateService.findByPrincipal().getPetitions();

			result = new ModelAndView("petition/list");
			result.addObject("petitions", petitions);
			result.addObject("uri", "petition/associate/list.do");
			result.addObject("message", "suggestion.error2");
		}

		return result;
	}

	protected ModelAndView createEditModelAndView(final Petition petition) {
		ModelAndView result;
		result = this.createEditModelAndView(petition, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final Petition petition, final String message) {

		final ModelAndView result = new ModelAndView("petition/create");
		result.addObject("petition", petition);
		result.addObject("message", message);
		return result;
	}

}
