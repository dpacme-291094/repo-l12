
package controllers.associate;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.SuggestionService;
import controllers.AbstractController;
import domain.Suggestion;

@Controller
@RequestMapping("/suggestion/associate")
public class SuggestionAssociateController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private SuggestionService	suggestionService;
	@Autowired
	private ActorService		actorService;


	// Constructor ---------------------------------------------------------------
	public SuggestionAssociateController() {
		super();
	}

	// Create -------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final Suggestion suggestion = this.suggestionService.create();
		final ModelAndView result = new ModelAndView("suggestion/create");
		result.addObject("suggestion", suggestion);
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@Valid final Suggestion suggestion, final BindingResult binding) {
		ModelAndView result;
		final String redirectView;

		if (binding.hasErrors())
			result = this.createEditModelAndView(suggestion);
		else
			try {

				this.suggestionService.save(suggestion);
				redirectView = "redirect:../../welcome/index.do";
				result = new ModelAndView(redirectView);

			} catch (final Throwable oops) {
				result = this.createEditModelAndView(suggestion, "suggestion.error");
			}

		return result;
	}
	// Other Ancillary Methods -------------------------------------
	protected ModelAndView createEditModelAndView(final Suggestion suggestion) {
		ModelAndView result;
		result = this.createEditModelAndView(suggestion, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final Suggestion suggestion, final String message) {

		final ModelAndView result = new ModelAndView("suggestion/create");
		result.addObject("suggestion", suggestion);
		result.addObject("message", message);
		return result;
	}

}
