
package controllers.associate;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActivityService;
import services.AssociateService;
import services.CommentService;
import services.LeisureService;
import services.RequestService;
import controllers.AbstractController;
import domain.Activity;
import domain.Comment;
import domain.Leisure;
import domain.Request;
import domain.Reunion;

@Controller
@RequestMapping("/activity/associate")
public class ActivityAssociateController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private ActivityService		activityService;
	@Autowired
	private AssociateService	associateService;

	@Autowired
	private RequestService		requestService;
	@Autowired
	private CommentService		commentService;
	@Autowired
	private LeisureService		leisureService;


	// Constructor ---------------------------------------------------------------
	public ActivityAssociateController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView create() {
		throw new RuntimeException("Oops! You should not be here. Go back to home page.");

	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final Collection<Activity> activities = this.activityService.findAll();

		final ModelAndView result = new ModelAndView("activity/list");
		result.addObject("activities", activities);
		result.addObject("uri", "activity/associate/list.do");
		return result;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.POST, params = "register")
	public ModelAndView register(@Valid final int activityId) {
		ModelAndView result;

		final Activity activity = this.activityService.findOne(activityId);
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);

		try {
			if (activity.getMoment().before(currentDate)) {
				result = new ModelAndView("activity/profile");
				result.addObject("message", "activity.late");
				result.addObject("activity", activity);
				result.addObject("hasRequest", true);
				result.addObject("fecha", currentDate);
				if (activity instanceof Leisure)
					result.addObject("isLeisure", true);
				else if (activity instanceof Reunion)
					result.addObject("isReunion", true);
				else
					result.addObject("isCourse", true);

			} else if (activity instanceof Leisure && !this.fechaValida(this.associateService.findByPrincipal().getCreditCard().getExpirationMonth(), this.associateService.findByPrincipal().getCreditCard().getExpirationYear())) {

				result = new ModelAndView("activity/profile");
				result.addObject("message", "actor.commit.error4");
				result.addObject("activity", activity);
				result.addObject("hasRequest", true);
				result.addObject("fecha", currentDate);
				if (activity instanceof Leisure)
					result.addObject("isLeisure", true);
				else if (activity instanceof Reunion)
					result.addObject("isReunion", true);
				else
					result.addObject("isCourse", true);
			} else if (this.requestService.findequest(this.associateService.findByPrincipal().getId(), activity.getId()) != null) {
				result = new ModelAndView("activity/profile");
				result.addObject("message", "activity.already");
				result.addObject("activity", activity);
				if (activity instanceof Leisure)
					result.addObject("isLeisure", true);
				else if (activity instanceof Reunion)
					result.addObject("isReunion", true);
				else
					result.addObject("isCourse", true);
				result.addObject("hasRequest", false);
				result.addObject("fecha", currentDate);
			} else if (this.activityService.findOne(activityId).getSeats() < 1) {
				result = new ModelAndView("activity/profile");
				result.addObject("message", "activity.noseats");
				result.addObject("activity", activity);
				if (activity instanceof Leisure)
					result.addObject("isLeisure", true);
				else if (activity instanceof Reunion)
					result.addObject("isReunion", true);
				else
					result.addObject("isCourse", true);
				result.addObject("hasRequest", true);
				result.addObject("fecha", currentDate);
			} else {
				final Request p = this.requestService.create(activity, this.associateService.findByPrincipal());

				result = new ModelAndView("request/create");

				result.addObject("request", p);
				final Collection<Integer> rates = Arrays.asList(1, 2, 3, 4);

				result.addObject("rates", rates);
			}

		} catch (final Throwable oops) {
			result = new ModelAndView("activity/profile");
			result.addObject("message", "leisure.error");
			result.addObject("activity", activity);
			if (activity instanceof Leisure)
				result.addObject("isLeisure", true);
			else if (activity instanceof Reunion)
				result.addObject("isReunion", true);
			else
				result.addObject("isCourse", true);
			result.addObject("hasRequest", true);
			result.addObject("fecha", currentDate);

		}

		return result;
	}
	@RequestMapping(value = "/profile", method = RequestMethod.POST, params = "unregister")
	public ModelAndView unregister(@Valid final int activityId) {
		ModelAndView result;

		final Activity activity = this.activityService.findOne(activityId);
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);

		final Request p = this.requestService.findequest(this.associateService.findByPrincipal().getId(), activity.getId());

		try {
			if (activity.getMoment().before(currentDate)) {
				result = new ModelAndView("activity/profile");
				result.addObject("message", "activity.late2");
				result.addObject("activity", activity);

				result.addObject("hasRequest", false);
				result.addObject("fecha", currentDate);
				if (activity instanceof Leisure)
					result.addObject("isLeisure", true);
				else if (activity instanceof Reunion)
					result.addObject("isReunion", true);
				else
					result.addObject("isCourse", true);

			} else if (this.requestService.findequest(this.associateService.findByPrincipal().getId(), activity.getId()) == null) {
				result = new ModelAndView("activity/profile");
				result.addObject("message", "activity.already2");
				result.addObject("activity", activity);

				result.addObject("hasRequest", true);
				result.addObject("fecha", currentDate);
				if (activity instanceof Leisure)
					result.addObject("isLeisure", true);
				else if (activity instanceof Reunion)
					result.addObject("isReunion", true);
				else
					result.addObject("isCourse", true);
			} else {
				this.requestService.delete(p);
				result = new ModelAndView("activity/profile");

				result.addObject("activity", activity);

				result.addObject("hasRequest", true);
				result.addObject("fecha", currentDate);
				if (activity instanceof Leisure)
					result.addObject("isLeisure", true);
				else if (activity instanceof Reunion)
					result.addObject("isReunion", true);
				else
					result.addObject("isCourse", true);
			}

		} catch (final Throwable oops) {
			result = new ModelAndView("activity/profile");
			result.addObject("message", "leisure.error");
			result.addObject("activity", activity);

			result.addObject("hasRequest", false);
			result.addObject("fecha", currentDate);
			if (activity instanceof Leisure)
				result.addObject("isLeisure", true);
			else if (activity instanceof Reunion)
				result.addObject("isReunion", true);
			else
				result.addObject("isCourse", true);
		}

		return result;
	}

	// Create -------------------------------------
	@RequestMapping(value = "/profile", method = RequestMethod.POST, params = "createComment")
	public ModelAndView createComment(@Valid final int activityId) {
		final ModelAndView result;

		final Activity activity = this.activityService.findOne(activityId);
		final Comment comment = this.commentService.create(activity, this.associateService.findByPrincipal());

		result = new ModelAndView("comment/edit");
		result.addObject("comment", comment);

		return result;
	}

	@SuppressWarnings("deprecation")
	private boolean fechaValida(final int mes, final int ano) {
		boolean res = false;
		final Date d = new Date();
		d.setSeconds(7 * 86400);
		if (d.getYear() + 1900 < ano)
			res = true;
		else if (d.getYear() + 1900 == ano && d.getMonth() < mes - 1)
			res = true;

		return res;
	}
}
