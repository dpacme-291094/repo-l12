
package controllers.associate;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActivityService;
import services.AssociateService;
import services.RequestService;
import controllers.AbstractController;
import domain.Leisure;
import domain.Request;
import domain.Reunion;

@Controller
@RequestMapping("/request/associate")
public class RequestAssociateController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private RequestService		requestService;
	@Autowired
	private AssociateService	associateService;
	@Autowired
	private ActivityService		activityService;


	// Constructor ---------------------------------------------------------------
	public RequestAssociateController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final Collection<Request> requests = this.associateService.findByPrincipal().getRequests();

		final ModelAndView result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("uri", "request/associate/list.do");
		return result;
	}

	// Create -------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		throw new RuntimeException("Oops! You should not be here. Go back to home page.");

	}
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@Valid final Request request, final BindingResult binding) {
		ModelAndView result;
		final String redirectView;
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		if (binding.hasErrors())
			result = this.createEditModelAndView(request);
		else
			try {
				if (request.getActivity().getMoment().before(currentDate)) {
					result = new ModelAndView("activity/profile");
					result.addObject("message", "activity.late");
					result.addObject("activity", request.getActivity());
					result.addObject("hasRequest", true);
					result.addObject("fecha", currentDate);
					if (request.getActivity() instanceof Leisure)
						result.addObject("isLeisure", true);
					else if (request.getActivity() instanceof Reunion)
						result.addObject("isReunion", true);
					else
						result.addObject("isCourse", true);

				} else if (request.getActivity() instanceof Leisure && !this.fechaValida(this.associateService.findByPrincipal().getCreditCard().getExpirationMonth(), this.associateService.findByPrincipal().getCreditCard().getExpirationYear())) {

					result = new ModelAndView("activity/profile");
					result.addObject("message", "actor.commit.error4");
					result.addObject("activity", request.getActivity());
					result.addObject("hasRequest", true);
					result.addObject("fecha", currentDate);
					if (request.getActivity() instanceof Leisure)
						result.addObject("isLeisure", true);
					else if (request.getActivity() instanceof Reunion)
						result.addObject("isReunion", true);
					else
						result.addObject("isCourse", true);
				} else if (this.requestService.findequest(this.associateService.findByPrincipal().getId(), request.getActivity().getId()) != null) {
					result = new ModelAndView("activity/profile");
					result.addObject("message", "activity.already");
					result.addObject("activity", request.getActivity());

					result.addObject("hasRequest", false);
					result.addObject("fecha", currentDate);
					if (request.getActivity() instanceof Leisure)
						result.addObject("isLeisure", true);
					else if (request.getActivity() instanceof Reunion)
						result.addObject("isReunion", true);
					else
						result.addObject("isCourse", true);
				} else if (request.getActivity().getSeats() < request.getSeats()) {
					result = new ModelAndView("activity/profile");
					result.addObject("message", "activity.noseats");
					result.addObject("activity", request.getActivity());
					if (request.getActivity() instanceof Leisure)
						result.addObject("isLeisure", true);
					else if (request.getActivity() instanceof Reunion)
						result.addObject("isReunion", true);
					else
						result.addObject("isCourse", true);
					result.addObject("hasRequest", true);
					result.addObject("fecha", currentDate);

				} else {

					this.requestService.save(request);
					redirectView = "redirect:../../request/associate/list.do";
					result = new ModelAndView(redirectView);
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(request, "request.error");
			}

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid final Integer requestId) {
		ModelAndView result;
		final Request s = this.requestService.findOne(requestId);

		try {

			if (s == null) {
				final Collection<Request> requests = this.associateService.findByPrincipal().getRequests();

				result = new ModelAndView("request/list");
				result.addObject("requests", requests);
				result.addObject("uri", "request/associate/list.do");
				result.addObject("message", "suggestion.deleted");
			} else {
				this.requestService.delete(s);
				result = new ModelAndView("suggestion/list");
				final Collection<Request> requests = this.associateService.findByPrincipal().getRequests();

				result = new ModelAndView("request/list");
				result.addObject("requests", requests);
				result.addObject("uri", "request/associate/list.do");
			}
		} catch (final Throwable oops) {

			final Collection<Request> requests = this.associateService.findByPrincipal().getRequests();

			result = new ModelAndView("request/list");
			result.addObject("requests", requests);
			result.addObject("uri", "request/associate/list.do");
			result.addObject("message", "suggestion.error2");
		}

		return result;
	}

	protected ModelAndView createEditModelAndView(final Request request) {
		ModelAndView result;
		result = this.createEditModelAndView(request, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final Request request, final String message) {

		final ModelAndView result = new ModelAndView("request/create");
		result.addObject("request", request);
		result.addObject("message", message);
		final Collection<Integer> rates = Arrays.asList(1, 2, 3, 4);

		result.addObject("rates", rates);
		return result;
	}

	@SuppressWarnings("deprecation")
	private boolean fechaValida(final int mes, final int ano) {
		boolean res = false;
		final Date d = new Date();
		d.setSeconds(7 * 86400);
		if (d.getYear() + 1900 < ano)
			res = true;
		else if (d.getYear() + 1900 == ano && d.getMonth() < mes - 1)
			res = true;

		return res;
	}
}
