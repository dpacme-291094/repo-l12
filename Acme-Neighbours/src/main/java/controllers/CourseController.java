
package controllers;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AssociateService;
import services.CourseService;
import services.RequestService;
import domain.Associate;
import domain.Course;
import domain.President;
import domain.Request;

@Controller
@RequestMapping("/course")
public class CourseController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private CourseService		courseService;

	@Autowired
	private AssociateService	associateService;
	@Autowired
	private RequestService		requestService;
	@Autowired
	private ActorService		actorService;


	// Constructor ---------------------------------------------------------------
	public CourseController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final Collection<Course> courses = this.courseService.findAll();

		final ModelAndView result = new ModelAndView("activity/list");
		result.addObject("activities", courses);
		result.addObject("uri", "course/list.do");
		result.addObject("isCourse", true);
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		result.addObject("currentDate", currentDate);
		return result;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView list(final Integer courseId) {
		if (courseId == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		final Course course = this.courseService.findOne(courseId);
		final ModelAndView result;
		if (course == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		else {

			result = new ModelAndView("activity/profile");
			final Date currentDate = new Date();
			currentDate.setTime(currentDate.getTime() + 86400000);
			result.addObject("fecha", currentDate);
			result.addObject("isCourse", true);
			result.addObject("activity", course);

			if (!this.actorService.isAnonymous() && this.actorService.findByPrincipal() instanceof Associate) {
				result.addObject("associateId", this.actorService.findByPrincipal().getId());

				final Request request = this.requestService.findequest(this.associateService.findByPrincipal().getId(), course.getId());
				if (request == null)
					result.addObject("hasRequest", true);
				else {
					result.addObject("hasRequest", false);
					result.addObject("requestStatus", request.getStatus());
				}
			}

			if (!this.actorService.isAnonymous() && this.actorService.findByPrincipal() instanceof President)
				result.addObject("isPresident", true);

		}
		return result;
	}

}
