
package controllers;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AssociateService;
import domain.Associate;
import forms.FormAssociate;

@Controller
@RequestMapping("/associate")
public class AssociateController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private AssociateService	associateService;
	@Autowired
	private ActorService		actorService;


	// Constructor ---------------------------------------------------------------
	public AssociateController() {
		super();
	}

	// Create -------------------------------------
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView create() {

		final FormAssociate formAssociate = this.associateService.createForm();
		final ModelAndView result = new ModelAndView("associate/register");
		result.addObject("formAssociate", formAssociate);
		final Collection<String> brandNames = Arrays.asList("VISA", "MASTERCARD", "DISCOVER", "DINNERS", "AMEX");
		result.addObject("brandNames", brandNames);
		return result;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@ModelAttribute("formAssociate") final FormAssociate formAssociate, final BindingResult binding, final boolean terms, final String repassword) {
		ModelAndView result;
		final String redirectView;

		final Associate associate = this.associateService.reconstruct(formAssociate, binding);
		if (binding.hasErrors())
			result = this.createEditModelAndView(formAssociate);
		else
			try {
				final String username = associate.getUserAccount().getUsername();

				if (!this.fechaValida(associate.getCreditCard().getExpirationMonth(), associate.getCreditCard().getExpirationYear()))
					result = this.createEditModelAndView(formAssociate, "actor.commit.error4");
				else if (this.actorService.letraDNI(associate.getDNI()) == false)
					result = this.createEditModelAndView(formAssociate, "actor.commit.error6");

				else if (this.actorService.existsUsername(username))
					result = this.createEditModelAndView(formAssociate, "actor.commit.error");
				else if (this.actorService.existsEmail(associate.getEmail()))
					result = this.createEditModelAndView(formAssociate, "actor.commit.error5");

				else if (!terms)
					result = this.createEditModelAndView(formAssociate, "actor.error.terms");
				else if (!repassword.equals(associate.getUserAccount().getPassword()))
					result = this.createEditModelAndView(formAssociate, "actor.pass");
				else {
					this.associateService.save(associate);
					redirectView = "redirect:../security/login.do";
					result = new ModelAndView(redirectView);
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(formAssociate, "actor.commit.error2");
			}

		return result;
	}
	// Other Ancillary Methods -------------------------------------
	protected ModelAndView createEditModelAndView(final FormAssociate associate) {
		ModelAndView result;
		result = this.createEditModelAndView(associate, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final FormAssociate associate, final String message) {
		ModelAndView result;
		result = new ModelAndView("associate/register");
		result.addObject("formAssociate", associate);
		result.addObject("message", message);
		final Collection<String> brandNames = Arrays.asList("VISA", "MASTERCARD", "DISCOVER", "DINNERS", "AMEX");
		result.addObject("brandNames", brandNames);
		return result;
	}

	@SuppressWarnings("deprecation")
	private boolean fechaValida(final int mes, final int ano) {
		boolean res = false;
		final Date d = new Date();
		d.setSeconds(7 * 86400);
		if (d.getYear() + 1900 < ano)
			res = true;
		else if (d.getYear() + 1900 == ano && d.getMonth() < mes - 1)
			res = true;

		return res;
	}

	// Administrators List -------------------------------------

}
