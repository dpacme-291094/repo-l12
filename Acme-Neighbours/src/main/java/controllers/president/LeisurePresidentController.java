
package controllers.president;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.LeisureService;
import controllers.AbstractController;
import domain.Leisure;

@Controller
@RequestMapping("/leisure/president")
public class LeisurePresidentController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private LeisureService	leisureService;


	// Constructor ---------------------------------------------------------------
	public LeisurePresidentController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView result;
		result = new ModelAndView("activity/list");
		result.addObject("activities", this.leisureService.findAll());
		result.addObject("isLeisure", true);
		result.addObject("uri", "leisure/president/list.do");

		return result;

	}

	// Delete --------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@RequestParam final int actId) throws Exception {
		ModelAndView result;

		final Leisure l = this.leisureService.findOne(actId);
		try {
			this.leisureService.delete(l);
		} catch (final Throwable oops) {
			result = new ModelAndView("activity/list");
			result.addObject("activities", this.leisureService.findAll());
			result.addObject("isLeisure", true);
			result.addObject("message", "leisure.error");
		}
		result = new ModelAndView("activity/list");
		result.addObject("activities", this.leisureService.findAll());
		result.addObject("isLeisure", true);

		return result;
	}
	// Create --------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView create() {
		Leisure leisure;
		leisure = this.leisureService.create();

		final ModelAndView result = new ModelAndView("activity/edit");

		result.addObject("isLeasure", true);
		result.addObject("isPassed", false);
		result.addObject("modelAttribute", "leisure");
		result.addObject("leisure", leisure);
		result.addObject("new1", true);
		result.addObject("uri", "leisure/president/edit.do");
		return result;
	}

	// Modify --------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "edit")
	public ModelAndView edit(@Valid final Integer actId) {
		final ModelAndView result;
		Leisure leisure;
		leisure = this.leisureService.findOne(actId);
		final Collection<String> estado;
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);

		if (leisure.getMoment().before(currentDate))
			estado = Arrays.asList("CLOSED");
		else
			estado = Arrays.asList("OPEN", "CANCELLED");

		result = new ModelAndView("activity/edit");
		result.addObject("estado", estado);
		result.addObject("modelAttribute", "leisure");
		result.addObject("leisure", leisure);
		result.addObject("isLeasure", true);
		result.addObject("new1", false);
		result.addObject("isPassed", leisure.getMoment().before(currentDate));
		result.addObject("uri", "leisure/president/edit.do");

		return result;
	}

	// Save ----------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView create(@Valid final Leisure leisure, final BindingResult binding, final boolean isPassed) {
		ModelAndView result;
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);

		if ((leisure.getId() > 0 && this.leisureService.findOne(leisure.getId()).getMoment().before(currentDate) && leisure.getStatus().equals("OPEN"))
			|| (leisure.getId() > 0 && this.leisureService.findOne(leisure.getId()).getMoment().before(currentDate) && leisure.getStatus().equals("CANCELLED")))
			result = this.createEditModelAndView(leisure, "leisure.passed");
		else if (binding.hasErrors())
			result = this.createEditModelAndView(leisure);
		else
			try {

				if (leisure.getMoment().before(currentDate) && isPassed == false)
					result = this.createEditModelAndView(leisure, "leisure.notpast");
				else {
					final Leisure leisuref = this.leisureService.reconstruct(leisure, binding);
					this.leisureService.save(leisuref);
					result = new ModelAndView("activity/list");
					result.addObject("activities", this.leisureService.findAll());

					result.addObject("isLeisure", true);
					result.addObject("uri", "leisure/president/list.do");
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(leisure, "leisure.error");
			}
		return result;
	}
	protected ModelAndView createEditModelAndView(final Leisure activity) {
		ModelAndView result;
		result = this.createEditModelAndView(activity, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final Leisure leisure, final String message) {

		final ModelAndView result = new ModelAndView("activity/edit");
		result.addObject("modelAttribute", "leisure");
		result.addObject("leisure", leisure);
		result.addObject("message", message);
		result.addObject("isLeasure", true);
		result.addObject("uri", "leisure/president/edit.do");
		Collection<String> estado = new ArrayList<String>();
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		if (leisure.getId() > 0)
			result.addObject("new1", false);
		else
			result.addObject("new1", true);
		if (leisure.getMoment() != null && leisure.getId() > 0)
			result.addObject("isPassed", this.leisureService.findOne(leisure.getId()).getMoment().before(currentDate));
		else
			result.addObject("isPassed", false);

		if (leisure.getId() > 0)
			if (this.leisureService.findOne(leisure.getId()).getMoment().before(currentDate))
				estado = Arrays.asList("CLOSED");
			else
				estado = Arrays.asList("OPEN", "CANCELLED");
		result.addObject("estado", estado);
		return result;
	}
}
