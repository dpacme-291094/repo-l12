
package controllers.president;

import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActivityService;
import services.PresidentService;
import services.RequestService;
import controllers.AbstractController;
import domain.Request;

@Controller
@RequestMapping("/request/president")
public class RequestPresidenController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private RequestService		requestService;
	@Autowired
	private PresidentService	presidentService;
	@Autowired
	private ActivityService		activityService;


	// Constructor ---------------------------------------------------------------
	public RequestPresidenController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		final Collection<Request> requests = this.requestService.findRequests();

		final ModelAndView result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("uri", "request/president/list.do");

		return result;
	}
	// Create -------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "accept")
	public ModelAndView register(@Valid final Integer requestId) {
		final ModelAndView result;
		final Request s = this.requestService.findOne(requestId);
		final Collection<Request> requests = this.requestService.findRequests();

		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);

		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("uri", "request/president/list.do");

		if (s.getActivity().getSeats() < s.getSeats())
			result.addObject("message", "activity.noseats");
		else if (s.getActivity().getMoment().before(currentDate))
			result.addObject("message", "leisure.notpast");
		else if (s.getActivity().getStatus().equals("CANCELLED") || s.getActivity().getStatus().equals("CLOSED"))
			result.addObject("message", "activity.late");
		else
			this.requestService.accept(s);
		return result;
	}
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "deny")
	public ModelAndView delete(@Valid final Integer requestId) {
		final ModelAndView result;
		final Request s = this.requestService.findOne(requestId);

		final Collection<Request> requests = this.requestService.findRequests();

		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("uri", "request/president/list.do");

		this.requestService.deny(s);
		return result;
	}

}
