
package controllers.president;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.AssociateService;
import services.RequestService;
import controllers.AbstractController;
import domain.Request;

@Controller
@RequestMapping("/associate/president")
public class AssociatePresidentController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private AssociateService	associateService;

	@Autowired
	private RequestService		requestService;


	// Constructor ---------------------------------------------------------------
	public AssociatePresidentController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView result;
		result = new ModelAndView("associate/list");
		result.addObject("associates", this.associateService.findAll());
		result.addObject("uri", "associate/president/list.do");

		return result;

	}
	@RequestMapping(value = "/leisureList", method = RequestMethod.GET)
	public ModelAndView leisureList(final Integer leisureId) {
		if (leisureId == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		ModelAndView result;
		result = new ModelAndView("associate/attendeesList");

		final Collection<Request> ca = this.requestService.requestAccepted(leisureId);
		result.addObject("requests", ca);
		result.addObject("uri", "associate/president/leisureList.do");
		result.addObject("seats", this.requestService.requestTotalSeats(leisureId));

		return result;

	}
	@RequestMapping(value = "/reunionList", method = RequestMethod.GET)
	public ModelAndView reunionList(final Integer reunionId) {
		if (reunionId == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		ModelAndView result;

		result = new ModelAndView("associate/attendeesList");

		final Collection<Request> cr = this.requestService.requestAccepted(reunionId);

		result.addObject("requests", cr);
		result.addObject("uri", "associate/president/reunionList.do");
		result.addObject("seats", this.requestService.requestTotalSeats(reunionId));
		return result;

	}
}
