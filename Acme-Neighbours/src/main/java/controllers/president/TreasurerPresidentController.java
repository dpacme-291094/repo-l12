
package controllers.president;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.TreasurerService;
import controllers.AbstractController;
import domain.Treasurer;
import forms.FormTreasurer;

@Controller
@RequestMapping("/treasurer/president")
public class TreasurerPresidentController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private TreasurerService	treasurerService;
	@Autowired
	private ActorService		actorService;


	// Constructor ---------------------------------------------------------------
	public TreasurerPresidentController() {
		super();
	}

	// Create -------------------------------------
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView create() {

		final FormTreasurer formTreasurer = this.treasurerService.createForm();
		final ModelAndView result = new ModelAndView("treasurer/register");
		result.addObject("formTreasurer", formTreasurer);
		return result;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@ModelAttribute("formTreasurer") final FormTreasurer formTreasurer, final BindingResult binding, final boolean terms, final String repassword) {
		ModelAndView result;
		final String redirectView;

		final Treasurer treasurer = this.treasurerService.reconstruct(formTreasurer, binding);
		if (binding.hasErrors())
			result = this.createEditModelAndView(formTreasurer);
		else
			try {
				final String username = treasurer.getUserAccount().getUsername();

				if (this.actorService.letraDNI(treasurer.getDNI()) == false)
					result = this.createEditModelAndView(formTreasurer, "actor.commit.error6");

				else if (this.actorService.existsUsername(username))
					result = this.createEditModelAndView(formTreasurer, "actor.commit.error");
				else if (this.actorService.existsEmail(treasurer.getEmail()))
					result = this.createEditModelAndView(formTreasurer, "actor.commit.error5");

				else if (!terms)
					result = this.createEditModelAndView(formTreasurer, "actor.error.terms");
				else if (!repassword.equals(treasurer.getUserAccount().getPassword()))
					result = this.createEditModelAndView(formTreasurer, "actor.pass");
				else {
					this.treasurerService.save(treasurer);
					redirectView = "redirect:../../security/login.do";
					result = new ModelAndView(redirectView);
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(formTreasurer, "actor.commit.error2");
			}

		return result;
	}
	// Other Ancillary Methods -------------------------------------
	protected ModelAndView createEditModelAndView(final FormTreasurer treasurer) {
		ModelAndView result;
		result = this.createEditModelAndView(treasurer, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final FormTreasurer treasurer, final String message) {
		ModelAndView result;
		result = new ModelAndView("treasurer/register");
		result.addObject("formTreasurer", treasurer);
		result.addObject("message", message);
		return result;
	}

}
