
package controllers.president;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.AssociateService;
import services.CourseService;
import services.LeisureService;
import services.MessageService;
import services.RecordService;
import services.ReunionService;
import services.RoomService;
import controllers.AbstractController;

@Controller
@RequestMapping("/report/president")
public class ReportPresidentController extends AbstractController {

	// Services ---------------------------------------

	@Autowired
	private AssociateService	associateService;

	@Autowired
	private LeisureService		leisureService;

	@Autowired
	private ReunionService		reunionService;

	@Autowired
	private MessageService		messageService;

	@Autowired
	private CourseService		courseService;

	@Autowired
	private RoomService			roomService;

	@Autowired
	private RecordService		recordService;


	// Constructors -----------------------------------

	public ReportPresidentController() {
		super();
	}

	// view -----------------------------------------
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;

		//C queries

		//Query 1
		final Double ar = this.associateService.avgRequests();
		//Query 2
		final Object[] mamir = this.associateService.maxMinAssistantsReunion().toArray();
		//Query 3
		final Object[] mamil = this.associateService.maxMinAssistantsLeisure().toArray();
		//Query 4 y 5
		final Object[] mamiLA = this.leisureService.maxMinLeisure().toArray();
		//Query 6
		final Collection<Object> mmar = this.reunionService.minMaxAvgReunion();
		//Query 7
		final Double cpa = this.associateService.commentPerAssociate();
		//Query 8
		final Collection<Object> mmaspa = this.messageService.mmaSenderPerActor();
		//Query 9
		final Collection<Object> mmarpa = this.messageService.mmaRecipientPerActor();

		//B queries
		//Query 1
		final Collection<Object[]> vmc = this.courseService.volunteerMoreCourses();
		//Query 2

		final Collection<Object[]> marr = this.roomService.maxRoomReserved();
		final Collection<Object[]> mirr = this.roomService.minRoomReserved();
		//Query 3
		final Double rac = this.roomService.ratioAceptadas();
		final Double rde = this.roomService.ratioDenegadas();
		final Double rpe = this.roomService.ratioPendientes();

		//Query 4
		final Collection<Object[]> cma = this.courseService.courseMoreAssistant();
		//Query 5
		final int rap = this.recordService.actasPendientes();
		final int raf = this.recordService.actasFinalizadas();

		result = new ModelAndView("report/dashboard1");

		//C queries

		//Query 1
		result.addObject("ar", ar);
		//Query 2
		if (mamir.length == 2) {
			result.addObject("mar", mamir[0]);
			result.addObject("mir", mamir[1]);
		}
		//Query 3
		if (mamil.length == 2) {
			result.addObject("mal", mamil[0]);
			result.addObject("mil", mamil[1]);
		}
		//Query 4 y 5
		if (mamiLA.length == 2) {
			result.addObject("mala", mamiLA[0]);
			result.addObject("mila", mamiLA[1]);
		}
		//Query 6
		if (mmar.iterator().hasNext())
			result.addObject("mmar", mmar.iterator().next());
		//Query 7
		result.addObject("cpa", cpa);
		//Query 8
		if (mmaspa.iterator().hasNext())
			result.addObject("mmaspa", mmaspa.iterator().next());
		//Query 9
		if (mmarpa.iterator().hasNext())
			result.addObject("mmarpa", mmarpa.iterator().next());

		//B queries

		//Query 1
		result.addObject("vmc", vmc);

		//Query 2

		result.addObject("marr", marr);
		result.addObject("mirr", mirr);

		//Query 3
		result.addObject("rac", rac);
		result.addObject("rde", rde);
		result.addObject("rpe", rpe);

		//Query 4
		result.addObject("cma", cma);

		//Query 5
		result.addObject("raf", raf);
		result.addObject("rap", rap);

		return result;
	}
}
