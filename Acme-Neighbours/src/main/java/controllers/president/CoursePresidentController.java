
package controllers.president;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.CourseService;
import controllers.AbstractController;
import domain.Course;

@Controller
@RequestMapping("/course/president")
public class CoursePresidentController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private CourseService	courseService;


	// Constructor ---------------------------------------------------------------
	public CoursePresidentController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView result;
		result = new ModelAndView("activity/list");
		result.addObject("activities", this.courseService.findAll());
		result.addObject("isCourse", true);
		result.addObject("isEditable", true);
		result.addObject("uri", "course/president/list.do");
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		result.addObject("ispossible", currentDate);

		return result;

	}
	// Delete --------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@RequestParam final int actId) throws Exception {
		ModelAndView result;
		result = new ModelAndView("activity/list");
		final Course l = this.courseService.findOne(actId);
		try {
			l.setStatus("CANCELLED");
			this.courseService.save(l);
		} catch (final Throwable oops) {
			result.addObject("message", "leisure.error");

		}

		result.addObject("activities", this.courseService.findAll());
		result.addObject("isCourse", true);
		result.addObject("isEditable", true);
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		result.addObject("ispossible", currentDate);
		return result;
	}
}
