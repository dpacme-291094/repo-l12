
package controllers.president;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.SecretaryService;
import controllers.AbstractController;
import domain.Secretary;
import forms.FormSecretary;

@Controller
@RequestMapping("/secretary/president")
public class SecretaryPresidentController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private SecretaryService	secretaryService;
	@Autowired
	private ActorService		actorService;


	// Constructor ---------------------------------------------------------------
	public SecretaryPresidentController() {
		super();
	}

	// Create -------------------------------------
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView create() {

		final FormSecretary formSecretary = this.secretaryService.createForm();
		final ModelAndView result = new ModelAndView("secretary/register");
		result.addObject("formSecretary", formSecretary);
		return result;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@ModelAttribute("formSecretary") final FormSecretary formSecretary, final BindingResult binding, final boolean terms, final String repassword) {
		ModelAndView result;
		final String redirectView;

		final Secretary secretary = this.secretaryService.reconstruct(formSecretary, binding);
		if (binding.hasErrors())
			result = this.createEditModelAndView(formSecretary);
		else
			try {
				final String username = secretary.getUserAccount().getUsername();

				if (this.actorService.letraDNI(secretary.getDNI()) == false)
					result = this.createEditModelAndView(formSecretary, "actor.commit.error6");

				else if (this.actorService.existsUsername(username))
					result = this.createEditModelAndView(formSecretary, "actor.commit.error");
				else if (this.actorService.existsEmail(secretary.getEmail()))
					result = this.createEditModelAndView(formSecretary, "actor.commit.error5");

				else if (!terms)
					result = this.createEditModelAndView(formSecretary, "actor.error.terms");
				else if (!repassword.equals(secretary.getUserAccount().getPassword()))
					result = this.createEditModelAndView(formSecretary, "actor.pass");
				else {
					this.secretaryService.save(secretary);
					redirectView = "redirect:../../security/login.do";
					result = new ModelAndView(redirectView);
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(formSecretary, "actor.commit.error2");
			}

		return result;
	}
	// Other Ancillary Methods -------------------------------------
	protected ModelAndView createEditModelAndView(final FormSecretary secretary) {
		ModelAndView result;
		result = this.createEditModelAndView(secretary, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final FormSecretary secretary, final String message) {
		ModelAndView result;
		result = new ModelAndView("secretary/register");
		result.addObject("formSecretary", secretary);
		result.addObject("message", message);
		return result;
	}

}
