
package controllers.president;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ReunionService;
import controllers.AbstractController;
import domain.Reunion;

@Controller
@RequestMapping("/reunion/president")
public class ReunionPresidentController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private ReunionService	reunionService;


	// Constructor ---------------------------------------------------------------
	public ReunionPresidentController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView result;
		result = new ModelAndView("activity/list");
		result.addObject("activities", this.reunionService.findAll());
		result.addObject("uri", "reunion/president/list.do");
		result.addObject("isReunion", true);

		return result;

	}

	// Delete --------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@RequestParam final int actId) throws Exception {
		ModelAndView result;

		final Reunion l = this.reunionService.findOne(actId);

		try {
			this.reunionService.delete(l);
		} catch (final Throwable oops) {
			result = new ModelAndView("activity/list");
			result.addObject("activities", this.reunionService.findAll());
			result.addObject("isReunion", true);
			result.addObject("message", "leisure.error");
		}
		result = new ModelAndView("activity/list");
		result.addObject("activities", this.reunionService.findAll());
		result.addObject("isReunion", true);

		return result;
	}

	// Create --------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView create() {
		Reunion reunion;
		reunion = this.reunionService.create();
		final ModelAndView result = new ModelAndView("activity/edit");
		result.addObject("modelAttribute", "reunion");
		result.addObject("reunion", reunion);
		result.addObject("new1", true);
		result.addObject("uri", "reunion/president/edit.do");
		result.addObject("isPassed", false);
		return result;
	}

	// Modify --------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "edit")
	public ModelAndView edit(@Valid final Integer actId) {
		final ModelAndView result;
		Reunion reunion;
		reunion = this.reunionService.findOne(actId);
		final Collection<String> estado;
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		if (reunion.getMoment().before(currentDate))
			estado = Arrays.asList("CLOSED");
		else
			estado = Arrays.asList("OPEN", "CANCELLED");

		result = new ModelAndView("activity/edit");
		result.addObject("modelAttribute", "reunion");
		result.addObject("reunion", reunion);
		result.addObject("estado", estado);
		result.addObject("new1", false);
		result.addObject("uri", "reunion/president/edit.do");
		result.addObject("isPassed", reunion.getMoment().before(currentDate));

		return result;
	}
	// Save ----------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView create(@Valid final Reunion reunion, final BindingResult binding, final boolean isPassed) {
		ModelAndView result;
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		if ((reunion.getId() > 0 && this.reunionService.findOne(reunion.getId()).getMoment().before(currentDate) && reunion.getStatus().equals("OPEN"))
			|| (reunion.getId() > 0 && this.reunionService.findOne(reunion.getId()).getMoment().before(currentDate) && reunion.getStatus().equals("CANCELLED")))
			result = this.createEditModelAndView(reunion, "leisure.passed");
		else if (binding.hasErrors())
			result = this.createEditModelAndView(reunion);
		else
			try {
				if (reunion.getMoment().before(currentDate) && isPassed == false)

					result = this.createEditModelAndView(reunion, "leisure.notpast");
				else {
					final Reunion reunionf = this.reunionService.reconstruct(reunion, binding);

					this.reunionService.save(reunionf);
					result = new ModelAndView("activity/list");
					result.addObject("activities", this.reunionService.findAll());
					result.addObject("isReunion", true);
					result.addObject("uri", "reunion/president/list.do");
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(reunion, "leisure.error");
			}

		return result;
	}
	protected ModelAndView createEditModelAndView(final Reunion reunion) {
		ModelAndView result;
		result = this.createEditModelAndView(reunion, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final Reunion reunion, final String message) {

		final ModelAndView result = new ModelAndView("activity/edit");
		result.addObject("modelAttribute", "reunion");
		result.addObject("reunion", reunion);
		result.addObject("message", message);
		result.addObject("uri", "reunion/president/edit.do");
		Collection<String> estado = new ArrayList<String>();
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		if (reunion.getId() > 0)
			result.addObject("new1", false);
		else
			result.addObject("new1", true);
		if (reunion.getMoment() != null && reunion.getId() > 0)
			result.addObject("isPassed", this.reunionService.findOne(reunion.getId()).getMoment().before(currentDate));
		else
			result.addObject("isPassed", false);

		if (reunion.getId() > 0)
			if (this.reunionService.findOne(reunion.getId()).getMoment().before(currentDate))
				estado = Arrays.asList("CLOSED");
			else
				estado = Arrays.asList("OPEN", "CANCELLED");
		result.addObject("estado", estado);
		return result;
	}
}
