
package controllers;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AssociateService;
import services.LeisureService;
import services.RequestService;
import domain.Associate;
import domain.Leisure;
import domain.President;
import domain.Request;

@Controller
@RequestMapping("/leisure")
public class LeisureController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private LeisureService		leisureService;
	@Autowired
	private ActorService		actorService;
	@Autowired
	private AssociateService	associateService;
	@Autowired
	private RequestService		requestService;


	// Constructor ---------------------------------------------------------------
	public LeisureController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final Collection<Leisure> leisures = this.leisureService.findAll();

		final ModelAndView result = new ModelAndView("activity/list");
		result.addObject("activities", leisures);
		result.addObject("uri", "leisure/list.do");
		result.addObject("isLeisure", true);
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		result.addObject("currentDate", currentDate);
		return result;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView list(final Integer leisureId) {
		if (leisureId == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		final Leisure leisure = this.leisureService.findOne(leisureId);
		final ModelAndView result;
		if (leisure == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		else {

			result = new ModelAndView("activity/profile");

			final Date currentDate = new Date();
			currentDate.setTime(currentDate.getTime() + 86400000);
			result.addObject("fecha", currentDate);
			result.addObject("isLeisure", true);

			result.addObject("activity", leisure);
			if (!this.actorService.isAnonymous() && this.actorService.findByPrincipal() instanceof Associate)
				if (this.actorService.findByPrincipal() instanceof Associate) {
					result.addObject("associateId", this.actorService.findByPrincipal().getId());
					final Request request = this.requestService.findequest(this.associateService.findByPrincipal().getId(), leisure.getId());
					if (request == null)
						result.addObject("hasRequest", true);
					else {
						result.addObject("hasRequest", false);
						result.addObject("requestStatus", request.getStatus());
					}
				}

			if (!this.actorService.isAnonymous() && this.actorService.findByPrincipal() instanceof President)
				result.addObject("isPresident", true);

		}
		return result;
	}
}
