
package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.VolunteerService;
import domain.Volunteer;
import forms.FormVolunteer;

@Controller
@RequestMapping("/volunteer")
public class VolunteerController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private VolunteerService	volunteerService;
	@Autowired
	private ActorService		actorService;


	// Constructor ---------------------------------------------------------------
	public VolunteerController() {
		super();
	}

	// Create -------------------------------------
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView create() {

		final FormVolunteer formVolunteer = this.volunteerService.createForm();
		final ModelAndView result = new ModelAndView("volunteer/register");
		result.addObject("formVolunteer", formVolunteer);
		return result;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@ModelAttribute("formVolunteer") final FormVolunteer formVolunteer, final BindingResult binding, final boolean terms, final String repassword) {
		ModelAndView result;
		final String redirectView;

		final Volunteer volunteer = this.volunteerService.reconstruct(formVolunteer, binding);
		if (binding.hasErrors())
			result = this.createEditModelAndView(formVolunteer);
		else
			try {
				final String username = volunteer.getUserAccount().getUsername();

				if (this.actorService.letraDNI(volunteer.getDNI()) == false)
					result = this.createEditModelAndView(formVolunteer, "actor.commit.error6");
				else if (this.actorService.existsUsername(username))
					result = this.createEditModelAndView(formVolunteer, "actor.commit.error");
				else if (this.actorService.existsEmail(volunteer.getEmail()))
					result = this.createEditModelAndView(formVolunteer, "actor.commit.error5");

				else if (!terms)
					result = this.createEditModelAndView(formVolunteer, "actor.error.terms");
				else if (!repassword.equals(volunteer.getUserAccount().getPassword()))
					result = this.createEditModelAndView(formVolunteer, "actor.pass");
				else {
					this.volunteerService.save(volunteer);
					redirectView = "redirect:../security/login.do";
					result = new ModelAndView(redirectView);
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(formVolunteer, "actor.commit.error2");
			}

		return result;
	}
	// Other Ancillary Methods -------------------------------------
	protected ModelAndView createEditModelAndView(final FormVolunteer volunteer) {
		ModelAndView result;
		result = this.createEditModelAndView(volunteer, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final FormVolunteer volunteer, final String message) {
		ModelAndView result;
		result = new ModelAndView("volunteer/register");
		result.addObject("formVolunteer", volunteer);
		result.addObject("message", message);
		return result;
	}

}
