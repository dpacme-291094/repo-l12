
package controllers.secretary;

import java.util.Arrays;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.RecordService;
import services.ReunionService;
import services.SecretaryService;
import controllers.AbstractController;
import domain.Record;
import domain.Reunion;

@Controller
@RequestMapping("/reunion/secretary")
public class ReunionSecretaryController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private ReunionService		reunionService;
	@Autowired
	private SecretaryService	secretaryService;
	@Autowired
	private RecordService		recordService;


	// Constructor ---------------------------------------------------------------
	public ReunionSecretaryController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView create() {
		throw new RuntimeException("Oops! You should not be here. Go back to home page.");

	}

	@RequestMapping(value = "/profile", method = RequestMethod.POST, params = "register")
	public ModelAndView create(@Valid final int reunionId) {
		final ModelAndView result;
		final Collection<String> itemsFinal = Arrays.asList("true", "false");

		final Reunion reunion = this.reunionService.findOne(reunionId);
		final Record record = this.recordService.create(reunion, this.secretaryService.findByPrincipal());

		result = new ModelAndView("record/edit");
		result.addObject("record", record);
		result.addObject("itemsFinal", itemsFinal);

		return result;
	}
}
