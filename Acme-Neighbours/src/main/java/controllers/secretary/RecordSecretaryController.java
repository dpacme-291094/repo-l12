
package controllers.secretary;

import java.util.Arrays;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.RecordService;
import services.ReunionService;
import services.SecretaryService;
import controllers.AbstractController;
import domain.Record;

@Controller
@RequestMapping("/record/secretary")
public class RecordSecretaryController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private RecordService		recordService;
	@Autowired
	private SecretaryService	secretaryService;
	@Autowired
	private ReunionService		reunionService;


	// Constructor ---------------------------------------------------------------
	public RecordSecretaryController() {
		super();
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView create() {
		throw new RuntimeException("Oops! You should not be here. Go back to home page.");

	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final Collection<Record> records = this.secretaryService.findByPrincipal().getRecords();
		final ModelAndView result = new ModelAndView("record/list");
		result.addObject("records", records);
		result.addObject("uri", "record/secretary/list.do");
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView create(@Valid final Record r, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(r);
		else if (r == null || (r.getId() > 0 && this.recordService.findOne(r.getId()).getIsFinal() == true))
			result = this.createEditModelAndView(r, "record.already");
		else {

			final Record record = this.recordService.reconstruct(r, binding);

			try {
				this.recordService.save(record);
				result = new ModelAndView("redirect: ../../list.do");
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(r, "record.error");
			}

		}
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "edit")
	public ModelAndView edit(@Valid final Integer recordId) {
		final ModelAndView result;
		Record record;
		record = this.recordService.findOne(recordId);

		if (record == null || this.recordService.findOne(record.getId()).getIsFinal() == true) {
			result = new ModelAndView("record/list");
			final Collection<Record> records = this.secretaryService.findByPrincipal().getRecords();
			result.addObject("records", records);
			result.addObject("uri", "record/secretary/list.do");
			result.addObject("message", "record.already");
		} else
			result = this.createEditModelAndView(record);

		return result;

	}
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid final Integer recordId) {
		ModelAndView result;
		Record record;
		final String redirectView = "record/secretary/list.do";

		record = this.recordService.findOne(recordId);

		if (record == null || this.recordService.findOne(record.getId()).getIsFinal() == true) {
			result = new ModelAndView("record/list");
			final Collection<Record> records = this.secretaryService.findByPrincipal().getRecords();
			result.addObject("records", records);
			result.addObject("uri", redirectView);
			result.addObject("message", "record.already");
		} else
			try {
				this.recordService.delete(record);
				result = new ModelAndView("record/list");
				final Collection<Record> records = this.secretaryService.findByPrincipal().getRecords();
				result.addObject("records", records);
				result.addObject("uri", redirectView);
			} catch (final Throwable oops) {
				result = new ModelAndView("record/list");
				final Collection<Record> records = this.secretaryService.findByPrincipal().getRecords();
				result.addObject("records", records);
				result.addObject("uri", redirectView);
				result.addObject("message", "record.error");
			}
		return result;
	}
	// Other Ancillary Methods -------------------------------------
	private ModelAndView createEditModelAndView(final Record record) {
		ModelAndView result;
		result = this.createEditModelAndView(record, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final Record record, final String message) {
		final Collection<String> itemsFinal = Arrays.asList("true", "false");

		final ModelAndView result = new ModelAndView("record/edit");
		result.addObject("record", record);
		result.addObject("itemsFinal", itemsFinal);
		result.addObject("message", message);
		return result;
	}

}
