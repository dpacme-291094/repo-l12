
package controllers.secretary;

import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.OfferService;
import services.PetitionService;
import services.RoomService;
import services.SecretaryService;
import controllers.AbstractController;
import domain.Offer;
import domain.Room;

@Controller
@RequestMapping("/offer/secretary")
public class OfferSecretaryController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private PetitionService		petitionService;
	@Autowired
	private SecretaryService	secretaryService;
	@Autowired
	private OfferService		offerService;
	@Autowired
	private RoomService			roomService;


	// Constructor ---------	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "edit")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final ModelAndView result;

		final Collection<Offer> offers = this.secretaryService.findByPrincipal().getOffers();

		result = new ModelAndView("offer/list");
		result.addObject("offers", offers);
		result.addObject("uri", "offer/secretary/list.do");
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		result.addObject("date", currentDate);
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView edit() {
		final ModelAndView result;
		Offer offer;
		offer = this.offerService.create(null, this.secretaryService.findByPrincipal());
		final Collection<Room> rooms = this.roomService.findAll();

		result = new ModelAndView("offer/edit");
		result.addObject("offer", offer);
		result.addObject("rooms", rooms);

		return result;
	}

	// Save ----------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView create(@Valid final Offer offer, final BindingResult binding, final boolean isPassed) {
		ModelAndView result;
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);

		if (binding.hasErrors())
			result = this.createEditModelAndView(offer);
		else

			try {

				if (offer.getDateRequest().before(currentDate))
					result = this.createEditModelAndView(offer, "offer.notpast");
				else if (this.offerService.exists(offer, offer.getRoom().getOffers()))
					result = this.createEditModelAndView(offer, "offer.exists");
				else {

					final Offer res = this.offerService.reconstruct(offer, binding);
					this.offerService.save(res);
					result = new ModelAndView("redirect:../../offer/secretary/list.do");
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(offer, "offer.error");
			}

		return result;
	}
	protected ModelAndView createEditModelAndView(final Offer offer) {
		ModelAndView result;
		result = this.createEditModelAndView(offer, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final Offer offer, final String message) {

		final ModelAndView result;
		final Collection<Room> rooms = this.roomService.findAll();

		result = new ModelAndView("offer/edit");
		result.addObject("offer", offer);
		result.addObject("rooms", rooms);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "cancel")
	public ModelAndView cancel(@Valid final Integer offerId) {
		ModelAndView result;

		final Offer offer = this.offerService.findOne(offerId);

		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		final Collection<Offer> offers = this.secretaryService.findByPrincipal().getOffers();

		result = new ModelAndView("offer/list");
		result.addObject("offers", offers);
		result.addObject("uri", "offer/secretary/list.do");
		result.addObject("date", currentDate);
		if (offer.getDateRequest().before(currentDate))
			result.addObject("message", "offer.past");
		else
			this.offerService.delete(offer);

		return result;
	}
	// Create -------------------------------------

}
