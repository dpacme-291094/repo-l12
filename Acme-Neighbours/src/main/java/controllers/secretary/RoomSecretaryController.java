
package controllers.secretary;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.RoomService;
import services.SecretaryService;
import controllers.AbstractController;
import domain.Room;

@Controller
@RequestMapping("/room/secretary")
public class RoomSecretaryController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private RoomService			roomService;
	@Autowired
	private ActorService		actorService;
	@Autowired
	private SecretaryService	secretaryService;


	// Constructor ---------------------------------------------------------------
	public RoomSecretaryController() {
		super();
	}

	// Create -------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView create() {
		throw new RuntimeException("Oops! You should not be here. Go back to home page.");

	}
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final Collection<Room> rooms = this.roomService.findAll();

		final ModelAndView result = new ModelAndView("room/list");
		result.addObject("rooms", rooms);
		result.addObject("uri", "room/secretary/list.do");
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView create(@Valid final Room r, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(r);

		else {
			final Room room = this.roomService.reconstruct(r, binding);
			try {

				this.roomService.save(room);
				result = new ModelAndView("room/list");
				final Collection<Room> rooms = this.roomService.findAll();

				result.addObject("rooms", rooms);
				result.addObject("uri", "room/secretary/list.do");

			} catch (final Throwable oops) {
				result = this.createEditModelAndView(room, "room.error");
			}
		}
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "edit")
	public ModelAndView edit(@Valid final Integer roomId) {
		final ModelAndView result;
		Room room;
		room = this.roomService.findOne(roomId);

		result = new ModelAndView("room/edit");
		result.addObject("room", room);

		return result;
	}

	// Other Ancillary Methods -------------------------------------
	protected ModelAndView createEditModelAndView(final Room room) {
		ModelAndView result;
		result = this.createEditModelAndView(room, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final Room room, final String message) {

		final ModelAndView result = new ModelAndView("room/edit");
		result.addObject("room", room);
		result.addObject("message", message);
		return result;
	}

}
