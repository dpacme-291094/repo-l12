
package controllers.secretary;

import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.PetitionService;
import services.SecretaryService;
import controllers.AbstractController;
import domain.Petition;

@Controller
@RequestMapping("/petition/secretary")
public class PetitionSecretaryController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private PetitionService		petitionService;
	@Autowired
	private SecretaryService	secretaryService;


	// Constructor ---------------------------------------------------------------
	public PetitionSecretaryController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final Collection<Petition> petitions = this.petitionService.findPetitions(this.secretaryService.findByPrincipal().getId());//this.petitionService.findPetitions(this.secretaryService.findByPrincipal().getId());

		final ModelAndView result = new ModelAndView("petition/list");
		result.addObject("petitions", petitions);
		result.addObject("uri", "petition/secretary/list.do");
		return result;
	}

	// Create -------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "accept")
	public ModelAndView register(@Valid final Integer petitionId) {
		final ModelAndView result;
		final Petition s = this.petitionService.findOne(petitionId);
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);

		final Collection<Petition> petitions = this.petitionService.findPetitions(this.secretaryService.findByPrincipal().getId());//this.petitionService.findPetitions(this.secretaryService.findByPrincipal().getId());

		result = new ModelAndView("petition/list");
		result.addObject("petitions", petitions);
		result.addObject("uri", "petition/secretary/list.do");

		if (s.getOffer().getDateRequest().before(currentDate))
			result.addObject("message", "leisure.notpast");
		else if (s.getOffer().getStatus().equals("OCCUPIED"))
			result.addObject("message", "offer.late");
		else
			this.petitionService.accept(s);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "deny")
	public ModelAndView delete(@Valid final Integer petitionId) {
		final ModelAndView result;
		final Petition s = this.petitionService.findOne(petitionId);

		final Collection<Petition> petitions = this.petitionService.findPetitions(this.secretaryService.findByPrincipal().getId());//this.petitionService.findPetitions(this.secretaryService.findByPrincipal().getId());

		result = new ModelAndView("petition/list");
		result.addObject("petitions", petitions);
		result.addObject("uri", "petition/secretary/list.do");

		this.petitionService.deny(s);
		return result;
	}

}
