
package controllers.treasurer;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ConfigurationService;
import controllers.AbstractController;
import domain.Configuration;

@Controller
@RequestMapping("/configuration/treasurer")
public class ConfigurationTreasurerController extends AbstractController {

	// Services ---------------------------------------

	@Autowired
	private ConfigurationService	configurationService;


	// Constructors -----------------------------------

	public ConfigurationTreasurerController() {
		super();
	}

	// view -----------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		final Configuration configuration = this.configurationService.findAll();

		result = new ModelAndView("configuration/edit");
		result.addObject("configuration", configuration);

		return result;
	}

	// Save ----------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Configuration configuration, final BindingResult binding) {
		ModelAndView result;
		result = new ModelAndView("configuration/edit");
		if (binding.hasErrors()) {
			result.addObject("configuration", configuration);
			result.addObject("message", null);
		} else {
			configuration = this.configurationService.reconstruct(configuration, binding);
			try {

				configuration = this.configurationService.save(configuration);
				result.addObject("configuration", configuration);
				result.addObject("message1", "configuration.commit.ok");

			} catch (final Throwable oops) {
				result.addObject("configuration", configuration);
				result.addObject("message", "configuration.commit.error");
			}
		}
		return result;
	}
}
