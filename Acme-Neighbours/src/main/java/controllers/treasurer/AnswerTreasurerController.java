
package controllers.treasurer;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AnswerService;
import services.SuggestionService;
import services.TreasurerService;
import controllers.AbstractController;
import domain.Answer;
import domain.Suggestion;
import domain.Treasurer;

@Controller
@RequestMapping("/answer/treasurer")
public class AnswerTreasurerController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private AnswerService		answerService;
	@Autowired
	private ActorService		actorService;
	@Autowired
	private SuggestionService	suggestionService;
	@Autowired
	private TreasurerService	treasurerService;


	// Constructor ---------------------------------------------------------------
	public AnswerTreasurerController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final Collection<Suggestion> suggestions = this.suggestionService.findNotAnswered();
		final ModelAndView result = new ModelAndView("suggestion/list");
		result.addObject("suggestions", suggestions);
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "answer")
	public ModelAndView answer(@Valid final Integer suggestionId) {
		ModelAndView result;
		final Suggestion s = this.suggestionService.findOne(suggestionId);
		final Treasurer c = this.treasurerService.findByPrincipal();

		try {
			if (s == null) {
				final Collection<Suggestion> suggestions = this.suggestionService.findNotAnswered();
				result = new ModelAndView("suggestion/list");
				result.addObject("suggestions", suggestions);
				result.addObject("message", "suggestion.deleted");
			} else if (s.getAnswer() != null) {

				final Collection<Suggestion> suggestions = this.suggestionService.findNotAnswered();
				result = new ModelAndView("suggestion/list");
				result.addObject("suggestions", suggestions);
				result.addObject("message", "answer.already");
			} else {
				final Answer a = this.answerService.create(s, c);
				result = new ModelAndView("answer/create");
				result.addObject("answer", a);
			}
		} catch (final Throwable oops) {

			final Collection<Suggestion> suggestions = this.suggestionService.findNotAnswered();
			result = new ModelAndView("suggestion/list");
			result.addObject("suggestions", suggestions);
			result.addObject("message", "answer.error");
		}

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView create(@Valid final Answer answer, final BindingResult binding) {
		ModelAndView result;
		final String redirectView;

		if (binding.hasErrors())
			result = this.createEditModelAndView(answer);
		else
			try {

				this.answerService.save(answer);
				redirectView = "redirect:../../suggestion/treasurer/list.do";
				result = new ModelAndView(redirectView);

			} catch (final Throwable oops) {
				result = this.createEditModelAndView(answer, "answer.error");
			}

		return result;
	}
	// Other Ancillary Methods -------------------------------------
	protected ModelAndView createEditModelAndView(final Answer answer) {
		ModelAndView result;
		result = this.createEditModelAndView(answer, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final Answer answer, final String message) {

		final ModelAndView result = new ModelAndView("answer/create");
		result.addObject("answer", answer);
		result.addObject("message", message);
		return result;
	}

}
