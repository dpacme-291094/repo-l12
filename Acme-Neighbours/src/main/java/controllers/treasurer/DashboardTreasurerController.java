
package controllers.treasurer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActivityService;
import services.AnswerService;
import services.AssociateService;
import services.LeisureService;
import controllers.AbstractController;

@Controller
@RequestMapping("/dashboard/treasurer")
public class DashboardTreasurerController extends AbstractController {

	@Autowired
	private ActivityService		activityService;
	@Autowired
	private LeisureService		leisureService;
	@Autowired
	private AssociateService	associateService;
	@Autowired
	private AnswerService		answerService;


	// Services ---------------------------------------

	// Constructors -----------------------------------

	public DashboardTreasurerController() {
		super();
	}

	// view -----------------------------------------
	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;

		result = new ModelAndView("report/dashboard");

		//The total amount of money that the association has won the former year vs. the current year regarding leisure activities.
		result.addObject("QueryA11", this.activityService.lastYearPayment());
		result.addObject("QueryA12", this.activityService.thisYearPayment());
		//The minimum, average and maximum price of leisure activities.
		result.addObject("QueryA2", this.leisureService.maxMinPriceLeisure());

		//The highest amount of money that an associate has spent in the association.
		result.addObject("QueryA3", this.leisureService.MaxPriceLeisure());

		//The number of suggestions not answerer vs. answered.
		result.addObject("QueryA41", this.answerService.maxAnswered());
		result.addObject("QueryA42", this.answerService.maxNotAnswered());
		//A list of associates followed by the amount of money that they owe, regarding monthly payments.
		result.addObject("QueryA5", this.associateService.totalAssociates());

		result.addObject("esTesorero", 1);

		return result;
	}
}
