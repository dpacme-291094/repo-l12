
package controllers.treasurer;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AnswerService;
import services.SuggestionService;
import services.TreasurerService;
import controllers.AbstractController;
import domain.Suggestion;

@Controller
@RequestMapping("/suggestion/treasurer")
public class SuggestionTreasurerController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private SuggestionService	suggestionService;
	@Autowired
	private TreasurerService	treasurerService;
	@Autowired
	private AnswerService		answerService;
	@Autowired
	private ActorService		actorService;


	// Constructor ---------------------------------------------------------------
	public SuggestionTreasurerController() {
		super();
	}

	// Create -------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView create() {

		final Collection<Suggestion> suggestions = this.suggestionService.findNotAnswered();
		final ModelAndView result = new ModelAndView("suggestion/list");
		result.addObject("suggestions", suggestions);
		return result;
	}

	// Other Ancillary Methods -------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid final Integer suggestionId) {
		ModelAndView result;
		final Suggestion s = this.suggestionService.findOne(suggestionId);

		try {

			if (s == null) {
				final Collection<Suggestion> suggestions = this.suggestionService.findNotAnswered();
				result = new ModelAndView("suggestion/list");
				result.addObject("suggestions", suggestions);
				result.addObject("message", "suggestion.deleted");
			} else if (s.getAnswer() != null) {

				final Collection<Suggestion> suggestions = this.suggestionService.findNotAnswered();
				result = new ModelAndView("suggestion/list");
				result.addObject("suggestions", suggestions);
				result.addObject("message", "suggestion.deleted2");
			} else
				this.suggestionService.delete(s);
			result = new ModelAndView("suggestion/list");
			final Collection<Suggestion> suggestions = this.suggestionService.findNotAnswered();

			result.addObject("suggestions", suggestions);
		} catch (final Throwable oops) {

			final Collection<Suggestion> suggestions = this.suggestionService.findNotAnswered();
			result = new ModelAndView("suggestion/list");
			result.addObject("suggestions", suggestions);
			result.addObject("message", "suggestion.error2");
		}

		return result;
	}
}
