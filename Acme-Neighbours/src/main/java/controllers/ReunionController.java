
package controllers;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AssociateService;
import services.RequestService;
import services.ReunionService;
import domain.Associate;
import domain.President;
import domain.Request;
import domain.Reunion;

@Controller
@RequestMapping("/reunion")
public class ReunionController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private ReunionService		reunionService;
	@Autowired
	private AssociateService	associateService;
	@Autowired
	private RequestService		requestService;
	@Autowired
	private ActorService		actorService;


	// Constructor ---------------------------------------------------------------
	public ReunionController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final Collection<Reunion> reunions = this.reunionService.findAll();

		final ModelAndView result = new ModelAndView("activity/list");
		result.addObject("activities", reunions);
		result.addObject("uri", "reunion/list.do");
		result.addObject("isReunion", true);
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		result.addObject("currentDate", currentDate);
		return result;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView list(final Integer reunionId) {
		if (reunionId == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		final Reunion reunion = this.reunionService.findOne(reunionId);
		final ModelAndView result;
		if (reunion == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		else {

			result = new ModelAndView("activity/profile");
			result.addObject("activity", reunion);

			if (!this.actorService.isAnonymous() && this.actorService.findByPrincipal() instanceof Associate) {
				result.addObject("associateId", this.actorService.findByPrincipal().getId());

				final Request request = this.requestService.findequest(this.associateService.findByPrincipal().getId(), reunion.getId());
				if (request == null)
					result.addObject("hasRequest", true);
				else {
					result.addObject("hasRequest", false);
					result.addObject("requestStatus", request.getStatus());
				}
			}
			result.addObject("isReunion", true);
			final Date currentDate = new Date();
			currentDate.setTime(currentDate.getTime() + 86400000);
			result.addObject("fecha", currentDate);

			if (!this.actorService.isAnonymous() && this.actorService.findByPrincipal() instanceof President)
				result.addObject("isPresident", true);

		}
		return result;
	}
}
