
package controllers.actor;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.MessageService;
import controllers.AbstractController;
import domain.Actor;
import domain.Associate;
import domain.Message;
import forms.FormMessage;

@Controller
@RequestMapping("/message/actor")
public class MessageActorController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private MessageService	messageService;

	@Autowired
	private ActorService	actorService;


	// Constructors -----------------------------------------------------------

	public MessageActorController() {
		super();
	}

	// Listing ----------------------------------------------------------------

	@RequestMapping(value = "/send", method = RequestMethod.GET)
	public ModelAndView create() {
		throw new RuntimeException("Oops! You should not be here. Go back to home page.");

	}
	@RequestMapping(value = "/inbox", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Message> messages;
		final Actor a = this.actorService.findByPrincipal();

		messages = this.messageService.findByActorRecipient(a);

		result = new ModelAndView("message/inbox");

		result.addObject("messages", messages);
		result.addObject("whereIAm", "i");
		result.addObject("ruta", "message/actor/inbox.do");

		return result;
	}

	@RequestMapping(value = "/outbox", method = RequestMethod.GET)
	public ModelAndView list2() {
		ModelAndView result;
		Collection<Message> messages;
		final Actor a = this.actorService.findByPrincipal();

		messages = this.messageService.findByActorSender(a);

		result = new ModelAndView("message/outbox");

		result.addObject("messages", messages);
		result.addObject("whereIAm", "o");
		result.addObject("ruta", "message/actor/outbox.do");
		return result;
	}

	// Read -------------------------------------------------------------------

	@RequestMapping(value = "/read", method = RequestMethod.GET)
	public ModelAndView read(@RequestParam final int id, @RequestParam final String l) {
		ModelAndView result;
		Message m2;
		if (this.messageService.findOne(id) == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");
		else {
			final Actor me = this.actorService.findByPrincipal();
			if (id != 0
				&& this.messageService.findOne(id) != null
				&& ((this.messageService.findOne(id).getActorRecipient() != null && this.messageService.findOne(id).getActorRecipient().equals(me)) || (this.messageService.findOne(id).getActorSender() != null && this.messageService.findOne(id)
					.getActorSender().equals(me)))) {
				m2 = this.messageService.findOne(id);
				final FormMessage formMessage = new FormMessage(m2);

				result = new ModelAndView("message/edit");

				result.addObject("formMessage", formMessage);
				String ruta = "/message/actor/inbox.do";
				if (l.equals("o"))
					ruta = "/message/actor/outbox.do";
				result.addObject("ruta", ruta);
			} else {
				String aux = "/message/actor/inbox.do";
				if (l.equals("o"))
					aux = "/message/actor/outbox.do";
				result = new ModelAndView(aux);
			}
		}
		return result;
	}

	// Create -----------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(defaultValue = "i") final String l) {
		ModelAndView result;
		final Message m2 = this.messageService.create();
		final FormMessage formMessage = new FormMessage(m2);
		String ruta = "/message/actor/inbox.do";
		if (l.equals("o"))
			ruta = "/message/actor/outbox.do";

		result = new ModelAndView("message/edit");
		result.addObject("ruta", ruta);
		result.addObject("formMessage", formMessage);
		result.addObject("create", true);

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ModelAndView create2(@RequestParam(defaultValue = "0") final String id) {
		ModelAndView result;
		//Assert.isTrue(StringUtils.isNumeric(id));
		//Assert.notNull(Integer.parseInt(id));
		if (!StringUtils.isNumeric(id))
			result = new ModelAndView("redirect:../..");
		else {
			final Actor recipient = this.actorService.findOne(Integer.parseInt(id));
			final Message m2 = this.messageService.create();
			if (recipient != null) {
				m2.setActorRecipient(recipient);
				m2.setEmailRecipient(recipient.getEmail());
			}
			final FormMessage formMessage = new FormMessage(m2);
			String ruta = "message/actor/outbox.do";
			if (id.equals("0"))
				ruta = "message/actor/inbox.do";

			result = new ModelAndView("message/edit");
			result.addObject("ruta", ruta);
			result.addObject("formMessage", formMessage);
			result.addObject("create", true);

		}
		return result;
	}

	// Forward ----------------------------------------------------------------

	@RequestMapping(value = "/send", method = RequestMethod.POST, params = "forward")
	public ModelAndView forward(@RequestParam final int id, @RequestParam final String whereIAm) {
		ModelAndView result;
		final Actor me = this.actorService.findByPrincipal();

		final Message m2 = this.messageService.findOne(id);
		final FormMessage forwarder = new FormMessage(m2);
		if ((m2.getActorRecipient() != null && m2.getActorRecipient().equals(me)) || (m2.getActorSender() != null && m2.getActorSender().equals(me))) {
			final Message formMessage = this.messageService.create();
			formMessage.setAttachments(forwarder.getAttachments());
			formMessage.setText(forwarder.getText());
			formMessage.setSubject(forwarder.getSubject());
			String ruta = "/message/actor/inbox.do";
			if (whereIAm.equals("o"))
				ruta = "/message/actor/outbox.do";

			result = new ModelAndView("message/edit");
			result.addObject("ruta", ruta);
			result.addObject("formMessage", formMessage);
			result.addObject("create", true);
		} else {
			String aux = "/message/actor/inbox.do";
			if (whereIAm.equals("o"))
				aux = "/message/actor/outbox.do";
			result = new ModelAndView(aux);
		}

		return result;
	}

	// Replay ----------------------------------------------------------------

	@RequestMapping(value = "/send", method = RequestMethod.POST, params = "reply")
	public ModelAndView reply(@RequestParam final int id, @RequestParam final String whereIAm) {
		ModelAndView result;
		final Actor me = this.actorService.findByPrincipal();

		final Message m2 = this.messageService.findOne(id);
		final FormMessage replicant = new FormMessage(m2);
		if ((m2.getActorRecipient() != null && m2.getActorRecipient().equals(me)) || (m2.getActorSender() != null && m2.getActorSender().equals(me))) {
			final Message formMessage = this.messageService.create();
			formMessage.setEmailRecipient(replicant.getEmailSender());
			formMessage.setAttachments(replicant.getAttachments());
			formMessage.setSubject("RE: " + replicant.getSubject());
			String ruta = "/message/actor/inbox.do";
			if (whereIAm.equals("o"))
				ruta = "/message/actor/outbox.do";

			result = new ModelAndView("message/edit");
			result.addObject("ruta", ruta);
			result.addObject("formMessage", formMessage);
			result.addObject("create", true);
		} else {
			String aux = "/message/actor/inbox.do";
			if (whereIAm.equals("o"))
				aux = "/message/actor/outbox.do";
			result = new ModelAndView(aux);
		}

		return result;
	}

	// Delete ----------------------------------------------------------------

	@RequestMapping(value = "/delete", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@RequestParam final int id, @RequestParam final String whereIAm) throws Exception {
		ModelAndView result;
		Collection<Message> messages;
		final Actor me = this.actorService.findByPrincipal();
		if (whereIAm.equals("i")) {
			result = new ModelAndView("message/inbox");
			messages = this.messageService.findByActorRecipient(me);
			result.addObject("ruta", "/message/actor/inbox.do");
		} else {
			result = new ModelAndView("message/outbox");
			messages = this.messageService.findByActorSender(me);
			result.addObject("ruta", "/message/actor/outbox.do");

		}

		final String error = "message.commit.error";

		try {
			final Message m = this.messageService.findOne(id);

			if ((m.getActorRecipient() != null && m.getActorRecipient().equals(me)) || (m.getActorSender() != null && m.getActorSender().equals(me))) {
				if (whereIAm.equals("i"))
					this.messageService.delete(m, false);
				else
					this.messageService.delete(m, true);
				messages.remove(m);
				result.addObject("messages", messages);
				result.addObject("whereIAm", whereIAm);
				result.addObject("message1", "message.commit.ok");

			} else {

				result.addObject("messages", messages);
				result.addObject("whereIAm", whereIAm);
				result.addObject("message", error);
			}

		} catch (final Throwable oops) {
			result.addObject("messages", messages);
			result.addObject("whereIAm", whereIAm);
			result.addObject("message", error);
		}

		return result;
	}

	// Send ----------------------------------------------------------------

	@RequestMapping(value = "/send", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute("formMessage") final FormMessage formMessage, final BindingResult binding) {
		ModelAndView result;
		final Message mensaje = this.messageService.reconstruct(formMessage, binding);
		if (binding.hasErrors()) {
			result = new ModelAndView("message/edit");

			result.addObject("formMessage", formMessage);
			result.addObject("create", true);
			result.addObject("ruta", "/message/actor/outbox.do");
			result.addObject("message", null);
		} else {
			Actor recipient;
			boolean fine = true;
			String error = "";
			if (mensaje.getAttachments().length() != 0) {
				final String[] urls = mensaje.getAttachments().split(",");
				for (final String url : urls)
					if (!MessageActorController.isUrl(url)) {
						fine = false;
						error = "message.error.url";
						break;
					}
			}
			if (mensaje.getActorRecipient() instanceof Associate && mensaje.getActorSender() instanceof Associate) {
				fine = false;
				error = "message.actor.notassociate";
			}
			recipient = this.actorService.findByEmail(mensaje.getEmailRecipient());
			if (recipient == null) {
				fine = false;
				error = "message.actor.notfound";
			}
			if (!fine) {
				result = new ModelAndView("message/edit");

				result.addObject("formMessage", formMessage);
				result.addObject("create", true);
				result.addObject("ruta", "/message/actor/inbox.do");
				result.addObject("message", error);
			} else {

				this.messageService.save(mensaje);

				result = new ModelAndView("redirect:../actor/outbox.do");

				result.addObject("message1", "message.commit.ok");

			}
		}

		return result;
	}

	private static boolean isUrl(final String s) {
		final String regex = "(https?:\\/\\/(?:www\\.)[^\\s\\.]+\\.[^\\s]{2,}|ftp?:\\/\\/[^\\s\\.]+\\.[^\\s]{2,}|http?:\\/\\/[^\\s\\.]+\\.[^\\s]{2,})";

		try {
			final Pattern patt = Pattern.compile(regex);
			final Matcher matcher = patt.matcher(s);
			return matcher.matches();

		} catch (final RuntimeException e) {
			return false;
		}
	}
}
