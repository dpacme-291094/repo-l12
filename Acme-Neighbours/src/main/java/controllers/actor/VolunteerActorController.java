
package controllers.actor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.VolunteerService;
import controllers.AbstractController;
import domain.Actor;

@Controller
@RequestMapping("/volunteer/actor")
public class VolunteerActorController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private VolunteerService	volunteerService;


	// Constructor ---------------------------------------------------------------
	public VolunteerActorController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView list(final Integer volunteerId) {
		if (volunteerId == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");
		final Actor volunteer = this.volunteerService.findOne(volunteerId);

		final ModelAndView result;
		if (volunteer == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		else {

			result = new ModelAndView("volunteer/profile");
			result.addObject("volunteer", volunteer);
		}
		return result;
	}

}
