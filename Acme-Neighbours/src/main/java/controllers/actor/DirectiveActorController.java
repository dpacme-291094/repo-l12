
package controllers.actor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.PresidentService;
import services.SecretaryService;
import services.TreasurerService;
import controllers.AbstractController;

@Controller
@RequestMapping("/directive/actor")
public class DirectiveActorController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private PresidentService	presidentService;
	@Autowired
	private SecretaryService	secretaryService;
	@Autowired
	private TreasurerService	treasurerService;


	// Constructor ---------------------------------------------------------------
	public DirectiveActorController() {
		super();
	}

	// Create -------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView result;
		result = new ModelAndView("associate/privilegedActorslist");
		result.addObject("uri", "directive/actor/list.do");
		result.addObject("president", this.presidentService.findAdministrator());
		result.addObject("secretaries", this.secretaryService.findAll());
		result.addObject("treasurers", this.treasurerService.findAll());

		return result;

	}
}
