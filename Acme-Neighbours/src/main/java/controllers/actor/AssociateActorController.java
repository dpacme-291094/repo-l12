
package controllers.actor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.AssociateService;
import controllers.AbstractController;
import domain.Associate;

@Controller
@RequestMapping("/associate/actor")
public class AssociateActorController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private AssociateService	associateService;


	// Constructor ---------------------------------------------------------------
	public AssociateActorController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView list(final Integer associateId) {
		if (associateId == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");
		final Associate associate = this.associateService.findOne(associateId);

		final ModelAndView result;
		if (associate == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		else {

			result = new ModelAndView("associate/profile");
			result.addObject("associate", associate);
		}
		return result;
	}

}
