
package controllers.actor;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.CommentService;
import controllers.AbstractController;
import controllers.CourseController;
import controllers.LeisureController;
import controllers.ReunionController;
import domain.Activity;
import domain.Comment;
import domain.Leisure;
import domain.Reunion;

@Controller
@RequestMapping("/comment/actor")
public class CommentActorController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private CommentService		commentService;
	@Autowired
	private ReunionController	reunionService;
	@Autowired
	private CourseController	courseService;
	@Autowired
	private LeisureController	leisureService;


	// Constructor ---------------------------------------------------------------
	public CommentActorController() {
		super();
	}

	// Create -------------------------------------
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView create() {
		throw new RuntimeException("Oops! You should not be here. Go back to home page.");

	}

	// Other Ancilliary Methods ---------------

	@RequestMapping(value = "/delete", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid final Integer commentId) {
		ModelAndView result;
		final Comment c;

		c = this.commentService.findOne(commentId);
		final Activity activity = c.getActivity();
		if (activity instanceof Reunion)
			result = this.reunionService.list(c.getActivity().getId());
		else if (activity instanceof Leisure)
			result = this.leisureService.list(c.getActivity().getId());
		else
			result = this.courseService.list(c.getActivity().getId());
		try {

			this.commentService.delete(c);

		} catch (final Throwable oops) {

			result.addObject("message", "comment.error");

		}
		return result;
	}
}
