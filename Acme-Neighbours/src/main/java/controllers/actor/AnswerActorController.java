
package controllers.actor;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AnswerService;
import services.SuggestionService;
import services.TreasurerService;
import controllers.AbstractController;
import domain.Suggestion;

@Controller
@RequestMapping("/answer/actor")
public class AnswerActorController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private AnswerService		answerService;
	@Autowired
	private ActorService		actorService;
	@Autowired
	private SuggestionService	suggestionService;
	@Autowired
	private TreasurerService	treasurerService;


	// Constructor ---------------------------------------------------------------
	public AnswerActorController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView create() {

		final Collection<Suggestion> suggestions = this.suggestionService.findAnswered();
		final ModelAndView result = new ModelAndView("answer/list");
		result.addObject("suggestions", suggestions);
		return result;
	}

}
