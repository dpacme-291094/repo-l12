
package controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.RecordService;
import domain.Record;

@Controller
@RequestMapping("/record")
public class RecordController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private RecordService	recordService;


	// Constructor ---------------------------------------------------------------
	public RecordController() {
		super();
	}

	// View

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView create() {
		throw new RuntimeException("Oops! You should not be here. Go back to home page.");

	}

	@RequestMapping(value = "/view", method = RequestMethod.POST, params = "view")
	public ModelAndView edit(@Valid final Integer recordId) {
		if (recordId == null)

			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		final ModelAndView result;
		Record record;
		record = this.recordService.findOne(recordId);
		if (record == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		result = new ModelAndView("record/view");
		result.addObject("record", record);
		return result;

	}

}
