
package controllers.volunteer;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.VolunteerService;
import controllers.AbstractController;
import domain.Volunteer;

@Controller
@RequestMapping("/volunteer")
public class ProfileVolunteerController extends AbstractController {

	@Autowired
	private VolunteerService	volunteerService;
	@Autowired
	private ActorService		actorService;


	public ProfileVolunteerController() {
		super();
	}

	@RequestMapping(value = "/private", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView result;

		final Volunteer volunteer = this.volunteerService.findByPrincipal();

		result = this.createEditModelAndView(volunteer);

		return result;
	}

	@RequestMapping(value = "/private", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Volunteer volunteer, final BindingResult binding) {

		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(volunteer);
		else
			try {
				if (this.actorService.letraDNI(volunteer.getDNI()) == false)
					result = this.createEditModelAndView2(volunteer, "actor.commit.error6");
				else {
					final Volunteer a = this.volunteerService.reconstruct(volunteer, binding);

					final Volunteer le = this.volunteerService.edit(a);
					result = new ModelAndView("volunteer/private");
					result.addObject("volunteer", le);

					result.addObject("message1", "actor.commit.success");
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView2(volunteer, "actor.commit.error2");
			}
		return result;
	}
	protected ModelAndView createEditModelAndView(final Volunteer volunteer) {

		ModelAndView result;

		result = this.createEditModelAndView2(volunteer, null);

		return result;
	}

	protected ModelAndView createEditModelAndView2(final Volunteer volunteer, final String message) {
		ModelAndView result;

		result = new ModelAndView("volunteer/private");
		result.addObject("volunteer", volunteer);
		result.addObject("message", message);
		return result;
	}

}
