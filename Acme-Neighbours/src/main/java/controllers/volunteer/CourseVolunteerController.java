
package controllers.volunteer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.CourseService;
import services.VolunteerService;
import controllers.AbstractController;
import domain.Course;

@Controller
@RequestMapping("/course/volunteer")
public class CourseVolunteerController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private CourseService		courseService;
	@Autowired
	private VolunteerService	volunteerService;


	// Constructor ---------------------------------------------------------------
	public CourseVolunteerController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView result;
		result = new ModelAndView("activity/list");
		result.addObject("activities", this.volunteerService.findByPrincipal().getCourses());
		result.addObject("isCourse", true);
		result.addObject("uri", "course/volunteer/list.do");

		return result;

	}
	// Delete --------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@RequestParam final int actId) throws Exception {
		ModelAndView result;

		final Course l = this.courseService.findOne(actId);
		try {
			this.courseService.delete(l);
		} catch (final Throwable oops) {
			result = new ModelAndView("activity/list");
			result.addObject("activities", this.volunteerService.findByPrincipal().getCourses());
			result.addObject("isCourse", true);
			result.addObject("message", "leisure.error");
		}
		result = new ModelAndView("activity/list");
		result.addObject("activities", this.volunteerService.findByPrincipal().getCourses());
		result.addObject("isCourse", true);

		return result;
	}
	// Create --------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView create() {
		Course course;
		course = this.courseService.create(this.volunteerService.findByPrincipal());

		final ModelAndView result = new ModelAndView("activity/edit");

		result.addObject("isCourse", true);
		result.addObject("isPassed", false);
		result.addObject("modelAttribute", "course");
		result.addObject("course", course);
		result.addObject("new1", true);
		result.addObject("uri", "course/volunteer/edit.do");
		return result;
	}

	// Modify --------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "edit")
	public ModelAndView edit(@Valid final Integer actId) {
		final ModelAndView result;
		Course course;
		course = this.courseService.findOne(actId);
		final Collection<String> estado;
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		if (course.getMoment().before(currentDate))
			estado = Arrays.asList("CLOSED");
		else
			estado = Arrays.asList("OPEN", "CANCELLED");

		result = new ModelAndView("activity/edit");
		result.addObject("estado", estado);
		result.addObject("modelAttribute", "course");
		result.addObject("course", course);
		result.addObject("isCourse", true);
		result.addObject("new1", false);
		result.addObject("isPassed", course.getMoment().before(currentDate));
		result.addObject("uri", "course/volunteer/edit.do");

		return result;
	}

	// Save ----------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView create(@Valid final Course course, final BindingResult binding, final boolean isPassed) {
		ModelAndView result;
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		if ((course.getId() > 0 && this.courseService.findOne(course.getId()).getMoment().before(currentDate) && course.getStatus().equals("OPEN"))
			|| (course.getId() > 0 && this.courseService.findOne(course.getId()).getMoment().before(currentDate) && course.getStatus().equals("CANCELLED")))
			result = this.createEditModelAndView(course, "leisure.passed");
		else if (binding.hasErrors())
			result = this.createEditModelAndView(course);
		else
			try {

				if (course.getMoment().before(currentDate) && isPassed == false)
					result = this.createEditModelAndView(course, "leisure.notpast");
				else {
					final Course coursef = this.courseService.reconstruct(course, binding);
					this.courseService.save(coursef);
					result = new ModelAndView("activity/list");
					result.addObject("activities", this.volunteerService.findByPrincipal().getCourses());

					result.addObject("isCourse", true);
					result.addObject("uri", "course/volunteer/list.do");
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(course, "leisure.error");
			}
		return result;
	}
	protected ModelAndView createEditModelAndView(final Course activity) {
		ModelAndView result;
		result = this.createEditModelAndView(activity, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final Course course, final String message) {

		final ModelAndView result = new ModelAndView("activity/edit");
		result.addObject("modelAttribute", "course");
		result.addObject("course", course);
		result.addObject("message", message);
		result.addObject("isCourse", true);
		result.addObject("uri", "course/volunteer/edit.do");
		Collection<String> estado = new ArrayList<String>();
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		if (course.getId() > 0)
			result.addObject("new1", false);
		else
			result.addObject("new1", true);
		if (course.getMoment() != null && course.getId() > 0)
			result.addObject("isPassed", this.courseService.findOne(course.getId()).getMoment().before(currentDate));
		else
			result.addObject("isPassed", false);

		if (course.getId() > 0)
			if (this.courseService.findOne(course.getId()).getMoment().before(currentDate))
				estado = Arrays.asList("CLOSED");
			else
				estado = Arrays.asList("OPEN", "CANCELLED");
		result.addObject("estado", estado);
		return result;
	}
}
