
package controllers.volunteer;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.AssociateService;
import services.RequestService;
import controllers.AbstractController;
import domain.Request;

@Controller
@RequestMapping("/associate/volunteer")
public class AssociateVolunteerController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private AssociateService	associateService;

	@Autowired
	private RequestService		requestService;


	// Constructor ---------------------------------------------------------------
	public AssociateVolunteerController() {
		super();
	}

	@RequestMapping(value = "/courseList", method = RequestMethod.GET)
	public ModelAndView courseList(final Integer courseId) {
		if (courseId == null)

			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		ModelAndView result;
		result = new ModelAndView("associate/attendeesList");

		final Collection<Request> ca = this.requestService.requestAccepted(courseId);
		result.addObject("requests", ca);
		result.addObject("uri", "associate/volunteer/courseList.do");
		result.addObject("seats", this.requestService.requestTotalSeats(courseId));

		return result;

	}
}
