
package controllers.volunteer;

import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.RequestService;
import services.VolunteerService;
import controllers.AbstractController;
import domain.Request;

@Controller
@RequestMapping("/request/volunteer")
public class RequestVolunteerController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private RequestService		requestService;
	@Autowired
	private VolunteerService	volunteerService;


	// Constructor ---------------------------------------------------------------
	public RequestVolunteerController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		final Collection<Request> requests = this.requestService.findCourseRequests(this.volunteerService.findByPrincipal().getId());

		final ModelAndView result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("uri", "request/volunteer/list.do");
		return result;
	}
	// Create -------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "accept")
	public ModelAndView register(@Valid final Integer requestId) {
		final ModelAndView result;
		final Request s = this.requestService.findOne(requestId);
		final Collection<Request> requests = this.requestService.findCourseRequests(this.volunteerService.findByPrincipal().getId());

		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("uri", "request/volunteer/list.do");

		if (s.getActivity().getSeats() < s.getSeats())
			result.addObject("message", "activity.noseats");
		else if (s.getActivity().getMoment().before(currentDate))
			result.addObject("message", "leisure.notpast");
		else if (s.getActivity().getStatus().equals("CANCELLED") || s.getActivity().getStatus().equals("CLOSED"))
			result.addObject("message", "activity.late");
		else
			this.requestService.accept(s);

		return result;
	}
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "deny")
	public ModelAndView delete(@Valid final Integer requestId) {
		final ModelAndView result;
		final Request s = this.requestService.findOne(requestId);

		final Collection<Request> requests = this.requestService.findCourseRequests(this.volunteerService.findByPrincipal().getId());

		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("uri", "request/volunteer/list.do");

		this.requestService.deny(s);
		return result;
	}
}
