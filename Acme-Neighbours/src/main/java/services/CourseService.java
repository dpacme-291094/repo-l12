
package services;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.CourseRepository;
import domain.Actor;
import domain.Comment;
import domain.Course;
import domain.President;
import domain.Request;
import domain.Volunteer;

@Transactional
@Service
public class CourseService {

	// Managed repository -------------------------------
	@Autowired
	private CourseRepository	courseRepository;
	@Autowired
	private RequestService		requestService;
	@Autowired
	private ActivityService		activityService;
	@Autowired
	private ActorService		actorService;
	// Validator -------------------------------
	@Autowired
	private Validator			validator;


	// Constructor --------------------------------------
	public CourseService() {
		super();
	}

	// Simple CRUD methods ------------------------------
	public Course create(final Volunteer volunteer) {
		final Course c = new Course();
		c.setVolunteer(volunteer);
		c.setDescription("");
		c.setComments(new HashSet<Comment>());
		c.setMoment(new Date());
		c.setTitle("");
		c.setStatus("OPEN");
		c.setSeats(1);
		c.setTotalDuration("");
		c.setRequests(new HashSet<Request>());

		return c;
	}

	public Course save(final Course c) {
		Assert.notNull(c);
		Assert.isTrue(this.actorService.findByPrincipal() instanceof President || this.actorService.findByPrincipal() instanceof Volunteer);

		if (this.actorService.findByPrincipal() instanceof President || this.actorService.findByPrincipal() instanceof Volunteer)
			try {
				if (c.getId() != 0)
					if (c.getStatus().equals("OPEN"))
						this.activityService.avisarModificacionSocios(this.requestService.requestActors(c.getId()), "El curso " + c.getTitle() + " fue modificado, por favor revise la informacion del evento", "Modificacion de actividad",
							this.actorService.findByPrincipal());
					else if (c.getStatus().equals("CANCELLED")) {
						final Collection<Actor> ca = this.requestService.requestActors(c.getId());

						if (this.actorService.findByPrincipal() instanceof President)
							ca.add(c.getVolunteer());

						this.activityService.avisarModificacionSocios(ca, "El curso " + c.getTitle() + " ha sido cancelado. Sentimos las molestias.", "Cancelaci�n de actividad", this.actorService.findByPrincipal());
					}
			} catch (final Throwable oops) {
			}
		return this.courseRepository.save(c);
	}
	public void delete(final Course c) {
		Assert.isTrue(this.actorService.findByPrincipal() instanceof President || this.actorService.findByPrincipal() instanceof Volunteer);

		for (final Request a : c.getRequests())
			this.requestService.delete(a);
		this.courseRepository.delete(c);
	}

	public Course findOne(final Integer id) {
		return this.courseRepository.findOne(id);
	}

	public Collection<Course> findAll() {
		return this.courseRepository.findAll();
	}

	// Other business methods ---------------------------

	public Course reconstruct(final Course course, final BindingResult binding) {
		Course result;

		if (course.getId() == 0)
			result = course;
		else {
			result = this.courseRepository.findOne(course.getId());
			result.setTitle(course.getTitle());
			result.setDescription(course.getDescription());
			result.setImage(course.getImage());
			result.setMoment(course.getMoment());
			result.setStatus(course.getStatus());
			result.setSeats(course.getSeats());
			result.setVolunteer(course.getVolunteer());
			result.setTotalDuration(course.getTotalDuration());
			// TODEL. Think about I have to write relationships
			result.setComments(course.getComments());
			result.setRequests(course.getRequests());
			this.validator.validate(result, binding);
		}

		return result;
	}

	public void flush() {
		this.courseRepository.flush();
	}

	public Object[] cursoMasAsistido() {
		final LinkedList<Object[]> valor = new LinkedList<Object[]>();
		valor.addAll(this.courseRepository.cursoMasAsistido());

		final Object[] o = valor.get(0);

		return o;
	}

	//B Queries
	//Query 1
	//El voluntario que m�s cursos tiene relacionados.
	public Collection<Object[]> volunteerMoreCourses() {
		return this.courseRepository.volunteerMoreCourses();
	}

	//Query 4
	//El curso al que mas socios han acudido.
	public Collection<Object[]> courseMoreAssistant() {
		return this.courseRepository.courseMoreAssistant();
	}
}
