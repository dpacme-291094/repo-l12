
package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ActivityRepository;
import domain.Activity;
import domain.Actor;
import domain.Message;

@Transactional
@Service
public class ActivityService {

	// Managed repository -------------------------------
	@Autowired
	private ActivityRepository	activityRepository;
	@Autowired
	private MessageService		messageService;


	// Constructor --------------------------------------
	public ActivityService() {
		super();

	}

	public Activity save(final Activity a) {
		Assert.notNull(a);
		return this.activityRepository.save(a);
	}

	// Simple CRUD methods ------------------------------

	public Activity findOne(final Integer id) {
		return this.activityRepository.findOne(id);
	}

	public Collection<Activity> findAll() {
		return this.activityRepository.findAll();
	}

	// Other business methods ---------------------------

	public void flush() {
		this.activityRepository.flush();
	}

	public void avisarModificacionSocios(final Collection<Actor> e, final String mensaje, final String subject, final Actor m) {

		for (final Actor c : e)
			this.avisoModificacion(c, mensaje, subject, m);
	}

	public void avisoModificacion(final Actor c, final String mensaje, final String subject, final Actor m) {

		Message aviso = this.messageService.create();
		aviso.setMoment(new Date());
		aviso.setSubject(subject);
		aviso.setText(mensaje);
		aviso.setEmailSender(m.getEmail());
		aviso.setEmailRecipient(c.getEmail());
		aviso.setAttachments("");
		aviso.setActorRecipient(c);
		aviso.setActorSender(m);
		try {
			aviso = this.messageService.save(aviso);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
		}
	}

	public Double thisYearPayment() {
		if (this.activityRepository.thisYearPayment() == null)

			return 0.;
		else
			return this.activityRepository.thisYearPayment();
	}

	public Double lastYearPayment() {
		if (this.activityRepository.lastYearPayment() == null)

			return 0.;
		else
			return this.activityRepository.lastYearPayment();
	}

}
