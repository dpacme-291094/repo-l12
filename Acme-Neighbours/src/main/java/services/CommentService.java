
package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.CommentRepository;
import domain.Activity;
import domain.Actor;
import domain.Associate;
import domain.Comment;

@Transactional
@Service
public class CommentService {

	// Managed repository -------------------------------
	@Autowired
	private CommentRepository	commentRepository;

	// Services -------------------------------
	@Autowired
	private ActivityService		activityService;
	@Autowired
	private ActorService		actorService;
	@Autowired
	private AssociateService	associateService;

	// Validator -------------------------------
	@Autowired
	private Validator			validator;


	// Suporting services --------------------------------

	// Constructor --------------------------------------
	public CommentService() {
		super();
	}

	// Simple CRUD methods ------------------------------
	public Comment create(final Activity a, final Associate b) {
		final Comment result = new Comment();
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() - 60000);
		result.setTitle("");
		result.setDescription("");
		result.setActivity(a);
		result.setAssociate(b);
		result.setMoment(currentDate);
		return result;
	}

	public Comment save(final Comment c) {
		Assert.notNull(c);

		final Comment savedComment = this.commentRepository.save(c);
		final Activity activity = savedComment.getActivity();
		final Collection<Comment> commentsActivity = activity.getComments();
		commentsActivity.add(savedComment);
		activity.setComments(commentsActivity);
		this.activityService.save(activity);
		final Associate associate = savedComment.getAssociate();
		final Collection<Comment> commentsAssociate = associate.getComments();
		commentsAssociate.add(savedComment);
		this.associateService.edit(associate);

		return savedComment;
	}
	public Comment findOne(final Integer id) {
		return this.commentRepository.findOne(id);
	}

	public void delete(final Comment comment) {
		Assert.notNull(comment);
		final Actor actor = this.actorService.findByPrincipal();
		if (actor instanceof Associate)
			this.checkPrincipal(comment);

		final Activity activity = comment.getActivity();
		final Collection<Comment> commentsActivity = activity.getComments();
		final Associate associate = comment.getAssociate();
		final Collection<Comment> commentsAssociate = associate.getComments();

		this.commentRepository.delete(comment);

		commentsActivity.remove(comment);
		activity.setComments(commentsActivity);
		this.activityService.save(activity);

		commentsAssociate.remove(comment);
		associate.setComments(commentsAssociate);
		this.associateService.edit(associate);

	}

	// Other business methods ---------------------------

	public Comment reconstruct(final Comment comment, final BindingResult binding) {
		final Comment result;

		if (comment.getId() == 0)
			result = comment;
		else {
			result = this.commentRepository.findOne(comment.getId());
			result.setTitle(comment.getTitle());
			result.setMoment(comment.getMoment());
			result.setDescription(comment.getDescription());
			// TODEL. Think about I have to write relationships
			result.setAssociate(comment.getAssociate());
			result.setActivity(comment.getActivity());
			this.validator.validate(result, binding);
		}

		return result;
	}

	public void flush() {
		this.commentRepository.flush();
	}

	public void checkPrincipal(final Comment c) {
		final Actor associate = this.actorService.findByPrincipal();
		Assert.isTrue((c.getAssociate() != null && c.getAssociate().equals(associate)));
	}

}
