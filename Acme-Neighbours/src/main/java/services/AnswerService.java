
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import repositories.AnswerRepository;
import domain.Answer;
import domain.Suggestion;
import domain.Treasurer;

@Transactional
@Service
public class AnswerService {

	// Managed repository -------------------------------
	@Autowired
	private AnswerRepository	answerRepository;


	// Constructor --------------------------------------
	public AnswerService() {
		super();

	}

	public Answer create(final Suggestion s, final Treasurer t) {

		final Answer a = new Answer();
		a.setSuggestion(s);
		a.setTreasurer(t);
		a.setText("");
		return a;
	}

	public Answer save(final Answer a) {
		Assert.notNull(a);
		return this.answerRepository.save(a);
	}

	public void delete(final Answer a) {

		this.answerRepository.delete(a);
	}

	// Simple CRUD methods ------------------------------

	public Answer findOne(final Integer id) {
		return this.answerRepository.findOne(id);
	}

	public Collection<Answer> findAll() {
		return this.answerRepository.findAll();
	}

	// Other business methods ---------------------------

	public void flush() {
		this.answerRepository.flush();
	}

	public Answer reconstruct(final Answer answer, final BindingResult binding) {
		final Answer result;

		if (answer.getId() == 0)
			result = answer;
		else {
			result = this.answerRepository.findOne(answer.getId());
			result.setText(answer.getText());
		}

		return result;
	}

	public Integer maxAnswered() {
		return this.answerRepository.maxAnswerer();
	}

	public Integer maxNotAnswered() {
		return this.answerRepository.maxNotAnswerer();
	}

}
