
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ActorRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;

@Transactional
@Service
public class ActorService {

	// Managed repository -------------------------------
	@Autowired
	private ActorRepository	actorRepository;

	@Autowired
	private LoginService	loginService;


	// Constructor --------------------------------------
	public ActorService() {
		super();

	}

	public Actor save(final Actor a) {
		Assert.notNull(a);
		return this.actorRepository.save(a);
	}

	// Simple CRUD methods ------------------------------

	public Actor findOne(final Integer id) {
		return this.actorRepository.findOne(id);
	}

	public Collection<Actor> findAll() {
		return this.actorRepository.findAll();
	}

	// Other business methods ---------------------------

	@SuppressWarnings("static-access")
	public Actor findByPrincipal() {
		UserAccount userAccount;
		Actor result;

		userAccount = this.loginService.getPrincipal();
		result = this.findByUserAccount(userAccount);
		return result;
	}

	public Actor findByUserAccount(final UserAccount userAccount) {
		Actor result;

		result = this.actorRepository.findByUserAccount(userAccount.getId());
		return result;

	}

	public Actor findByEmail(final String email) {
		Assert.notNull(email);
		return this.actorRepository.findByEmail(email);
	}

	public boolean existsUsername(final String username) {
		boolean result = false;
		long howManyUsers;

		howManyUsers = this.actorRepository.countUsersWithUsername(username);

		if (howManyUsers != 0)
			result = true;

		return result;
	}

	public boolean existsEmail(final String email) {
		boolean result = false;
		long howManyUsers;

		howManyUsers = this.actorRepository.countUsersWithEmail(email);

		if (howManyUsers != 0)
			result = true;

		return result;
	}

	@SuppressWarnings("static-access")
	public boolean isAnonymous() {
		return this.loginService.isAnonymous();
	}

	public void flush() {
		this.actorRepository.flush();
	}

	public Actor findByUsername(final String username) {
		return this.actorRepository.findByUsername(username);
	}

	public boolean letraDNI(final String dni) {
		boolean res;
		final String letra = dni.substring(8);
		final int miDNI = Integer.parseInt(dni.substring(0, 8));
		int resto = 0;
		String miLetra = "";
		final String[] asignacionLetra = {
			"T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"
		};

		resto = miDNI % 23;

		miLetra = asignacionLetra[resto];

		if (miLetra.toLowerCase().equals(letra.toLowerCase()))
			res = true;
		else
			res = false;

		return res;
	}

	public Collection<Actor> allPrivilegedActors() {
		return this.actorRepository.allPrivilegeActors();
	}
}
