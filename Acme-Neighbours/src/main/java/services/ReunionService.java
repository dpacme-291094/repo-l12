
package services;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ReunionRepository;
import domain.Comment;
import domain.President;
import domain.Request;
import domain.Reunion;

@Transactional
@Service
public class ReunionService {

	// Managed repository -------------------------------
	@Autowired
	private ReunionRepository	reunionRepository;
	@Autowired
	private RequestService		requestService;
	@Autowired
	private RecordService		recordService;
	@Autowired
	private Validator			validator;
	@Autowired
	private ActivityService		activityService;
	@Autowired
	private ActorService		actorService;


	// Constructor --------------------------------------
	public ReunionService() {
		super();

	}

	public Reunion create() {

		final Reunion a = new Reunion();
		a.setDescription("");
		a.setComments(new HashSet<Comment>());
		a.setSeats(1);
		a.setMoment(new Date());
		a.setTitle("");
		a.setStatus("OPEN");
		a.setTotalDuration("");
		a.setRequests(new HashSet<Request>());
		return a;
	}

	public Reunion save(final Reunion c) {
		Assert.notNull(c);
		Assert.isTrue(this.actorService.findByPrincipal() instanceof President);

		if (this.actorService.findByPrincipal() instanceof President)
			try {
				if (c.getId() != 0 && c.getRequests().size() > 0)
					if (c.getStatus().equals("OPEN"))
						this.activityService.avisarModificacionSocios(this.requestService.requestActors(c.getId()), "La reunion " + c.getTitle() + " fue modificada, por favor revise la informacion del evento", "Modificacion de actividad",
							this.actorService.findByPrincipal());
					else if (c.getStatus().equals("CANCELLED"))
						this.activityService.avisarModificacionSocios(this.requestService.requestActors(c.getId()), "La reunion " + c.getTitle() + " ha sido cancelada. Sentimos las molestias.", "Cancelaci�n de actividad",
							this.actorService.findByPrincipal());
			} catch (final Throwable oops) {
			}

		return this.reunionRepository.save(c);
	}

	public void delete(final Reunion a) {
		Assert.isTrue(this.actorService.findByPrincipal() instanceof President);

		for (final Request b : a.getRequests())
			this.requestService.delete(b);
		if (a.getRecord() != null)
			this.recordService.delete(a.getRecord());

		this.reunionRepository.delete(a);
	}

	// Simple CRUD methods ------------------------------

	public Reunion findOne(final Integer id) {
		return this.reunionRepository.findOne(id);
	}

	public Collection<Reunion> findAll() {
		return this.reunionRepository.findAll();
	}

	// Other business methods ---------------------------

	public void flush() {
		this.reunionRepository.flush();
	}

	public Reunion reconstruct(final Reunion reunion, final BindingResult binding) {
		final Reunion result;

		if (reunion.getId() == 0)
			result = reunion;
		else {
			result = this.reunionRepository.findOne(reunion.getId());
			result.setDescription(reunion.getDescription());
			result.setSeats(reunion.getSeats());
			result.setMoment(reunion.getMoment());
			result.setTitle(reunion.getTitle());
			result.setStatus(reunion.getStatus());
			result.setTotalDuration(reunion.getTotalDuration());
			this.validator.validate(result, binding);
		}

		return result;
	}

	public Collection<Reunion> findReunionBeforeCurrentDate() {
		return this.reunionRepository.findReunionBeforeCurrentDate();
	}

	//Queries C
	//Query 6
	//El m�nimo, la media y el m�ximo n�mero de asistentes a las reuniones.
	public Collection<Object> minMaxAvgReunion() {
		return this.reunionRepository.minMaxAvgReunion();
	}
}
