
package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import repositories.OfferRepository;
import domain.Offer;
import domain.Petition;
import domain.Room;
import domain.Secretary;

@Transactional
@Service
public class OfferService {

	// Managed repository -------------------------------
	@Autowired
	private OfferRepository	offerRepository;
	@Autowired
	private ActorService	actorService;
	@Autowired
	private RoomService		roomService;
	@Autowired
	private PetitionService	petitionService;


	// Constructor --------------------------------------
	public OfferService() {
		super();

	}

	public Offer create(final Room s, final Secretary t) {

		final Offer a = new Offer();
		a.setRoom(s);
		a.setSecretary(t);
		a.setDateRequest(new Date());
		a.setStatus("FREE");
		a.setTime("");
		return a;
	}

	public Offer save(final Offer a) {
		Assert.notNull(a);
		return this.offerRepository.save(a);
	}

	public void delete(final Offer c) {
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		Assert.isTrue(c.getDateRequest().after(currentDate));
		if (this.petitionService.findPetition(c.getId()) != null) {
			final Petition a = this.petitionService.findPetition(c.getId());
			this.roomService.avisoModificacion(a.getAssociate(), "Se ha cancelado la oferta sobre la sala " + c.getRoom().getTitle() + " que tenia reservada. Siento las molestias.", "Cancelación reserva.", this.actorService.findByPrincipal());

		}

		for (final Petition f : c.getPetitions())
			this.petitionService.delete(f);

		this.offerRepository.delete(c);
	}
	// Simple CRUD methods ------------------------------

	public Offer findOne(final Integer id) {
		return this.offerRepository.findOne(id);
	}

	public Collection<Offer> findAll() {
		return this.offerRepository.findAll();
	}

	// Other business methods ---------------------------

	public void flush() {
		this.offerRepository.flush();
	}

	public Offer reconstruct(final Offer offer, final BindingResult binding) {
		final Offer result;

		if (offer.getId() == 0)
			result = offer;
		else {
			result = this.offerRepository.findOne(offer.getId());
			result.setDateRequest(offer.getDateRequest());
			result.setStatus(offer.getStatus());
			result.setTime(offer.getTime());
		}

		return result;
	}

	public boolean exists(final Offer o, final Collection<Offer> offers) {
		boolean res = false;
		final String[] urlSplitted = o.getTime().split(":");

		final Integer hours = Integer.parseInt(urlSplitted[0]);
		final Integer minits = Integer.parseInt(urlSplitted[1]);

		@SuppressWarnings("deprecation")
		final Date currentDate = new Date(o.getDateRequest().getYear(), o.getDateRequest().getMonth(), o.getDateRequest().getDate(), o.getDateRequest().getHours(), o.getDateRequest().getMinutes(), o.getDateRequest().getSeconds());
		currentDate.setTime(currentDate.getTime() + (hours * 3600000) + (minits * 60000));
		final Date first = new Date(o.getDateRequest().getYear(), o.getDateRequest().getMonth(), o.getDateRequest().getDate(), o.getDateRequest().getHours(), o.getDateRequest().getMinutes(), o.getDateRequest().getSeconds());
		;
		for (final Offer a : offers) {
			final String[] urlSplitted1 = a.getTime().split(":");
			final Integer hours1 = Integer.parseInt(urlSplitted1[0]);
			final Integer minits1 = Integer.parseInt(urlSplitted1[1]);

			@SuppressWarnings("deprecation")
			final Date currentDate1 = new Date(a.getDateRequest().getYear(), a.getDateRequest().getMonth(), a.getDateRequest().getDate(), a.getDateRequest().getHours(), a.getDateRequest().getMinutes(), a.getDateRequest().getSeconds());
			currentDate1.setTime(currentDate1.getTime() + (hours1 * 3600000) + (minits1 * 60000));
			final Date second = new Date(a.getDateRequest().getYear(), a.getDateRequest().getMonth(), a.getDateRequest().getDate(), a.getDateRequest().getHours(), a.getDateRequest().getMinutes(), a.getDateRequest().getSeconds());
			if ((currentDate1.after(first)) && (currentDate1.before(currentDate) || currentDate1.equals(currentDate))) {
				res = true;
				break;
			}

			if ((second.equals(first) || second.after(first)) && second.before(currentDate)) {
				res = true;
				break;
			}

			if ((second.equals(first) || second.before(first)) && (currentDate1.after(currentDate) || currentDate1.equals(currentDate))) {
				res = true;
				break;
			}
		}
		return res;
	}
}
