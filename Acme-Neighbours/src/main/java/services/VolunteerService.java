
package services;

import java.util.Collection;
import java.util.HashSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.VolunteerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Course;
import domain.Message;
import domain.Volunteer;
import forms.FormVolunteer;

@Service
@Transactional
public class VolunteerService {

	@Autowired
	private VolunteerRepository	volunteerRepository;

	@Autowired
	private LoginService		loginService;

	@Autowired
	private Validator			validator;


	// Constructor --------------------------------------
	public VolunteerService() {
		super();

	}
	public Volunteer create() {
		final Volunteer c = new Volunteer();
		c.setCourses(new HashSet<Course>());

		return c;
	}
	public Volunteer save(final Volunteer c) {
		Assert.notNull(c);
		Volunteer volunteer;
		String password;
		String hash;

		password = c.getUserAccount().getPassword();
		hash = this.encodePassword(password);
		c.getUserAccount().setPassword(hash);

		volunteer = this.volunteerRepository.save(c);
		return volunteer;
	}
	public Volunteer edit(final Volunteer l) {
		Assert.notNull(l);

		return this.volunteerRepository.save(l);
	}

	private String encodePassword(final String password) {
		Md5PasswordEncoder encoder;
		String result;

		if (password == null || "".equals(password))
			result = null;
		else {
			encoder = new Md5PasswordEncoder();
			result = encoder.encodePassword(password, null);
		}

		return result;
	}

	// Simple CRUD methods ------------------------------

	public Actor findOne(final Integer id) {
		return this.volunteerRepository.findOne(id);
	}

	public Collection<Volunteer> findAll() {
		return this.volunteerRepository.findAll();
	}

	// Other business methods ---------------------------

	@SuppressWarnings("static-access")
	public Volunteer findByPrincipal() {
		UserAccount userAccount;
		Volunteer result;

		userAccount = this.loginService.getPrincipal();
		result = this.findByUserAccount(userAccount);
		return result;
	}

	public Volunteer findByUserAccount(final UserAccount userAccount) {
		Volunteer result;

		result = this.volunteerRepository.findByUserAccount(userAccount.getId());
		return result;

	}

	public Volunteer reconstruct(final FormVolunteer formVolunteer, final BindingResult binding) {

		final Volunteer result = new Volunteer();
		result.setId(0);
		result.setVersion(0);
		result.setName(formVolunteer.getName());
		result.setSurname(formVolunteer.getSurname());
		final UserAccount userAccount = formVolunteer.getUserAccount();
		result.setUserAccount(userAccount);
		result.setPhone(formVolunteer.getPhone());
		result.setEmail(formVolunteer.getEmail());
		result.setDNI(formVolunteer.getDNI());
		result.setMessagesIncoming(new HashSet<Message>());
		result.setMessagesOutgoing(new HashSet<Message>());
		result.setCourses(new HashSet<Course>());
		this.validator.validate(result, binding);
		return result;
	}

	public Volunteer reconstruct(final Volunteer volunteer, final BindingResult binding) {
		Volunteer result;

		if (volunteer.getId() == 0)
			result = volunteer;
		else {
			result = this.volunteerRepository.findOne(volunteer.getId());
			result.setName(volunteer.getName());
			result.setSurname(volunteer.getSurname());
			result.setEmail(volunteer.getEmail());
			result.setPhone(volunteer.getPhone());
			result.setDNI(volunteer.getDNI());

			this.validator.validate(result, binding);
		}

		return result;
	}

	public FormVolunteer createForm() {
		final FormVolunteer formVolunteer = new FormVolunteer();

		final Authority a = new Authority();
		final UserAccount userAccount = new UserAccount();
		a.setAuthority("VOLUNTEER");
		userAccount.addAuthority(a);
		formVolunteer.setUserAccount(userAccount);

		return formVolunteer;
	}

	public Object[] voluntarioMasCursos() {
		return this.volunteerRepository.voluntarioMasCursos();
	}
	public void flush() {
		this.volunteerRepository.flush();
	}
}
