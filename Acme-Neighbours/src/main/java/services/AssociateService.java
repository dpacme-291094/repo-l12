
package services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.AssociateRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Associate;
import domain.Comment;
import domain.Message;
import domain.Petition;
import domain.Request;
import forms.FormAssociate;

@Service
@Transactional
public class AssociateService {

	@Autowired
	private AssociateRepository		associateRepository;

	@Autowired
	private LoginService			loginService;
	@Autowired
	private ConfigurationService	configService;

	@Autowired
	private Validator				validator;


	// Constructor --------------------------------------
	public AssociateService() {
		super();

	}
	public Associate create() {
		final Associate c = new Associate();
		c.setPetitions(new HashSet<Petition>());
		c.setRequests(new HashSet<Request>());
		c.setComments(new HashSet<Comment>());
		return c;
	}
	public Associate save(final Associate c) {
		Assert.notNull(c);
		Associate associate;
		String password;
		String hash;

		password = c.getUserAccount().getPassword();
		hash = this.encodePassword(password);
		c.getUserAccount().setPassword(hash);

		associate = this.associateRepository.save(c);
		return associate;
	}
	public Associate edit(final Associate l) {
		Assert.notNull(l);

		return this.associateRepository.save(l);
	}

	private String encodePassword(final String password) {
		Md5PasswordEncoder encoder;
		String result;

		if (password == null || "".equals(password))
			result = null;
		else {
			encoder = new Md5PasswordEncoder();
			result = encoder.encodePassword(password, null);
		}

		return result;
	}

	// Simple CRUD methods ------------------------------

	public Associate findOne(final Integer id) {
		return this.associateRepository.findOne(id);
	}

	public Collection<Associate> findAll() {
		return this.associateRepository.findAll();
	}

	// Other business methods ---------------------------

	@SuppressWarnings("static-access")
	public Associate findByPrincipal() {
		UserAccount userAccount;
		Associate result;

		userAccount = this.loginService.getPrincipal();
		result = this.findByUserAccount(userAccount);
		return result;
	}

	public Associate findByUserAccount(final UserAccount userAccount) {
		Associate result;

		result = this.associateRepository.findByUserAccount(userAccount.getId());
		return result;

	}

	public Associate reconstruct(final FormAssociate formAssociate, final BindingResult binding) {

		final Associate result = new Associate();
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() - 600000);
		result.setId(0);
		result.setVersion(0);
		result.setName(formAssociate.getName());
		result.setSurname(formAssociate.getSurname());
		final UserAccount userAccount = formAssociate.getUserAccount();
		result.setUserAccount(userAccount);
		result.setPhone(formAssociate.getPhone());
		result.setEmail(formAssociate.getEmail());
		result.setDNI(formAssociate.getDNI());
		result.setTotalFee(this.configService.findAll().getFee());
		result.setLastPayment(currentDate);
		result.setCreditCard(formAssociate.getCreditCard());
		result.setMessagesIncoming(new HashSet<Message>());
		result.setMessagesOutgoing(new HashSet<Message>());
		result.setPetitions(new HashSet<Petition>());
		result.setRequests(new HashSet<Request>());
		result.setComments(new HashSet<Comment>());
		result.setAssociateNumber(this.associateRepository.findAll().size() + 1);
		this.validator.validate(result, binding);
		return result;
	}

	public Associate reconstruct(final Associate associate, final BindingResult binding) {
		Associate result;

		if (associate.getId() == 0)
			result = associate;
		else {
			result = this.associateRepository.findOne(associate.getId());
			result.setName(associate.getName());
			result.setSurname(associate.getSurname());
			result.setEmail(associate.getEmail());
			result.setPhone(associate.getPhone());
			result.setDNI(associate.getDNI());

			this.validator.validate(result, binding);
		}

		return result;
	}

	public FormAssociate createForm() {
		final FormAssociate formAssociate = new FormAssociate();

		final Authority a = new Authority();
		final UserAccount userAccount = new UserAccount();
		a.setAuthority("ASSOCIATE");
		userAccount.addAuthority(a);
		formAssociate.setUserAccount(userAccount);
		return formAssociate;
	}

	public String ocultarNumCC(final String numero) {
		final char[] res = numero.toCharArray();

		for (int i = 0; i < numero.length() - 4; i++)
			res[i] = '*';

		return String.valueOf(res);
	}

	//Queries C
	//Query 1
	// Media de peticiones a las actividades de ocio por socio
	public Double avgRequests() {
		return this.associateRepository.avgRequests();
	}
	//Query 2
	// El socio que ha acudido a mas reuniones vs el que menos.
	@Query("select r.associate,sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) from Request r  where r.activity in (select r2 from Reunion r2) group by r.associate order by sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) desc")
	public List<Object> maxMinAssistantsReunion() {
		final List<Object> result = new ArrayList<Object>();
		final List<Object> data = Arrays.asList(this.associateRepository.maxMinAssistantsReunion().toArray());
		if (data.size() >= 1) {
			result.add(data.get(0));
			result.add(data.get(data.size() - 1));
		}
		return result;
	}

	//Query 3
	// El socio que ha acudido a mas actividades de ocio vs el que menos.
	@Query("select r.associate,sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) from Request r  where r.activity in (select l from Leisure l) group by r.associate order by sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) desc")
	public List<Object> maxMinAssistantsLeisure() {
		final List<Object> result = new ArrayList<Object>();
		final List<Object> data = Arrays.asList(this.associateRepository.maxMinAssistantsLeisure().toArray());
		if (data.size() >= 1) {
			result.add(data.get(0));
			result.add(data.get(data.size() - 1));
		}
		return result;
	}

	//Query 7
	//Media de n�mero de comentarios por socio.
	public Double commentPerAssociate() {
		return this.associateRepository.commentPerAssociate();
	}

	public Object[] associateMasReservas() {
		return this.associateRepository.associateMasReservas();
	}

	public Object[] associateMenosReservas() {
		return this.associateRepository.associateMenosReservas();

	}
	public void flush() {
		this.associateRepository.flush();
	}

	public Associate associateToPay(final Associate associate) {

		associate.setTotalFee(this.totalMonths(associate.getLastPayment()) * this.configService.findAll().getFee());

		return this.associateRepository.save(associate);

	}

	public Integer totalMonths(final Date payment) {
		final Calendar endCalendar = new GregorianCalendar();
		endCalendar.setTime(new Date());
		final Calendar startCalendar = new GregorianCalendar();
		startCalendar.setTime(payment);

		final int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
		final int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
		return diffMonth;

	}
	public void pay(final Associate associate) {
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() - 600000);
		associate.setTotalFee(0.);
		associate.setLastPayment(currentDate);
		this.associateRepository.save(associate);

	}

	public List<Object> totalAssociates() {
		return new ArrayList<Object>(this.associateRepository.totalAssociates());

	}
}
