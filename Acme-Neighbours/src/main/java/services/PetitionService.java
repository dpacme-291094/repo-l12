
package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import repositories.PetitionRepository;
import domain.Associate;
import domain.Offer;
import domain.Petition;

@Transactional
@Service
public class PetitionService {

	// Managed repository -------------------------------
	@Autowired
	private PetitionRepository	petitionRepository;
	@Autowired
	private ActorService		actorService;
	@Autowired
	private RoomService			roomService;
	@Autowired
	private OfferService		offerService;


	// Constructor --------------------------------------
	public PetitionService() {
		super();

	}

	public Petition create(final Offer s, final Associate t) {

		final Petition a = new Petition();
		a.setOffer(s);
		a.setAssociate(t);
		a.setDescription("");
		a.setStatus("PENDING");
		return a;
	}

	public Petition save(final Petition a) {
		Assert.notNull(a);
		return this.petitionRepository.save(a);
	}

	public void delete(final Petition c) {
		if (this.actorService.findByPrincipal() instanceof Associate)
			try {
				if (c.getId() != 0)
					if (c.getStatus().equals("ACCEPTED")) {
						this.roomService.avisoModificacion(c.getOffer().getSecretary(), "Cancelo mi reserva sobre la sala " + c.getOffer().getRoom().getTitle() + ". Siento las molestias.", "Cancelación reserva.", this.actorService.findByPrincipal());
						c.getOffer().setStatus("FREE");
						this.offerService.save(c.getOffer());
					}
			} catch (final Throwable oops) {
			}
		this.petitionRepository.delete(c);
	}
	// Simple CRUD methods ------------------------------

	public Petition findOne(final Integer id) {
		return this.petitionRepository.findOne(id);
	}

	public Collection<Petition> findAll() {
		return this.petitionRepository.findAll();
	}

	// Other business methods ---------------------------

	public void flush() {
		this.petitionRepository.flush();
	}

	public Petition reconstruct(final Petition petition, final BindingResult binding) {
		final Petition result;

		if (petition.getId() == 0)
			result = petition;
		else {
			result = this.petitionRepository.findOne(petition.getId());

			result.setDescription(petition.getDescription());
			result.setStatus(petition.getStatus());

		}

		return result;
	}

	/*
	 * public Collection<Petition> findPetitions(final Integer id) {
	 * return this.petitionRepository.findPetitions(id);
	 * }
	 */
	public void accept(final Petition a) {
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() + 86400000);
		Assert.isTrue(a.getOffer().getDateRequest().after(currentDate));
		Assert.isTrue(a.getStatus().equals("PENDING"));
		a.setStatus("ACCEPTED");
		a.getOffer().setStatus("OCCUPIED");
		this.offerService.save(a.getOffer());
		this.petitionRepository.save(a);
	}

	public void deny(final Petition a) {
		Assert.isTrue(a.getStatus().equals("PENDING"));
		a.setStatus("DENIED");
		this.petitionRepository.save(a);
	}

	public Petition findPetition(final Integer id) {
		return this.petitionRepository.findPetition(id);
	}
	public Collection<Petition> findPetitions(final Integer id) {
		return this.petitionRepository.findPetitions(id);
	}
	public Petition findPetitionOffer(final Integer id, final Integer id2) {
		return this.petitionRepository.findPetitionOffer(id, id2);
	}

}
