
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.MessageRepository;
import domain.Actor;
import domain.Associate;
import domain.Message;
import forms.FormMessage;

@Service
@Transactional
public class MessageService {

	// Managed repository -------------------------------
	@Autowired
	private MessageRepository	messageRepository;

	@Autowired
	private ActorService		actorService;

	@Autowired
	private Validator			validator;


	// Constructor --------------------------------------
	public MessageService() {
		super();
	}

	// Simple CRUD methods ------------------------------
	public Message create() {

		final Message m = new Message();
		final Actor sender = this.actorService.findByPrincipal();
		m.setEmailSender(sender.getEmail());
		m.setText("");
		m.setSubject("");
		m.setEmailRecipient("");

		m.setActorSender(sender);
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() - 600000);
		m.setMoment(currentDate);

		return m;
	}

	public Message save(final Message mm) {
		if (mm.getActorRecipient() == null)
			mm.setActorRecipient(this.actorService.findByEmail(mm.getEmailRecipient()));
		Assert.isTrue(!(mm.getActorRecipient() instanceof Associate && mm.getActorSender() instanceof Associate));
		Message m = null;
		if (mm.getId() != 0)
			m = this.messageRepository.findOne(mm.getId());
		else {
			Assert.isTrue(mm.getId() == 0);
			Assert.notNull(mm);
			if (!mm.getAttachments().isEmpty())
				Assert.isTrue(this.checkUrls(mm.getAttachments()));
			final Actor recipient = this.actorService.findByEmail(mm.getEmailRecipient());
			Assert.notNull(recipient);
			final Date currentDate = new Date();
			currentDate.setTime(currentDate.getTime() - 60000);
			mm.setMoment(currentDate);
			final Collection<Message> cm = recipient.getMessagesIncoming();
			cm.add(mm);
			recipient.setMessagesIncoming(cm);
			this.actorService.save(recipient);
			final Actor connected = this.actorService.findByPrincipal();
			final Collection<Message> cm2 = connected.getMessagesOutgoing();
			cm2.add(mm);
			connected.setMessagesOutgoing(cm2);
			this.actorService.save(connected);
			m = this.messageRepository.save(mm);
		}
		return m;
	}

	public void deleteWR(final Message m) {
		this.checkPrincipal(m);
		this.messageRepository.delete(m.getId());
	}

	public void delete(final Message m, final boolean sender) {
		this.checkPrincipal(m);
		final Actor connected = this.actorService.findByPrincipal();
		if (m.getActorRecipient() == connected && !sender) {
			final Collection<Message> cm = connected.getMessagesIncoming();
			cm.remove(m);
			connected.setMessagesIncoming(cm);
			this.actorService.save(connected);
			m.setActorRecipient(null);
		} else if (m.getActorSender() == connected && sender) {
			final Collection<Message> cm = connected.getMessagesOutgoing();
			cm.remove(m);
			connected.setMessagesOutgoing(cm);
			this.actorService.save(connected);
			m.setActorSender(null);
		}
		if (m.getActorRecipient() == null && m.getActorSender() == null)
			this.messageRepository.delete(m);

	}

	public Collection<Message> findByActorSender(final Actor sender) {
		return this.messageRepository.findByActorSender(sender.getId());
	}

	public Collection<Message> findByActorRecipient(final Actor recipient) {
		return this.messageRepository.findByActorRecipient(recipient.getId());
	}

	// Other Message methods ---------------------------

	public Collection<Message> findByPrincipal() {
		Collection<Message> result = new ArrayList<Message>();
		final Actor actor = this.actorService.findByPrincipal();

		result = this.messageRepository.findByActor(actor);
		return result;
	}

	public Message findOne(final int id) {
		return this.messageRepository.findOne(id);
	}

	public Message reconstruct(final FormMessage formMessage, final BindingResult binding) {
		final Message result = new Message();

		result.setActorRecipient(this.actorService.findByEmail(formMessage.getEmailRecipient()));
		result.setActorSender(this.actorService.findByEmail(formMessage.getEmailSender()));
		result.setEmailRecipient(formMessage.getEmailRecipient());
		result.setEmailSender(formMessage.getEmailSender());
		result.setText(formMessage.getText());
		result.setSubject(formMessage.getSubject());
		result.setAttachments(formMessage.getAttachments());
		result.setMoment(formMessage.getMoment());

		this.validator.validate(result, binding);
		return result;
	}

	public Message reconstruct(final Message message, final BindingResult binding) {
		Message result = new Message();

		if (message.getId() == 0)
			result = message;
		else {
			result.setActorRecipient(message.getActorRecipient());
			result.setActorSender(message.getActorSender());
			result.setEmailRecipient(message.getEmailRecipient());
			result.setEmailSender(message.getEmailSender());
			result.setText(message.getText());
			result.setSubject(message.getSubject());
			result.setAttachments(message.getAttachments());
			result.setMoment(message.getMoment());

			this.validator.validate(result, binding);
		}

		return result;
	}

	public boolean checkUrls(final String urls) {
		boolean fine = true;
		final String[] urlSplitted = urls.split(",");
		for (final String url : urlSplitted)
			if (!MessageService.isUrl(url)) {
				fine = false;
				break;
			}
		return fine;
	}

	private static boolean isUrl(final String s) {
		final String regex = "(https?:\\/\\/(?:www\\.)[^\\s\\.]+\\.[^\\s]{2,}|ftp?:\\/\\/[^\\s\\.]+\\.[^\\s]{2,}|http?:\\/\\/[^\\s\\.]+\\.[^\\s]{2,})";

		try {
			final Pattern patt = Pattern.compile(regex);
			final Matcher matcher = patt.matcher(s);
			return matcher.matches();

		} catch (final RuntimeException e) {
			return false;
		}
	}

	public void checkPrincipal(final Message m) {
		final Actor me = this.actorService.findByPrincipal();
		Assert.isTrue(me != null && ((m.getActorRecipient() != null && m.getActorRecipient().equals(me)) || (m.getActorSender() != null && m.getActorSender().equals(me))));
	}

	public void flush() {
		this.messageRepository.flush();
	}

	//Queries C
	//Query 8
	public Collection<Object> mmaSenderPerActor() {
		return this.messageRepository.mmaSenderPerActor();
	}
	//Query 9
	public Collection<Object> mmaRecipientPerActor() {
		return this.messageRepository.mmaRecipientPerActor();
	}
}
