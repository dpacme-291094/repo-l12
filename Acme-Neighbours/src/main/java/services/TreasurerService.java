
package services;

import java.util.Collection;
import java.util.HashSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.TreasurerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Answer;
import domain.Message;
import domain.President;
import domain.Treasurer;
import forms.FormTreasurer;

@Service
@Transactional
public class TreasurerService {

	@Autowired
	private TreasurerRepository	treasurerRepository;

	@Autowired
	private LoginService		loginService;

	@Autowired
	private PresidentService	presidentService;
	@Autowired
	private Validator			validator;


	// Constructor --------------------------------------
	public TreasurerService() {
		super();

	}
	public Treasurer create() {
		this.checkPrincipal();
		final Treasurer c = new Treasurer();
		c.setAnswers(new HashSet<Answer>());

		return c;
	}
	private void checkPrincipal() {
		final President pre = this.presidentService.findByPrincipal();
		for (final Authority a : pre.getUserAccount().getAuthorities())
			Assert.isTrue(a.getAuthority().equals("PRESIDENT"));
	}
	public Treasurer save(final Treasurer c) {
		Assert.notNull(c);
		Treasurer treasurer;
		String password;
		String hash;

		password = c.getUserAccount().getPassword();
		hash = this.encodePassword(password);
		c.getUserAccount().setPassword(hash);

		treasurer = this.treasurerRepository.save(c);
		return treasurer;
	}
	public Treasurer edit(final Treasurer l) {
		Assert.notNull(l);

		return this.treasurerRepository.save(l);
	}

	private String encodePassword(final String password) {
		Md5PasswordEncoder encoder;
		String result;

		if (password == null || "".equals(password))
			result = null;
		else {
			encoder = new Md5PasswordEncoder();
			result = encoder.encodePassword(password, null);
		}

		return result;
	}

	// Simple CRUD methods ------------------------------

	public Actor findOne(final Integer id) {
		return this.treasurerRepository.findOne(id);
	}

	public Collection<Treasurer> findAll() {
		return this.treasurerRepository.findAll();
	}

	// Other business methods ---------------------------

	@SuppressWarnings("static-access")
	public Treasurer findByPrincipal() {
		UserAccount userAccount;
		Treasurer result;

		userAccount = this.loginService.getPrincipal();
		result = this.findByUserAccount(userAccount);
		return result;
	}

	public Treasurer findByUserAccount(final UserAccount userAccount) {
		Treasurer result;

		result = this.treasurerRepository.findByUserAccount(userAccount.getId());
		return result;

	}

	public Treasurer reconstruct(final FormTreasurer formTreasurer, final BindingResult binding) {

		final Treasurer result = new Treasurer();
		result.setId(0);
		result.setVersion(0);
		result.setName(formTreasurer.getName());
		result.setSurname(formTreasurer.getSurname());
		final UserAccount userAccount = formTreasurer.getUserAccount();
		result.setUserAccount(userAccount);
		result.setPhone(formTreasurer.getPhone());
		result.setEmail(formTreasurer.getEmail());
		result.setMessagesIncoming(new HashSet<Message>());
		result.setMessagesOutgoing(new HashSet<Message>());
		result.setDNI(formTreasurer.getDNI());
		result.setAnswers(new HashSet<Answer>());

		this.validator.validate(result, binding);
		return result;
	}

	public Treasurer reconstruct(final Treasurer treasurer, final BindingResult binding) {
		Treasurer result;

		if (treasurer.getId() == 0)
			result = treasurer;
		else {
			result = this.treasurerRepository.findOne(treasurer.getId());
			result.setName(treasurer.getName());
			result.setSurname(treasurer.getSurname());
			result.setEmail(treasurer.getEmail());
			result.setPhone(treasurer.getPhone());
			result.setDNI(treasurer.getDNI());

			this.validator.validate(result, binding);
		}

		return result;
	}

	public FormTreasurer createForm() {
		final FormTreasurer formTreasurer = new FormTreasurer();

		final Authority a = new Authority();
		final UserAccount userAccount = new UserAccount();
		a.setAuthority("TREASURER");
		userAccount.addAuthority(a);
		formTreasurer.setUserAccount(userAccount);

		return formTreasurer;
	}
	public void flush() {
		this.treasurerRepository.flush();
	}

}
