
package services;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import repositories.RequestRepository;
import domain.Activity;
import domain.Actor;
import domain.Associate;
import domain.President;
import domain.Request;
import domain.Volunteer;

@Transactional
@Service
public class RequestService {

	// Managed repository -------------------------------
	@Autowired
	private RequestRepository	requestRepository;
	@Autowired
	private ActivityService		activityService;
	@Autowired
	private AssociateService	associateService;
	@Autowired
	private LeisureService		leisureService;
	@Autowired
	private ActorService		actorService;


	// Constructor --------------------------------------
	public RequestService() {
		super();

	}

	public Request create(final Activity s, final Associate t) {

		final Request a = new Request();
		a.setActivity(s);
		a.setAssociate(t);
		a.setStatus("PENDING");
		a.setMoment(new Date());
		a.setSeats(1);
		return a;
	}

	public Request save(final Request a) {
		Assert.notNull(a);
		return this.requestRepository.save(a);
	}

	public void delete(final Request a) {

		if (a.getStatus().equals("ACCEPTED")) {

			final Activity r = this.activityService.findOne(a.getActivity().getId());

			r.setSeats(r.getSeats() + a.getSeats());
			this.activityService.save(r);
		}

		this.requestRepository.delete(a);
	}
	// Simple CRUD methods ------------------------------

	public Request findOne(final Integer id) {
		return this.requestRepository.findOne(id);
	}

	public Collection<Request> findAll() {
		return this.requestRepository.findAll();
	}

	// Other business methods ---------------------------

	public void flush() {
		this.requestRepository.flush();
	}

	public Request reconstruct(final Request request, final BindingResult binding) {
		final Request result;

		if (request.getId() == 0)
			result = request;
		else {
			result = this.requestRepository.findOne(request.getId());
			result.setStatus(request.getStatus());
			result.setSeats(request.getSeats());
		}

		return result;
	}

	public Collection<Request> findRequests() {
		final Collection<Request> result = new HashSet<Request>();
		result.addAll(this.requestRepository.findRequests());
		result.addAll(this.requestRepository.findRequestsAct());

		return result;
	}
	public Collection<Request> findCourseRequests(final Integer id) {
		return this.requestRepository.findCourseRequests(id);
	}
	public Request findequest(final Integer asid, final Integer acid) {
		return this.requestRepository.findRequest(asid, acid);
	}

	public Collection<Request> requestAccepted(final Integer id) {
		return this.requestRepository.requestAccepted(id);
	}
	public Integer requestTotalSeats(final Integer id) {
		return this.requestRepository.requestTotalSeats(id);
	}

	public Collection<Actor> requestActors(final Integer id) {
		return this.requestRepository.requestActors(id);
	}

	public void accept(final Request a) {
		Assert.isTrue(this.actorService.findByPrincipal() instanceof President || this.actorService.findByPrincipal() instanceof Volunteer);

		a.setStatus("ACCEPTED");
		this.requestRepository.save(a);

		final Activity r = this.activityService.findOne(a.getActivity().getId());

		r.setSeats(r.getSeats() - a.getSeats());
		this.activityService.save(r);

	}
	public void deny(final Request a) {
		Assert.isTrue(this.actorService.findByPrincipal() instanceof President || this.actorService.findByPrincipal() instanceof Volunteer);
		a.setStatus("DENIED");
		this.requestRepository.save(a);
	}

	public void unregister(final Request a) {
		a.setStatus("DENIED");
		this.requestRepository.save(a);
	}

}
