
package services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.PresidentRepository;
import security.LoginService;
import security.UserAccount;
import domain.President;

@Service
@Transactional
public class PresidentService {

	@Autowired
	private PresidentRepository	presidentRepository;
	@Autowired
	private LoginService		loginService;


	public PresidentService() {
		super();
	}

	public President edit(final President u) {
		Assert.notNull(u);

		final President saved = this.presidentRepository.save(u);

		return saved;
	}

	@SuppressWarnings("static-access")
	public President findByPrincipal() {

		UserAccount userAccount;
		President result;

		userAccount = this.loginService.getPrincipal();
		result = this.findByUserAccount(userAccount);
		return result;
	}

	public President findByUserAccount(final UserAccount userAccount) {
		President result;
		result = this.presidentRepository.findByUserAccount(userAccount.getId());
		return result;
	}

	public void flush() {
		this.presidentRepository.flush();
	}

	public President findAdministrator() {
		return this.presidentRepository.findAdministrator();
	}

}
