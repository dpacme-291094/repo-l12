
package services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.LeisureRepository;
import domain.Comment;
import domain.Leisure;
import domain.President;
import domain.Request;

@Transactional
@Service
public class LeisureService {

	// Managed repository -------------------------------
	@Autowired
	private LeisureRepository	leisureRepository;
	@Autowired
	private RequestService		requestService;
	@Autowired
	private Validator			validator;

	@Autowired
	private ActivityService		activityService;
	@Autowired
	private ActorService		actorService;


	// Constructor --------------------------------------
	public LeisureService() {
		super();

	}

	public Leisure create() {

		final Leisure a = new Leisure();
		a.setDescription("");
		a.setComments(new HashSet<Comment>());
		a.setImage("");
		a.setMoment(new Date());
		a.setPrice(0.);
		a.setSeats(0);
		a.setTitle("");
		a.setStatus("OPEN");
		a.setTotalDuration("");
		a.setRequests(new HashSet<Request>());

		return a;
	}

	public Leisure save(final Leisure c) {
		Assert.notNull(c);

		if (this.actorService.findByPrincipal() instanceof President)
			try {
				if (c.getId() != 0 && c.getRequests().size() > 0)
					if (c.getStatus().equals("OPEN"))
						this.activityService.avisarModificacionSocios(this.requestService.requestActors(c.getId()), "La actividad " + c.getTitle() + " fue modificada, por favor revise la informacion del evento", "Modificacion de actividad",
							this.actorService.findByPrincipal());
					else if (c.getStatus().equals("CANCELLED"))
						this.activityService.avisarModificacionSocios(this.requestService.requestActors(c.getId()), "La actividad " + c.getTitle() + " ha sido cancelada. Sentimos las molestias.", "Cancelaci�n de actividad",
							this.actorService.findByPrincipal());
			} catch (final Throwable oops) {
			}
		return this.leisureRepository.save(c);
	}

	public void delete(final Leisure a) {
		for (final Request b : a.getRequests())
			this.requestService.delete(b);
		this.leisureRepository.delete(a);
	}

	// Simple CRUD methods ------------------------------

	public Leisure findOne(final Integer id) {
		return this.leisureRepository.findOne(id);
	}

	public Collection<Leisure> findAll() {
		return this.leisureRepository.findAll();
	}

	// Other business methods ---------------------------

	public void flush() {
		this.leisureRepository.flush();
	}

	public Leisure reconstruct(final Leisure leasure, final BindingResult binding) {
		final Leisure result;

		if (leasure.getId() == 0)
			result = leasure;
		else {
			result = this.leisureRepository.findOne(leasure.getId());
			result.setDescription(leasure.getDescription());
			result.setImage(leasure.getImage());
			result.setMoment(leasure.getMoment());
			result.setPrice(leasure.getPrice());
			result.setSeats(leasure.getSeats());
			result.setTitle(leasure.getTitle());
			result.setStatus(leasure.getStatus());
			result.setTotalDuration(leasure.getTotalDuration());
			try {
				this.validator.validate(result, binding);
			} catch (final Throwable oops) {

			}
		}

		return result;
	}

	//Queries C
	//Queries 4 y 5
	//La actividad de ocio que m�s y menos asistentes ha tenido
	public List<Object> maxMinLeisure() {
		final List<Object> result = new ArrayList<Object>();
		final List<Object> data = Arrays.asList(this.leisureRepository.maxMinLeisure().toArray());
		if (data.size() >= 1) {
			result.add(data.get(0));
			result.add(data.get(data.size() - 1));
		}
		return result;
	}

	public List<Object> maxMinPriceLeisure() {

		return new ArrayList<Object>(this.leisureRepository.maxMinPriceLeisure());
	}
	public List<Object> MaxPriceLeisure() {

		final List<Object> data = Arrays.asList(this.leisureRepository.maxPriceLeisure().toArray());

		return data;
	}
}
