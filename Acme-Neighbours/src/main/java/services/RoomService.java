
package services;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import repositories.RoomRepository;
import domain.Actor;
import domain.Message;
import domain.Offer;
import domain.Room;

@Transactional
@Service
public class RoomService {

	// Managed repository -------------------------------
	@Autowired
	private RoomRepository	roomRepository;

	@Autowired
	private PetitionService	petitionService;
	@Autowired
	private MessageService	messageService;


	// Constructor --------------------------------------
	public RoomService() {
		super();

	}

	public Room create() {

		final Room a = new Room();
		a.setDescription("");
		a.setOffers(new HashSet<Offer>());

		a.setPicture("");
		a.setTitle("");

		return a;
	}

	public Room save(final Room a) {
		Assert.notNull(a);
		return this.roomRepository.save(a);
	}

	// Simple CRUD methods ------------------------------

	public Room findOne(final Integer id) {
		return this.roomRepository.findOne(id);
	}

	public Collection<Room> findAll() {
		return this.roomRepository.findAll();
	}

	// Other business methods ---------------------------

	public void flush() {
		this.roomRepository.flush();
	}

	public Room reconstruct(final Room room, final BindingResult binding) {
		final Room result;

		if (room.getId() == 0)
			result = room;
		else {
			result = this.roomRepository.findOne(room.getId());
			result.setDescription(room.getDescription());
			result.setPicture(room.getPicture());
			result.setTitle(room.getTitle());
		}

		return result;
	}

	public Double ratioAceptadas() {
		return this.roomRepository.ratioAceptadas();
	}

	public Double ratioPendientes() {
		return this.roomRepository.ratioPendientes();
	}

	public Double ratioDenegadas() {
		return this.roomRepository.ratioDenegadas();
	}

	public Collection<Object[]> maxRoomReserved() {
		return this.roomRepository.maxRoomReserved();
	}

	public Collection<Object[]> minRoomReserved() {
		return this.roomRepository.minRoomReserved();
	}

	public void avisarModificacionSocios(final Collection<Actor> e, final String mensaje, final String subject, final Actor m) {

		for (final Actor c : e)
			this.avisoModificacion(c, mensaje, subject, m);
	}

	public void avisoModificacion(final Actor c, final String mensaje, final String subject, final Actor m) {

		Message aviso = this.messageService.create();
		aviso.setMoment(new Date());
		aviso.setSubject(subject);
		aviso.setText(mensaje);
		aviso.setEmailSender(m.getEmail());
		aviso.setEmailRecipient(c.getEmail());
		aviso.setAttachments("");
		aviso.setActorRecipient(c);
		aviso.setActorSender(m);
		try {
			aviso = this.messageService.save(aviso);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
		}
	}
}
