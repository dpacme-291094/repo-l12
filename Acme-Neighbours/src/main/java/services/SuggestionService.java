
package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import repositories.SuggestionRepository;
import domain.Suggestion;
import domain.Treasurer;

@Transactional
@Service
public class SuggestionService {

	// Managed repository -------------------------------
	@Autowired
	private SuggestionRepository	suggestionRepository;

	@Autowired
	private ActorService			actorService;


	// Constructor --------------------------------------
	public SuggestionService() {
		super();

	}

	public Suggestion create() {

		final Suggestion a = new Suggestion();
		a.setDescription("");
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() - 600000);
		a.setMoment(currentDate);
		a.setTitle("");
		return a;
	}

	public Suggestion save(final Suggestion a) {
		Assert.notNull(a);
		return this.suggestionRepository.save(a);
	}

	public void delete(final Suggestion a) {
		Assert.notNull(a);
		Assert.isInstanceOf(Treasurer.class, this.actorService.findByPrincipal());
		this.suggestionRepository.delete(a);
	}

	// Simple CRUD methods ------------------------------

	public Suggestion findOne(final Integer id) {
		return this.suggestionRepository.findOne(id);
	}

	public Collection<Suggestion> findAll() {
		return this.suggestionRepository.findAll();
	}

	// Other business methods ---------------------------

	public void flush() {
		this.suggestionRepository.flush();
	}

	public Suggestion reconstruct(final Suggestion suggestion, final BindingResult binding) {
		final Suggestion result;

		if (suggestion.getId() == 0)
			result = suggestion;
		else {
			result = this.suggestionRepository.findOne(suggestion.getId());
			result.setDescription(suggestion.getDescription());
			result.setMoment(suggestion.getMoment());
			result.setTitle(suggestion.getTitle());
		}

		return result;
	}

	public Collection<Suggestion> findNotAnswered() {
		Assert.isInstanceOf(Treasurer.class, this.actorService.findByPrincipal());
		return this.suggestionRepository.findNotAnswered();

	}

	public Collection<Suggestion> findAnswered() {
		return this.suggestionRepository.findAnswered();

	}

}
