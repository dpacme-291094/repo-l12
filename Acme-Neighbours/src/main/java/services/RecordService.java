
package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.RecordRepository;
import domain.Record;
import domain.Reunion;
import domain.Secretary;

@Transactional
@Service
public class RecordService {

	// Managed repository -------------------------------
	@Autowired
	private RecordRepository	recordRepository;

	// Validator
	@Autowired
	private Validator			validator;


	// Constructor --------------------------------------
	public RecordService() {
		super();

	}

	public Record create(final Reunion reunion, final Secretary s) {

		final Record a = new Record();
		a.setReunion(reunion);
		a.setSecretary(s);
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() - 600000);
		a.setMoment(currentDate);
		a.setIsFinal(false);
		a.setTitle("");
		a.setDescription("");
		return a;
	}
	public Record save(final Record a) {
		Assert.notNull(a);
		Assert.isTrue(a.getReunion().getStatus().equals("CLOSED"));
		if (a.getId() > 0)
			Assert.isTrue(this.findOne(a.getId()).getIsFinal() == false);

		if (a.getId() == 0)
			Assert.isTrue(a.getReunion().getRecord() == null);
		return this.recordRepository.save(a);
	}

	public void delete(final Record a) {
		Assert.isTrue(a.getIsFinal() == false);
		this.recordRepository.delete(a);
	}
	// Simple CRUD methods ------------------------------

	public Record findOne(final Integer id) {
		return this.recordRepository.findOne(id);
	}

	public Collection<Record> findAll() {
		return this.recordRepository.findAll();
	}

	// Other business methods ---------------------------

	public void flush() {
		this.recordRepository.flush();
	}

	public Record reconstruct(final Record record, final BindingResult binding) {
		final Record result;

		if (record.getId() == 0)
			result = record;
		else {
			result = this.recordRepository.findOne(record.getId());
			result.setMoment(record.getMoment());
			result.setIsFinal(record.getIsFinal());
			result.setTitle(record.getTitle());
			result.setDescription(record.getDescription());
			result.setReunion(record.getReunion());
			this.validator.validate(result, binding);
		}

		return result;
	}

	public int actasPendientes() {
		return this.recordRepository.actasPendientes();
	}

	public int actasFinalizadas() {
		return this.recordRepository.actasFinalizadas();
	}
}
