
package services;

import java.util.Collection;
import java.util.HashSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.SecretaryRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Message;
import domain.Offer;
import domain.President;
import domain.Record;
import domain.Secretary;
import forms.FormSecretary;

@Service
@Transactional
public class SecretaryService {

	@Autowired
	private SecretaryRepository	secretaryRepository;

	@Autowired
	private LoginService		loginService;

	@Autowired
	private Validator			validator;
	@Autowired
	private PresidentService	presidentService;


	// Constructor --------------------------------------
	public SecretaryService() {
		super();

	}
	public Secretary create() {
		this.checkPrincipal();
		final Secretary c = new Secretary();
		c.setOffers(new HashSet<Offer>());
		c.setRecords(new HashSet<Record>());

		return c;
	}
	private void checkPrincipal() {
		final President pre = this.presidentService.findByPrincipal();
		for (final Authority a : pre.getUserAccount().getAuthorities())
			Assert.isTrue(a.getAuthority().equals("PRESIDENT"));
	}

	public Secretary save(final Secretary c) {
		Assert.notNull(c);
		Secretary secretary;
		String password;
		String hash;

		password = c.getUserAccount().getPassword();
		hash = this.encodePassword(password);
		c.getUserAccount().setPassword(hash);

		secretary = this.secretaryRepository.save(c);
		return secretary;
	}
	public Secretary edit(final Secretary l) {
		Assert.notNull(l);

		return this.secretaryRepository.save(l);
	}

	private String encodePassword(final String password) {
		Md5PasswordEncoder encoder;
		String result;

		if (password == null || "".equals(password))
			result = null;
		else {
			encoder = new Md5PasswordEncoder();
			result = encoder.encodePassword(password, null);
		}

		return result;
	}

	// Simple CRUD methods ------------------------------

	public Actor findOne(final Integer id) {
		return this.secretaryRepository.findOne(id);
	}

	public Collection<Secretary> findAll() {
		return this.secretaryRepository.findAll();
	}

	// Other business methods ---------------------------

	@SuppressWarnings("static-access")
	public Secretary findByPrincipal() {
		UserAccount userAccount;
		Secretary result;

		userAccount = this.loginService.getPrincipal();
		result = this.findByUserAccount(userAccount);
		return result;
	}

	public Secretary findByUserAccount(final UserAccount userAccount) {
		Secretary result;

		result = this.secretaryRepository.findByUserAccount(userAccount.getId());
		return result;

	}

	public Secretary reconstruct(final FormSecretary formSecretary, final BindingResult binding) {

		final Secretary result = new Secretary();
		result.setId(0);
		result.setVersion(0);
		result.setName(formSecretary.getName());
		result.setSurname(formSecretary.getSurname());
		final UserAccount userAccount = formSecretary.getUserAccount();
		result.setUserAccount(userAccount);
		result.setPhone(formSecretary.getPhone());
		result.setEmail(formSecretary.getEmail());
		result.setDNI(formSecretary.getDNI());
		result.setMessagesIncoming(new HashSet<Message>());
		result.setMessagesOutgoing(new HashSet<Message>());
		result.setRecords(new HashSet<Record>());
		result.setOffers(new HashSet<Offer>());
		this.validator.validate(result, binding);
		return result;
	}

	public Secretary reconstruct(final Secretary secretary, final BindingResult binding) {
		Secretary result;

		if (secretary.getId() == 0)
			result = secretary;
		else {
			result = this.secretaryRepository.findOne(secretary.getId());
			result.setName(secretary.getName());
			result.setSurname(secretary.getSurname());
			result.setEmail(secretary.getEmail());
			result.setPhone(secretary.getPhone());
			result.setDNI(secretary.getDNI());

			this.validator.validate(result, binding);
		}

		return result;
	}

	public FormSecretary createForm() {
		final FormSecretary formSecretary = new FormSecretary();

		final Authority a = new Authority();
		final UserAccount userAccount = new UserAccount();
		a.setAuthority("SECRETARY");
		userAccount.addAuthority(a);
		formSecretary.setUserAccount(userAccount);

		return formSecretary;
	}
	public void flush() {
		this.secretaryRepository.flush();
	}

}
