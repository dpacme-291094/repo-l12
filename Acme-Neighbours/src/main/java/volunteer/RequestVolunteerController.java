
package volunteer;

import java.util.Collection;
import java.util.HashSet;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.RequestService;
import services.VolunteerService;
import controllers.AbstractController;
import domain.Course;
import domain.Request;
import domain.Volunteer;

@Controller
@RequestMapping("/request/volunteer")
public class RequestVolunteerController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private RequestService		requestService;
	@Autowired
	private VolunteerService	volunteerService;
	@Autowired
	private ActorService		actorService;


	// Constructor ---------------------------------------------------------------
	public RequestVolunteerController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		//		final Integer id = this.volunteerService.findByPrincipal().getId();
		//		final Collection<Request> requests = this.requestService.findCourseRequests(id);
		final Volunteer a = this.volunteerService.findByPrincipal();
		final Collection<Request> requests = new HashSet<Request>();
		final Collection<Course> cc = a.getCourses();
		for (final Course c : cc)
			for (final Request r : c.getRequests())
				requests.add(r);

		final ModelAndView result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("uri", "request/volunteer/list.do");
		return result;
	}
	// Create -------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "accept")
	public ModelAndView register(@Valid final Integer requestId) {
		final ModelAndView result;
		final Request s = this.requestService.findOne(requestId);

		this.requestService.accept(s);

		final Collection<Request> requests = this.requestService.findRequests();

		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("uri", "request/volunteer/list.do");

		return result;
	}
	@RequestMapping(value = "/list", method = RequestMethod.POST, params = "deny")
	public ModelAndView delete(@Valid final Integer requestId) {
		final ModelAndView result;
		final Request s = this.requestService.findOne(requestId);

		final Collection<Request> requests = this.requestService.findRequests();

		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("uri", "request/volunteer/list.do");

		this.requestService.deny(s);
		return result;
	}

}
