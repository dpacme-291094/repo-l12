
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {

	@Query("select a.title, sum(r.seats) From Request r Inner Join r.activity a WHERE TYPE(a) IN Course Group By r.activity ORDER BY sum(r.seats) DESC")
	Collection<Object[]> cursoMasAsistido();

	//B Queries
	//Query 1
	//El voluntario que m�s cursos tiene relacionados.
	@Query("select v, v.courses.size from Volunteer v where v.courses.size = (select max(v.courses.size) from Volunteer v)")
	Collection<Object[]> volunteerMoreCourses();

	//Query 4
	//El curso al que mas socios han acudido.
	@Query("select a.title, sum(r.seats) From Request r Inner Join r.activity a WHERE TYPE(a) IN Course AND a.status='CLOSED' AND r.status='ACCEPTED' Group By r.activity ORDER BY sum(r.seats) DESC")
	Collection<Object[]> courseMoreAssistant();
}
