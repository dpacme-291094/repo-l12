
package repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Leisure;

@Repository
public interface LeisureRepository extends JpaRepository<Leisure, Integer> {

	//Query 4 y 5
	//La actividad de ocio que m�s y menos asistentes ha tenido
	@Query("select l,sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) from Leisure l join l.requests r group by l order by sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) desc")
	List<Object[]> maxMinLeisure();

	@Query("select min(l.price), avg(l.price), max(l.price) From Leisure l")
	List<Object[]> maxMinPriceLeisure();

	@Query("select r.associate.name, r.associate.surname, sum(l.price) * r.seats From Leisure l Inner Join l.requests r Where l.status = 'CLOSED' GROUP BY r.associate ORDER BY sum(l.price) * r.seats DESC")
	List<Object[]> maxPriceLeisure();

}
