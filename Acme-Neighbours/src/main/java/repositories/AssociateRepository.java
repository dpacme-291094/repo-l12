
package repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Associate;

@Repository
public interface AssociateRepository extends JpaRepository<Associate, Integer> {

	@Query("select a, a.petitions.size from Associate a where a.petitions.size = (select max(a.petitions.size) from Associate a)")
	Object[] associateMasReservas();

	@Query("select a, a.petitions.size from Associate a where a.petitions.size = (select min(a.petitions.size) from Associate a)")
	Object[] associateMenosReservas();

	@Query("select c from Associate c where c.userAccount.id = ?1")
	Associate findByUserAccount(int id);

	//Queries C
	//Query 1
	// Media de peticiones a las actividades de ocio por socio
	@Query("select avg(a.requests.size) from Associate a")
	Double avgRequests();
	//Query 2
	// El socio que ha acudido a mas reuniones vs el que menos.
	@Query("select r.associate,sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) from Request r  where r.activity in (select r2 from Reunion r2) group by r.associate order by sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) desc")
	List<Object[]> maxMinAssistantsReunion();
	//De la query de arriba solo he pensado en esa forma, para coger el que mas tiene el la posicion 0 y el que menos tiene, la ultima posicion.

	//Query 3
	// El socio que ha acudido a mas actividades de ocio vs el que menos.
	@Query("select r.associate,sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) from Request r  where r.activity in (select l from Leisure l) group by r.associate order by sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) desc")
	List<Object[]> maxMinAssistantsLeisure();
	//Como la anterior.

	//Query 7
	//Media de n�mero de comentarios por socio.
	@Query("select avg(a.comments.size) from Associate a")
	Double commentPerAssociate();

	//Media de n�mero de comentarios por socio.
	@Query("select l, l.totalFee from Associate l")
	List<Object[]> totalAssociates();
}
