
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Suggestion;

@Repository
public interface SuggestionRepository extends JpaRepository<Suggestion, Integer> {

	@Query("select s from Suggestion s where s.answer IS EMPTY")
	Collection<Suggestion> findNotAnswered();

	@Query("select s from Suggestion s where s.answer IS NOT EMPTY")
	Collection<Suggestion> findAnswered();

}
