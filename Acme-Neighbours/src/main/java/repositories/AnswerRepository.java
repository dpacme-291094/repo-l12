
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Answer;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Integer> {

	@Query("select count(a) From Answer a")
	Integer maxAnswerer();

	@Query("select count(s) From Suggestion s Where s NOT IN (Select a.suggestion From Answer a)")
	Integer maxNotAnswerer();

}
