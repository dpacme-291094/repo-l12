
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {

	//Queries B
	//Query 2
	//El socio que mas salas ha reservado vs el que menos.

	@Query("select a, p.size from Associate a Inner Join a.petitions p where p.status = 'ACCEPTED' AND p.size = (select max(p.size) from Associate a Inner join a.petitions p where p.status = 'ACCEPTED')")
	Collection<Object[]> maxRoomReserved();

	@Query("select a, p.size from Associate a Inner Join a.petitions p where p.status = 'ACCEPTED' AND p.size = (select min(p.size) from Associate a Inner join a.petitions p where p.status = 'ACCEPTED')")
	Collection<Object[]> minRoomReserved();
	//Query 3
	//El ratio de solicitudes a salas aceptadas, pendientes y denegadas.

	@Query("select sum( Case when p.status='ACCEPTED' then 1 Else 0 end)*1.0/ count(p)from Petition p")
	Double ratioAceptadas();

	@Query("select sum( Case when p.status='PENDING' then 1 Else 0 end)*1.0/ count(p)from Petition p")
	Double ratioPendientes();

	@Query("select sum( Case when p.status='DENIED' then 1 Else 0 end)*1.0/ count(p)from Petition p")
	Double ratioDenegadas();

}
