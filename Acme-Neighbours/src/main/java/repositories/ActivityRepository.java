
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Activity;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Integer> {

	@Query("select sum(l.price) * r.seats From Leisure l Inner Join l.requests r Where YEAR(l.moment) = YEAR(CURRENT_DATE) AND l.status = 'CLOSED'")
	Double thisYearPayment();

	@Query("select sum(l.price) * r.seats From Leisure l Inner Join l.requests r Where YEAR(l.moment) = YEAR(CURRENT_DATE)-1 AND l.status = 'CLOSED'")
	Double lastYearPayment();
}
