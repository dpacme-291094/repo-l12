
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Actor;
import domain.Request;

@Repository
public interface RequestRepository extends JpaRepository<Request, Integer> {

	@Query("select a.requests from Activity a where TYPE(a) IN Leisure")
	Collection<Request> findRequestsAct();

	@Query("select a.requests from Activity a where TYPE(a) IN Reunion")
	Collection<Request> findRequests();

	@Query("select c.requests from Course c where c.volunteer.id = ?1")
	Collection<Request> findCourseRequests(Integer id);

	@Query("select r from Request r where r.associate.id =?1 and r.activity.id=?2")
	Request findRequest(Integer asId, Integer acId);

	@Query("select r from Request r where r.activity.id=?1 and r.status= 'ACCEPTED'")
	Collection<Request> requestAccepted(Integer id);

	@Query("select sum(r.seats) from Request r where r.activity.id=?1 and r.status= 'ACCEPTED'")
	Integer requestTotalSeats(Integer id);

	@Query("select r.associate from Request r where r.activity.id=?1 and r.status= 'ACCEPTED'")
	Collection<Actor> requestActors(Integer id);

}
