
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.President;

@Repository
public interface PresidentRepository extends JpaRepository<President, Integer> {

	@Query("select c from President c where c.userAccount.id = ?1")
	President findByUserAccount(int id);

	@Query("select a from President a")
	public President findAdministrator();

}
