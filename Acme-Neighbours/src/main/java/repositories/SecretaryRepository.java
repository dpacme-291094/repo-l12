
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Secretary;

@Repository
public interface SecretaryRepository extends JpaRepository<Secretary, Integer> {

	@Query("select c from Secretary c where c.userAccount.id = ?1")
	Secretary findByUserAccount(int id);
}
