
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Reunion;

@Repository
public interface ReunionRepository extends JpaRepository<Reunion, Integer> {

	@Query("select r From Reunion r Where r.moment < current_date()")
	Collection<Reunion> findReunionBeforeCurrentDate();

	//Queries C
	//Query 6
	//El m�nimo, la media y el m�ximo n�mero de asistentes a las reuniones.
	@Query("select min(r.requests.size),max(r.requests.size),avg(r.requests.size) from Reunion r join r.requests rr where rr.status='ACCEPTED'")
	Collection<Object> minMaxAvgReunion();
}
