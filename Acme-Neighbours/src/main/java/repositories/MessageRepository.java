
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Actor;
import domain.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

	@Query("select m from Message m where m.actorSender=?1 or m.actorRecipient=?1 order by m.moment desc")
	Collection<Message> findByActor(Actor actor);

	@Query("select m from Message m where m.actorSender.id=?1 order by m.moment desc")
	Collection<Message> findByActorSender(int id);

	@Query("select m from Message m where m.actorRecipient.id=?1 order by m.moment desc")
	Collection<Message> findByActorRecipient(int id);

	//Queries C
	//Query 8
	//El m�nimo, la media y el m�ximo n�mero de mensajes enviados por actor.
	@Query("select min(a.messagesOutgoing.size),max(a.messagesOutgoing.size),avg(a.messagesOutgoing.size) from Actor a")
	Collection<Object> mmaSenderPerActor();

	//Query 9
	//El m�nimo, la media y el m�ximo n�mero de mensajes enviados por actor.
	@Query("select min(a.messagesIncoming.size),max(a.messagesIncoming.size),avg(a.messagesIncoming.size) from Actor a")
	Collection<Object> mmaRecipientPerActor();

}
