
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Petition;

@Repository
public interface PetitionRepository extends JpaRepository<Petition, Integer> {

	@Query("select r from Petition r where r.offer.id =?1 and r.status='ACCEPTED'")
	Petition findPetition(Integer asId);
	@Query("select r from Petition r where r.offer.secretary.id =?1")
	Collection<Petition> findPetitions(Integer asId);
	@Query("select r from Petition r where r.offer.id =?1 and r.associate.id=?2")
	Petition findPetitionOffer(Integer asId, Integer oId);

}
