
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Record;

@Repository
public interface RecordRepository extends JpaRepository<Record, Integer> {

	@Query("select count(r) From Record r Where r.isFinal!=1")
	int actasPendientes();

	@Query("select count(r) From Record r Where r.isFinal=1")
	int actasFinalizadas();
}
