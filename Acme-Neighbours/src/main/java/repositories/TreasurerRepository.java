
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Treasurer;

@Repository
public interface TreasurerRepository extends JpaRepository<Treasurer, Integer> {

	@Query("select c from Treasurer c where c.userAccount.id = ?1")
	Treasurer findByUserAccount(int id);
}
