
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Volunteer;

@Repository
public interface VolunteerRepository extends JpaRepository<Volunteer, Integer> {

	@Query("select c from Volunteer c where c.userAccount.id = ?1")
	Volunteer findByUserAccount(int id);

	@Query("select v, v.courses.size from Volunteer v where v.courses.size = (select max(v.courses.size) from Volunteer v)")
	Object[] voluntarioMasCursos();
}
