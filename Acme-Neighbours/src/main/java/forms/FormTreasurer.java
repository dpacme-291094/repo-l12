
package forms;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

import security.UserAccount;

public class FormTreasurer {

	// *ToDel* Creo que aqu� no va la CreditCard, puesto que no se va usar para el register.

	private UserAccount	userAccount;
	private String		name;
	private String		surname;
	private String		phone;
	private String		email;
	private String		DNI;


	public UserAccount getUserAccount() {
		return this.userAccount;
	}

	public void setUserAccount(final UserAccount userAccount) {
		this.userAccount = userAccount;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getSurname() {
		return this.surname;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	@Email
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	@NotBlank
	@Pattern(regexp = "^(([+])([0-9]{1,3})([ ])?)?(([0-9]{3}([ ])?[0-9]{3}([ ])?[0-9]{3})|([0-9]{3}([ ])?[0-9]{2}([ ])?[0-9]{2}([ ])?[0-9]{2}))$$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Pattern(regexp = "^([0-9]{8})([a-zA-Z]{1})$")
	public String getDNI() {
		return this.DNI;
	}

	public void setDNI(final String dNI) {
		this.DNI = dNI;
	}

}
