
package forms;

import java.util.Date;

import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

import domain.Message;

public class FormMessage {

	// Attributes -------------------------------------------------------------
	private String	emailSender;
	private String	emailRecipient;
	private Date	moment;
	private String	subject;
	private String	text;
	private String	attachments;


	public FormMessage() {
		super();
	}

	public FormMessage(final Message m) {

		this.setEmailSender(m.getEmailSender());
		this.setEmailRecipient(m.getEmailRecipient());
		this.setText(m.getText());
		this.setSubject(m.getSubject());
		this.setAttachments(m.getAttachments());
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() - 60000);
		this.setMoment(currentDate);

	}

	@Email
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getEmailSender() {
		return this.emailSender;
	}

	public void setEmailSender(final String sender) {
		this.emailSender = sender;
	}

	@Email
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getEmailRecipient() {
		return this.emailRecipient;
	}

	public void setEmailRecipient(final String recipient) {
		this.emailRecipient = recipient;
	}

	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getMoment() {
		return this.moment;
	}

	public void setMoment(final Date moment) {
		this.moment = moment;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getSubject() {
		return this.subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getText() {
		return this.text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public String getAttachments() {
		return this.attachments;
	}

	public void setAttachments(final String attachments) {
		this.attachments = attachments;
	}

}
