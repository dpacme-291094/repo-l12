
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Secretary extends Actor {

	// Constructors -----------------------------------------------------------
	public Secretary() {
		super();
	}


	// Attributes -------------------------------------------------------------

	//Relationships ----------------------------------------------------
	private Collection<Offer>	offers;
	private Collection<Record>	records;


	@NotNull
	@Valid
	@OneToMany(mappedBy = "secretary")
	public Collection<Offer> getOffers() {
		return this.offers;
	}

	public void setOffers(final Collection<Offer> offers) {
		this.offers = offers;
	}

	@NotNull
	@Valid
	@OneToMany(mappedBy = "secretary")
	public Collection<Record> getRecords() {
		return this.records;
	}

	public void setRecords(final Collection<Record> records) {
		this.records = records;
	}

}
