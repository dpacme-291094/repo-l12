
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = {
	@Index(columnList = "status")
})
public class Petition extends DomainEntity {

	// Constructors -----------------------------------------------------------
	public Petition() {
		super();
	}


	// Attributes -------------------------------------------------------------
	private String	description;
	private String	status;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@NotBlank
	@Pattern(regexp = "^(ACCEPTED|PENDING|DENIED)$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}


	//Relationships--------------------------------
	private Offer		offer;
	private Associate	associate;


	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Offer getOffer() {
		return this.offer;
	}

	public void setOffer(final Offer offer) {
		this.offer = offer;
	}

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Associate getAssociate() {
		return this.associate;
	}

	public void setAssociate(final Associate associate) {
		this.associate = associate;
	}
}
