
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

@Entity
@Access(AccessType.PROPERTY)
public class Answer extends DomainEntity {

	// Constructors -----------------------------------------------------------

	public Answer() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private String	text;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getText() {
		return this.text;
	}

	public void setText(final String text) {
		this.text = text;
	}


	//Relationships--------------------------------
	private Treasurer	treasurer;
	private Suggestion	suggestion;


	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Treasurer getTreasurer() {
		return this.treasurer;
	}

	public void setTreasurer(final Treasurer treasurer) {
		this.treasurer = treasurer;
	}

	@OneToOne(optional = false)
	@Valid
	@NotNull
	public Suggestion getSuggestion() {
		return this.suggestion;
	}

	public void setSuggestion(final Suggestion suggestion) {
		this.suggestion = suggestion;
	}

}
