
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Volunteer extends Actor {

	// Constructors -----------------------------------------------------------
	public Volunteer() {
		super();
	}


	// Attributes -------------------------------------------------------------

	//Relationships ----------------------------------------------------
	private Collection<Course>	courses;


	@Valid
	@NotNull
	@OneToMany(mappedBy = "volunteer")
	public Collection<Course> getCourses() {
		return this.courses;
	}

	public void setCourses(final Collection<Course> courses) {
		this.courses = courses;
	}

}
