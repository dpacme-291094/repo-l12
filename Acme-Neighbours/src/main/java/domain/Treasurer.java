
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Treasurer extends Actor {

	// Constructors -----------------------------------------------------------

	public Treasurer() {
		super();
	}


	// Attributes -------------------------------------------------------------

	//Relationships----------------------------------------------
	private Collection<Answer>	answers;


	@Valid
	@NotNull
	@OneToMany(mappedBy = "treasurer")
	public Collection<Answer> getAnswers() {
		return this.answers;
	}

	public void setAnswers(final Collection<Answer> answers) {
		this.answers = answers;
	}

}
