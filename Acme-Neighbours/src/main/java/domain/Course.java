
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
public class Course extends Activity {

	// Constructors -----------------------------------------------------------
	public Course() {
		super();
	}


	// Attributes -------------------------------------------------------------
	private String	image;
	//Relationships ----------------------------------------------------
	private Actor	volunteer;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@URL
	public String getImage() {
		return this.image;
	}

	public void setImage(final String image) {
		this.image = image;
	}

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Actor getVolunteer() {
		return this.volunteer;
	}

	public void setVolunteer(final Actor volunteer) {
		this.volunteer = volunteer;
	}

}
