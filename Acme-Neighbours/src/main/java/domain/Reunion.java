
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class Reunion extends Activity {

	// Constructors -----------------------------------------------------------

	public Reunion() {
		super();
	}


	// Attributes -------------------------------------------------------------

	//Relationships----------------------------
	private Record	record;


	@Valid
	@OneToOne(mappedBy = "reunion", optional = true)
	public Record getRecord() {
		return this.record;
	}

	public void setRecord(final Record record) {
		this.record = record;
	}

}
