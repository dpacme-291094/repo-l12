
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Offer extends DomainEntity {

	// Constructors -----------------------------------------------------------
	public Offer() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private Date	dateRequest;
	private String	time;
	private String	status;


	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getDateRequest() {
		return this.dateRequest;
	}

	public void setDateRequest(final Date dateRequest) {
		this.dateRequest = dateRequest;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Pattern(regexp = "^([0-9]{2}):([0-9]{2})$")
	public String getTime() {
		return this.time;
	}

	public void setTime(final String time) {
		this.time = time;
	}
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Pattern(regexp = "^(OCCUPIED|FREE)$")
	public String getStatus() {
		return this.status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}


	//Relationships--------------------------------
	private Room					room;
	private Secretary				secretary;
	private Collection<Petition>	petitions;


	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Room getRoom() {
		return this.room;
	}

	public void setRoom(final Room room) {
		this.room = room;
	}

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Secretary getSecretary() {
		return this.secretary;
	}

	public void setSecretary(final Secretary secretary) {
		this.secretary = secretary;
	}

	@Valid
	@NotNull
	@OneToMany(mappedBy = "offer")
	public Collection<Petition> getPetitions() {
		return this.petitions;
	}

	public void setPetitions(final Collection<Petition> petitions) {
		this.petitions = petitions;
	}

}
