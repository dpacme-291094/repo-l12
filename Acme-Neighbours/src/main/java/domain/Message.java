
package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = {
	@Index(columnList = "moment")
})
public class Message extends DomainEntity {

	// Constructors -----------------------------------------------------------

	public Message() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private Date	moment;
	private String	subject;
	private String	text;
	private String	attachments;
	private String	emailSender;
	private String	emailRecipient;


	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getMoment() {
		return this.moment;
	}

	public void setMoment(final Date moment) {
		this.moment = moment;
	}
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getSubject() {
		return this.subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getText() {
		return this.text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getAttachments() {
		return this.attachments;
	}

	public void setAttachments(final String attachments) {
		this.attachments = attachments;
	}

	@NotBlank
	public String getEmailSender() {
		return this.emailSender;
	}

	public void setEmailSender(final String emailSender) {
		this.emailSender = emailSender;
	}

	@NotBlank
	public String getEmailRecipient() {
		return this.emailRecipient;
	}

	public void setEmailRecipient(final String emailRecipient) {
		this.emailRecipient = emailRecipient;
	}


	private Actor	actorSender;
	private Actor	actorRecipient;


	@Valid
	@ManyToOne(optional = true)
	public Actor getActorSender() {
		return this.actorSender;
	}

	public void setActorSender(final Actor actorSender) {
		this.actorSender = actorSender;
	}

	@Valid
	@ManyToOne(optional = true)
	public Actor getActorRecipient() {
		return this.actorRecipient;
	}

	public void setActorRecipient(final Actor actorRecipient) {
		this.actorRecipient = actorRecipient;
	}

}
