
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = {
	@Index(columnList = "moment, status")
})
public abstract class Activity extends DomainEntity {

	// Constructors -----------------------------------------------------------
	public Activity() {
		super();

	}


	// Attributes -------------------------------------------------------------
	private String	title;
	private String	description;
	private Date	moment;

	private int		seats;
	private String	totalDuration;
	private String	status;


	@Min(0)
	public int getSeats() {
		return this.seats;
	}

	public void setSeats(final int seats) {
		this.seats = seats;
	}
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getMoment() {
		return this.moment;
	}

	public void setMoment(final Date moment) {
		this.moment = moment;
	}

	@Pattern(regexp = "^([0-9]{2}):([0-9]{2})$")
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTotalDuration() {
		return this.totalDuration;
	}

	public void setTotalDuration(final String totalDuration) {
		this.totalDuration = totalDuration;
	}

	@NotBlank
	@Pattern(regexp = "^(OPEN|CLOSED|CANCELLED)$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}


	//Relationships---------------------
	private Collection<Comment>	comments;
	private Collection<Request>	requests;


	@Valid
	@NotNull
	@OneToMany(mappedBy = "activity")
	public Collection<Comment> getComments() {
		return this.comments;
	}

	public void setComments(final Collection<Comment> comments) {
		this.comments = comments;
	}

	@Valid
	@NotNull
	@OneToMany(mappedBy = "activity")
	public Collection<Request> getRequests() {
		return this.requests;
	}

	public void setRequests(final Collection<Request> requests) {
		this.requests = requests;
	}

}
