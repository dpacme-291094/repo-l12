
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Associate extends Actor {

	// Constructors -----------------------------------------------------------

	public Associate() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private Integer		associateNumber;
	private CreditCard	creditCard;
	private Double		totalFee;
	private Date		lastPayment;


	public Integer getAssociateNumber() {
		return this.associateNumber;
	}

	public void setAssociateNumber(final Integer associateNumber) {
		this.associateNumber = associateNumber;
	}

	@Valid
	@NotNull
	public CreditCard getCreditCard() {
		return this.creditCard;
	}

	public void setCreditCard(final CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	@Min(0)
	public Double getTotalFee() {
		return this.totalFee;
	}

	public void setTotalFee(final Double totalFee) {
		this.totalFee = totalFee;
	}

	@NotNull
	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getLastPayment() {
		return this.lastPayment;
	}

	public void setLastPayment(final Date lastPayment) {
		this.lastPayment = lastPayment;
	}


	//Relationships-------------------------------------------
	private Collection<Petition>	petitions;
	private Collection<Request>		requests;
	private Collection<Comment>		comments;


	@Valid
	@NotNull
	@OneToMany(mappedBy = "associate")
	public Collection<Petition> getPetitions() {
		return this.petitions;
	}

	public void setPetitions(final Collection<Petition> petitions) {
		this.petitions = petitions;
	}
	@Valid
	@NotNull
	@OneToMany(mappedBy = "associate")
	public Collection<Request> getRequests() {
		return this.requests;
	}

	public void setRequests(final Collection<Request> requests) {
		this.requests = requests;
	}
	@Valid
	@NotNull
	@OneToMany(mappedBy = "associate")
	public Collection<Comment> getComments() {
		return this.comments;
	}

	public void setComments(final Collection<Comment> comments) {
		this.comments = comments;
	}

}
