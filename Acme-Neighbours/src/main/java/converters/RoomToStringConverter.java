
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Room;

@Component
@Transactional
public class RoomToStringConverter implements Converter<Room, String> {

	@Override
	public String convert(final Room room) {
		Assert.notNull(room);
		final String result = String.valueOf(room.getId());

		return result;
	}

}
