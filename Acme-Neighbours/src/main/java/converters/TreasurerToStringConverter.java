
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Treasurer;

@Component
@Transactional
public class TreasurerToStringConverter implements Converter<Treasurer, String> {

	@Override
	public String convert(final Treasurer treasurer) {
		Assert.notNull(treasurer);
		final String result = String.valueOf(treasurer.getId());

		return result;
	}

}
