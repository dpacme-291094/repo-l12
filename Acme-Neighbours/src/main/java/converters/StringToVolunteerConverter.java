
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.VolunteerRepository;
import domain.Volunteer;

@Component
@Transactional
public class StringToVolunteerConverter implements Converter<String, Volunteer> {

	@Autowired
	VolunteerRepository	volunteerRepository;


	@Override
	public Volunteer convert(final String text) {
		Volunteer result;
		int id;

		try {
			if (StringUtils.isEmpty(text))
				result = null;
			else {
				id = Integer.valueOf(text);
				result = this.volunteerRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
