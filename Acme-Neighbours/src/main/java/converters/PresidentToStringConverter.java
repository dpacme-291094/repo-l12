
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.President;

@Component
@Transactional
public class PresidentToStringConverter implements Converter<President, String> {

	@Override
	public String convert(final President president) {
		Assert.notNull(president);
		final String result = String.valueOf(president.getId());

		return result;
	}

}
