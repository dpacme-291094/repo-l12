
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Suggestion;

@Component
@Transactional
public class SuggestionToStringConverter implements Converter<Suggestion, String> {

	@Override
	public String convert(final Suggestion suggestion) {
		Assert.notNull(suggestion);
		final String result = String.valueOf(suggestion.getId());

		return result;
	}

}
