
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Reunion;

@Component
@Transactional
public class ReunionToStringConverter implements Converter<Reunion, String> {

	@Override
	public String convert(final Reunion reunion) {
		Assert.notNull(reunion);
		final String result = String.valueOf(reunion.getId());

		return result;
	}

}
