
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Secretary;

@Component
@Transactional
public class SecretaryToStringConverter implements Converter<Secretary, String> {

	@Override
	public String convert(final Secretary secretary) {
		Assert.notNull(secretary);
		final String result = String.valueOf(secretary.getId());

		return result;
	}

}
