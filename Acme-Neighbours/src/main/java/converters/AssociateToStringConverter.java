
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Associate;

@Component
@Transactional
public class AssociateToStringConverter implements Converter<Associate, String> {

	@Override
	public String convert(final Associate associate) {
		Assert.notNull(associate);
		final String result = String.valueOf(associate.getId());

		return result;
	}

}
