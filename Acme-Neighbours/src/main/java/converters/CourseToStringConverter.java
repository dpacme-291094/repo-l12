
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Course;

@Component
@Transactional
public class CourseToStringConverter implements Converter<Course, String> {

	@Override
	public String convert(final Course course) {
		Assert.notNull(course);
		final String result = String.valueOf(course.getId());

		return result;
	}

}
