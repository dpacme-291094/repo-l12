
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.SecretaryRepository;
import domain.Secretary;

@Component
@Transactional
public class StringToSecretaryConverter implements Converter<String, Secretary> {

	@Autowired
	SecretaryRepository	secretaryRepository;


	@Override
	public Secretary convert(final String text) {
		Secretary result;
		int id;

		try {
			if (StringUtils.isEmpty(text))
				result = null;
			else {
				id = Integer.valueOf(text);
				result = this.secretaryRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
