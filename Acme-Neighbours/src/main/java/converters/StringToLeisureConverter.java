
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.LeisureRepository;
import domain.Leisure;

@Component
@Transactional
public class StringToLeisureConverter implements Converter<String, Leisure> {

	@Autowired
	LeisureRepository	leisureRepository;


	@Override
	public Leisure convert(final String text) {
		Leisure result;
		int id;

		try {
			if (StringUtils.isEmpty(text))
				result = null;
			else {
				id = Integer.valueOf(text);
				result = this.leisureRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
