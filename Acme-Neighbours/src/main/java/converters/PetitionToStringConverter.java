
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Petition;

@Component
@Transactional
public class PetitionToStringConverter implements Converter<Petition, String> {

	@Override
	public String convert(final Petition petition) {
		Assert.notNull(petition);
		final String result = String.valueOf(petition.getId());

		return result;
	}

}
