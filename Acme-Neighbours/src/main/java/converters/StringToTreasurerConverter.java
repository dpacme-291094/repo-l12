
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.TreasurerRepository;
import domain.Treasurer;

@Component
@Transactional
public class StringToTreasurerConverter implements Converter<String, Treasurer> {

	@Autowired
	TreasurerRepository	treasurerRepository;


	@Override
	public Treasurer convert(final String text) {
		Treasurer result;
		int id;

		try {
			if (StringUtils.isEmpty(text))
				result = null;
			else {
				id = Integer.valueOf(text);
				result = this.treasurerRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
