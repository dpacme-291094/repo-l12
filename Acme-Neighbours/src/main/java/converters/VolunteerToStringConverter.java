
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Volunteer;

@Component
@Transactional
public class VolunteerToStringConverter implements Converter<Volunteer, String> {

	@Override
	public String convert(final Volunteer volunteer) {
		Assert.notNull(volunteer);
		final String result = String.valueOf(volunteer.getId());

		return result;
	}

}
