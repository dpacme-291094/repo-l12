
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.AssociateRepository;
import domain.Associate;

@Component
@Transactional
public class StringToAssociateConverter implements Converter<String, Associate> {

	@Autowired
	AssociateRepository	associateRepository;


	@Override
	public Associate convert(final String text) {
		Associate result;
		int id;

		try {
			if (StringUtils.isEmpty(text))
				result = null;
			else {
				id = Integer.valueOf(text);
				result = this.associateRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
