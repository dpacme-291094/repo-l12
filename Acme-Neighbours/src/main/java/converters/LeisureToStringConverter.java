
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Leisure;

@Component
@Transactional
public class LeisureToStringConverter implements Converter<Leisure, String> {

	@Override
	public String convert(final Leisure leisure) {
		Assert.notNull(leisure);
		final String result = String.valueOf(leisure.getId());

		return result;
	}

}
