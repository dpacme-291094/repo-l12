
package usercases;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.RecordService;
import services.ReunionService;
import services.SecretaryService;
import utilities.AbstractTest;
import domain.Record;
import domain.Reunion;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class RecordCaseTest extends AbstractTest {

	@Autowired
	private RecordService		recordService;
	@Autowired
	private SecretaryService	secretaryService;
	@Autowired
	private ReunionService		ReunionService;


	/*
	 * Requisito funcional: Crear un acta para una reuni�n cerrada.
	 * Se comprueba en el siguiente orden:
	 * Un secretario crea un acta sobre una reuni�n cerrada (Correcto)
	 * Un secretario intenta crear un acta sobre una reuni�n que ya tiene acta (Error)
	 * Un socio intenta crear un acta (Error)
	 * Un secretario intenta crear un acta con la descripci�n en blanco (Error)
	 */
	@Test
	public void driverCreateRecord() {

		final Object testingData[][] = {

			{
				"secretary", "prueba", "prueba", false, 88, true, null
			}, {
				"secretary", "prueba", "prueba", false, 87, false, IllegalArgumentException.class
			}, {
				"associate1", "prueba", "prueba", false, 88, false, ConstraintViolationException.class
			}, {
				"secretary", "", "", false, 88, true, ConstraintViolationException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCreateRecord((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (boolean) testingData[i][3], (Integer) testingData[i][4], (boolean) testingData[i][5], (Class<?>) testingData[i][6]);

	}

	/*
	 * Requisito funcional: Editar un acta.
	 * Se comprueba en el siguiente orden:
	 * Un secretario edita un acta borrador (Correcto)
	 * Un secretario edita un acta finalziada (Error)
	 * Null intenta editar un acta cerrada (Error)
	 */
	@Test
	public void driverEditRecord() {

		final Object testingData[][] = {

			{
				"secretary", "prueba", "prueba", false, null
			}, {
				"secretary", "prueba", "prueba", true, IllegalArgumentException.class
			}, {
				null, "prueba", "prueba", false, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateEditRecord((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (boolean) testingData[i][3], (Class<?>) testingData[i][4]);

	}

	/*
	 * Requisito funcional: Eliminar un acta que est� como borrador
	 * Se comprueba en el siguiente orden:
	 * Un secretario elimina un acta que estaba como borrador (Correcto)
	 */
	@Test
	public void driverDeleteRecord() {

		final Object testingData[][] = {

			{
				"secretary", 104, false, null
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateDeleteRecord((String) testingData[i][0], (Integer) testingData[i][1], (boolean) testingData[i][2], (Class<?>) testingData[i][3]);

	}

	protected void templateCreateRecord(final String username, final String title, final String description, final boolean isFinal, final Integer reunionId, final boolean cerrar, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Reunion r = this.ReunionService.findOne(reunionId);
			if (cerrar)
				r.setStatus("CLOSED");
			final Record a = this.recordService.create(r, this.secretaryService.findByPrincipal());
			a.setTitle(title);
			a.setDescription(description);
			a.setIsFinal(isFinal);
			this.recordService.save(a);
			this.ReunionService.flush();
			this.recordService.flush();

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	protected void templateEditRecord(final String username, final String title, final String description, final boolean isFinal, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Reunion r = this.ReunionService.findOne(88);
			r.setStatus("CLOSED");
			final Record a = this.recordService.create(r, this.secretaryService.findByPrincipal());
			a.setTitle("prueba");
			a.setDescription("prueba");
			a.setIsFinal(isFinal);
			final Record saved = this.recordService.save(a);
			saved.setTitle(title);
			saved.setDescription(description);
			this.recordService.save(saved);

			this.recordService.flush();
			this.ReunionService.flush();
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	protected void templateDeleteRecord(final String username, final Integer recordId, final boolean isFinal, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);

			final Record a = this.recordService.findOne(recordId);
			a.setIsFinal(isFinal);
			final Record saved = this.recordService.save(a);
			this.recordService.flush();
			this.recordService.delete(saved);
			this.recordService.flush();
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

}
