
package usercases;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import security.Authority;
import security.UserAccount;
import services.ActorService;
import services.CourseService;
import services.VolunteerService;
import utilities.AbstractTest;
import domain.Actor;
import domain.Course;
import domain.Message;
import domain.Volunteer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class VolunteerCaseTest extends AbstractTest {

	@Autowired
	private VolunteerService	volunteerService;
	@Autowired
	private CourseService		courseService;
	@Autowired
	private ActorService		actorService;


	/*
	 * Requisito funcional: Registro como un/a voluntario
	 * Se comprueba en el siguiente orden:
	 * Registrarse con todos los datos correctos (Correcto)
	 * Registrarse con un username que ya existe (Error)
	 * Registrarse sin seguir el patron de telefono ni email (Error)
	 * Registrarse con un DNI inv�lido (Error)
	 * Registrarse sin coincidir las contrase�as (Erroneo)
	 */

	@Test
	public void driverRegistrarVolunteer() {

		final Object testingData[][] = {
			{
				"volunteer10", "123456", "123456", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "75893212A", null

			}, {
				"volunteer1", "123456", "123456", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79029894Y", DataIntegrityViolationException.class

			}, {
				"volunteer10", "123456", "123456", "Juana", "Martinez", "jmartines.com", "123", "79029894Y", DataIntegrityViolationException.class

			}, {
				"volunteer10", "123456", "mal", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79YSA894Y", IllegalArgumentException.class

			}, {
				"volunteer10", "123456", "mal", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79029894Y", IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateRegister((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (Class<?>) testingData[i][8]);
	}
	/*
	 * Requisito funcional: Volunteer change his or fer profile
	 * Se comprueba en el siguiente orden:
	 * El voluntario "volunteer1" edita sus datos introduciendolos correctos (correcto)
	 * "null" intenta modificar sus datos (fallo)
	 * El voluntario "volunteer1" intenta introducir un DNI err�neo (fallo)
	 */
	@Test
	public void driverEditPersonalData() {

		final Object testingData[][] = {
			{
				"volunteer1", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79029984Y", null,

			}, {
				null, "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79029984Y", IllegalArgumentException.class
			}, {
				"volunteer1", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79076W778", ConstraintViolationException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateEdit((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	/*
	 * Requisito funcional: un voluntario puede
	 * crear un curso para que los socios asistan
	 * Se comprueba en el siguiente orden:
	 * Creacion de un curso con sus datos correctos (correcto)
	 * creacion de un curso sin estar conectado como voluntario (Error)
	 * creacion de un curso con otro usuario (Error)
	 * creacion de un curso con un usuario incorrecto (Error)
	 * Creacion de un curso con una imagen errornea (Error)
	 * Creacion de un curso con el titulo en blanco (Error)
	 * Creacion de un curso con la descripcion en blanco (Error)
	 * Creacion de un curso con el la fecha en pasado (Error)
	 * Creacion de un curso con los asientos en negativo (Error)
	 * Creacion de un curso con un status incorrecto (Error)
	 */

	@Test
	public void driverCreateCourse() {
		Date d = new Date();
		Date d2 = new Date();
		final Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.HOUR, 2);
		d = c.getTime();
		c.add(Calendar.HOUR, -10);
		d2 = c.getTime();

		final Object testingData[][] = {
			{
				"volunteer1", "title1", "description1", d, 20, "02:30", "OPEN", "https://www.google.es", null
			}, {
				"president", "title1", "description1", d, 20, "02:30", "OPEN", "https://www.google.es", javax.validation.ConstraintViolationException.class
			}, {
				"abcdefg", "title1", "description1", d, 20, "02:30", "OPEN", "https://www.google.es", java.lang.IllegalArgumentException.class
			}, {
				null, "title1", "description1", d, 20, "02:30", "OPEN", "https://www.google.es", java.lang.IllegalArgumentException.class
			}, {
				"volunteer1", "title1", "description1", d, 20, "02:30", "OPEN", "imagen", javax.validation.ConstraintViolationException.class
			}, {
				"volunteer1", null, "description1", d, 20, "02:30", "OPEN", "https://www.google.es", javax.validation.ConstraintViolationException.class
			}, {
				"volunteer1", "title1", null, d, 20, "02:30", "OPEN", "https://www.google.es", javax.validation.ConstraintViolationException.class
			}, {
				"volunteer1", "title1", "description1", d2, 20, "02:30", "OPEN", "https://www.google.es", javax.validation.ConstraintViolationException.class
			}, {
				"volunteer1", "title1", "description1", d, -10, "02:30", "OPEN", "https://www.google.es", javax.validation.ConstraintViolationException.class
			}, {
				"volunteer1", "title1", "description1", d, 20, "02:30", "ABCD", "https://www.google.es", javax.validation.ConstraintViolationException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCreateCoures((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Date) testingData[i][3], (int) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (Class<?>) testingData[i][8]);
	}

	/*
	 * Requisito funcional; Listar los cursos
	 * creados por un voluntario.
	 * Se comprueba en el siguiente orden:
	 * Listado de los cursos de un voluntario (Correcto)
	 * Listado de los cursos con un usuario null (Error)
	 * Listado de los cursos con otro tipo de usuario (Error)
	 */

	@Test
	public void driverCourseList() {

		final Object testingData[][] = {
			{
				"volunteer1", null

			}, {
				null, java.lang.IllegalArgumentException.class

			}, {
				"president", java.lang.ClassCastException.class

			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCourseList((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	/*
	 * Requisito funcional: un voluntario puede
	 * cancelar un curso de su lista de cursos.
	 * Se comprueban en el siguiente orden:
	 * Cancelar un curso aun abierto con un usuario voluntario(Correcto)
	 * Cancelar un curso aun abierto con un usuario null(Error)
	 * Cancelar un curso aun abierto con un usuario diferente a voluntario(Error)
	 * Cancelar un curso aun cerrado (Error)
	 * Cancelar un curso ya cancelado (Error)
	 */
	@Test
	public void driverCancelCourse() {
		Date d = new Date();
		@SuppressWarnings("unused")
		Date d2 = new Date();
		final Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.HOUR, 2);
		d = c.getTime();
		c.add(Calendar.HOUR, -10);
		d2 = c.getTime();

		final Object testingData[][] = {
			{
				"volunteer1", "title1", "description1", d, 20, "02:30", "OPEN", "https://www.google.es", null
			}, {
				null, "title1", "description1", d, 20, "02:30", "OPEN", "https://www.google.es", java.lang.IllegalArgumentException.class
			}, {
				"president", "title1", "description1", d, 20, "02:30", "OPEN", "https://www.google.es", javax.validation.ConstraintViolationException.class
			}, {
				"volunteer1", "title1", "description1", d, 20, "02:30", "CLOSED", "https://www.google.es", javax.validation.ConstraintViolationException.class
			}, {
				"volunteer1", "title1", "description1", d, 20, "02:30", "CANCELLED", "https://www.google.es", javax.validation.ConstraintViolationException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCreateCoures((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Date) testingData[i][3], (int) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (Class<?>) testingData[i][8]);
	}

	protected void templateCancelCourse(final String username, final String title, final String description, final Date moment, final int seats, final String duracion, final String status, final String imagen, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final Actor a = this.actorService.findByPrincipal();
			final Volunteer v = (Volunteer) a;

			Course c = this.courseService.create(v);
			c.setTitle(title);
			c.setDescription(description);
			c.setMoment(moment);
			c.setSeats(seats);
			c.setTotalDuration(duracion);
			c.setStatus(status);
			c.setImage(imagen);
			c = this.courseService.save(c);

			c = this.courseService.findOne(c.getId());
			c.setStatus("CANCELLED");
			this.courseService.save(c);

			this.unauthenticate();
			this.volunteerService.flush();
			this.courseService.flush();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	private void templateCourseList(final String username, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final Actor a = this.actorService.findByPrincipal();
			final Volunteer v = (Volunteer) a;
			final Collection<Course> lc = v.getCourses();

			this.unauthenticate();
			this.volunteerService.flush();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);

	}

	protected void templateCreateCoures(final String username, final String title, final String description, final Date moment, final int seats, final String duracion, final String status, final String imagen, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final Volunteer v = this.volunteerService.findByPrincipal();

			final Course c = this.courseService.create(v);
			c.setTitle(title);
			c.setDescription(description);
			c.setMoment(moment);
			c.setSeats(seats);
			c.setTotalDuration(duracion);
			c.setStatus(status);
			c.setImage(imagen);
			this.courseService.save(c);

			this.unauthenticate();
			this.volunteerService.flush();
			this.courseService.flush();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateRegister(final String username, final String password, final String repeatPassword, final String name, final String surname, final String email, final String phone, final String DNI, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			final Volunteer s = this.volunteerService.create();

			final UserAccount ua = new UserAccount();
			ua.setUsername(username);
			ua.setPassword(password);
			final Authority aut = new Authority();
			aut.setAuthority("VOLUNTEER");
			ua.addAuthority(aut);
			s.setUserAccount(ua);
			Assert.isTrue(password == repeatPassword);
			s.setEmail(email);
			s.setName(name);
			s.setSurname(surname);
			s.setPhone(phone);
			s.setDNI(DNI);
			s.setMessagesIncoming(new HashSet<Message>());
			s.setMessagesOutgoing(new HashSet<Message>());
			s.setCourses(new HashSet<Course>());

			this.volunteerService.save(s);
			this.volunteerService.flush();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateEdit(final String username, final String name, final String surname, final String email, final String phone, final String DNI, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final Volunteer v = this.volunteerService.findByPrincipal();

			v.setEmail(email);
			v.setName(name);
			v.setSurname(surname);
			v.setPhone(phone);
			v.setDNI(DNI);
			this.volunteerService.edit(v);
			this.volunteerService.flush();

			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

}
