
package usercases;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.ActivityService;
import services.AssociateService;
import services.CommentService;
import services.RequestService;
import utilities.AbstractTest;
import domain.Activity;
import domain.Associate;
import domain.Comment;
import domain.Request;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class ActivityCaseTest extends AbstractTest {

	@Autowired
	private AssociateService	associateService;
	@Autowired
	private RequestService		requestService;
	@Autowired
	private ActivityService		activityService;
	@Autowired
	private CommentService		commentService;


	/*
	 * Requisito funcional: Mandar una solicitud para acudir a una actividad abierta.
	 * Se comprueba en el siguiente orden:
	 * Mandar una solicitud a una actividad abierta y que haya sitio disponible como asociado(Correcto)
	 * Mandar una solicitud a una actividad sin estar logueado(Fallo)
	 * Mandar una solicitud a una actividad que no existe(Fallo)
	 * Mandar una solicitud a una actividad en el que ya estoy registrado(Fallo)
	 * Mandar una solicitud a una actividad como presidente(Fallo)
	 * Mandar una solicitud a una actividad que no hay sitio disponible(Fallo)
	 * Mandar una solcitud a una actividad null(Fallo)
	 */
	@Test
	public void driverSendRequestActivity() {

		final Object testingData[][] = {

			{
				"associate1", 86, null
			}

			, {
				null, 86, IllegalArgumentException.class
			}, {
				"associate1", 928399, ConstraintViolationException.class

			}, {
				"associate3", 89, ConstraintViolationException.class

			}, {
				"president", 86, ConstraintViolationException.class
			}, {
				"associate1", 65536, ConstraintViolationException.class
			}, {
				"associate1", null, InvalidDataAccessApiUsageException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateSendRequestActivity((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	protected void templateSendRequestActivity(final String username, final Integer activityId, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Associate a = this.associateService.findByPrincipal();
			final Activity act = this.activityService.findOne(activityId);
			final Request r = this.requestService.create(act, a);
			r.setSeats(1);
			this.requestService.save(r);
			this.requestService.flush();
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Requisito funcional: Cancelar una solicitud de una actividad en la que estoy registrada.
	 * Se comprueba en el siguiente orden:
	 * Cancelar una solicitud de una actividad como asociado(Correcto)
	 * Cancelar una solicitud de una actividad sin estar logueado(Fallo)
	 * Cancelar una solicitud de una actividad que no existe(Fallo)
	 * Cancelar una solicitud de una actividad en la que no estoy registrado(Fallo)
	 * Cancelar una solicitud de una actividad que tengo denegada(Fallo)
	 * Cancelar una solicitud de una actividad como presidente(Fallo)
	 * Cancelar una solicitud de una actividad null(Fallo)
	 */
	@Test
	public void driverCancelRequestActivity() {

		final Object testingData[][] = {

			{
				"associate3", 89, null
			}, {
				"associate1", 1, NullPointerException.class
			}, {
				"associate1", 78, NullPointerException.class
			}, {
				null, 76, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCancelRequestActivity((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	protected void templateCancelRequestActivity(final String username, final Integer activityId, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Associate a = this.associateService.findByPrincipal();

			final Request r = this.requestService.findequest(a.getId(), activityId);
			Assert.isTrue(r.getStatus().equals("ACCEPTED"));
			this.requestService.delete(r);

			this.unauthenticate();
			this.requestService.flush();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Requisito funcional: Escribir un comentario a una actividad en la que he participado.
	 * Se comprueba en el siguiente orden:
	 * Escribir un comentario a una actividad como asociado(Correcto)
	 * Escribir un comentario a una actividad sin estar logueado(Fallo)
	 * Escribir un comentario a una actividad que no existe(Fallo)
	 * Escribir un comentario a una actividad en la que no he asistido(Fallo)
	 * Escribir un comentario a una actividad que tengo denegada(Fallo)
	 * Escribir un comentario a una actividad como presidente(Fallo)
	 * Escribir un comentario a una actividad null(Fallo)
	 */
	@Test
	public void driverWriteComment() {
		final Object testingData[][] = {
			{
				"associate1", 87, null
			}, {
				null, 85, IllegalArgumentException.class
			}, {
				"associate1", 9999, NullPointerException.class
			}, {
				"associate1", 86, NullPointerException.class
			}, {
				"associate2", 87, NullPointerException.class
			}, {
				"president", 87, NullPointerException.class
			}, {
				"associate1", null, InvalidDataAccessApiUsageException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateWriteComment((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	protected void templateWriteComment(final String username, final Integer activityId, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Associate a = this.associateService.findByPrincipal();
			final Activity act = this.activityService.findOne(activityId);
			final Request r = this.requestService.findequest(a.getId(), activityId);
			Assert.isTrue(r.getStatus().equals("ACCEPTED"));
			Assert.isTrue(act.getStatus().equals("CLOSED"));
			final Comment c = this.commentService.create(act, a);
			c.setTitle("Good activity");
			c.setDescription("It's a good activity! So funny!");
			this.commentService.save(c);
			this.commentService.flush();
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
}
