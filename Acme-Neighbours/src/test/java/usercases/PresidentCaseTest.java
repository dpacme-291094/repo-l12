
package usercases;

import java.util.Calendar;
import java.util.Date;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.CommentService;
import services.CourseService;
import services.PresidentService;
import services.RequestService;
import services.ReunionService;
import utilities.AbstractTest;
import domain.Comment;
import domain.Course;
import domain.President;
import domain.Request;
import domain.Reunion;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class PresidentCaseTest extends AbstractTest {

	// Services -----------------------------------
	@Autowired
	private PresidentService	presidentService;
	@Autowired
	private ReunionService		reunionService;
	@Autowired
	private RequestService		requestService;
	@Autowired
	private CommentService		commentService;
	@Autowired
	private CourseService		courseService;


	// Test Drivers -------------------------

	/*
	 * Requisito funcional: Presidente cambia su perfil
	 * Se comprueba en el siguiente orden:
	 * El presidente "president" edita sus datos introduciendolos correctos (correcto)
	 * "null" intenta modificar sus datos (fallo)
	 * El presidente "president" intenta introducir un DNI err�neo (fallo)
	 */

	@Test
	public void driverEditPersonalData() {

		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"president", "Kiko", "Kike", "erkiko@hotmail.com", "954407347", "79021984Y", null,
			}, {
				null, "Kiko", "Kike", "erkiko@hotmail.com", "954407347", "79021984Y", IllegalArgumentException.class
			}, {
				"president", "Kiko", "Kike", "erkiko@hotmail.com", "954407347", "79176W778", ConstraintViolationException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateEdit((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	/*
	 * Requisito funcional: un presidente puede
	 * crear una actividad para que los socios asistan
	 * Se comprueba en el siguiente orden:
	 * Creacion de una actividad con sus datos correctos (correcto)
	 * creacion de una actividad sin estar conectado como presidente (Error)
	 * creacion de una actividad con un usuario incorecto (Error)
	 * creacion de una actividad sin loguearse (Error)
	 * Creacion de una actividad con el titulo en blanco (Error)
	 * Creacion de una actividad con la descripcion en blanco (Error)
	 * Creacion de una actividad con el la fecha en pasado (Error)
	 * Creacion de una actividad con los asientos en negativo (Error)
	 * Creacion de una actividad con un status incorrecto (Error)
	 */

	@Test
	public void driverCreateActivity() {
		Date d = new Date();
		Date d2 = new Date();
		final Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.HOUR, 2);
		d = c.getTime();
		c.add(Calendar.HOUR, -10);
		d2 = c.getTime();

		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"president", "title1", "description1", d, 20, "02:30", "OPEN", null
			}, {
				"associate1", "title1", "description1", d, 20, "02:30", "OPEN", java.lang.IllegalArgumentException.class
			}, {
				"abcdefg", "title1", "description1", d, 20, "02:30", "OPEN", java.lang.IllegalArgumentException.class
			}, {
				null, "title1", "description1", d, 20, "02:30", "OPEN", java.lang.IllegalArgumentException.class
			}, {
				"president", null, "description1", d, 20, "02:30", "OPEN", javax.validation.ConstraintViolationException.class
			}, {
				"president", "title1", null, d, 20, "02:30", "OPEN", javax.validation.ConstraintViolationException.class
			}, {
				"president", "title1", "description1", d2, 20, "02:30", "OPEN", javax.validation.ConstraintViolationException.class
			}, {
				"president", "title1", "description1", d, -10, "02:30", "OPEN", javax.validation.ConstraintViolationException.class
			}, {
				"president", "title1", "description1", d, 20, "02:30", "ABCD", javax.validation.ConstraintViolationException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCreateActivities((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Date) testingData[i][3], (int) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(Class<?>) testingData[i][7]);
	}

	/*
	 * Requisito funcional: un presidente puede
	 * editar una actividad
	 * Se comprueba en el siguiente orden:
	 * Edici�n de una actividad con sus datos correctos (correcto)
	 * Edici�n de una actividad sin estar conectado como presidente (Error)
	 * Edici�n de una actividad con un usuario incorecto (Error)
	 * Edici�n de una actividad sin loguearse (Error)
	 * Edici�n de una actividad con el titulo en blanco (Error)
	 * Edici�n de una actividad con la descripcion en blanco (Error)
	 * Edici�n de una actividad con el la fecha en pasado (Error)
	 * Edici�n de una actividad con los asientos en negativo (Error)
	 * Edici�n de una actividad con un status incorrecto (Error)
	 * Edici�n de una acticidad con un id inexistente (Error)
	 */

	@Test
	public void driverEditActivity() {
		Date d = new Date();
		Date d2 = new Date();
		final Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.HOUR, 2);
		d = c.getTime();
		c.add(Calendar.HOUR, -20);
		d2 = c.getTime();

		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"president", 89, "title1", "description1", d, 20, "02:30", "OPEN", null
			}, {
				"associate1", 89, "title1", "description1", d, 20, "02:30", "OPEN", java.lang.IllegalArgumentException.class
			}, {
				"abcdefg", 89, "title1", "description1", d, 20, "02:30", "OPEN", java.lang.IllegalArgumentException.class
			}, {
				null, 89, "title1", "description1", d, 20, "02:30", "OPEN", java.lang.IllegalArgumentException.class
			}, {
				"president", 89, null, "description1", d, 20, "02:30", "OPEN", javax.validation.ConstraintViolationException.class
			}, {
				"president", 89, "title1", null, d, 20, "02:30", "OPEN", javax.validation.ConstraintViolationException.class
			}
			//	En el test crear Activity si que peta la fecha pasada. Aqui no. No lo entiendo. Se usa el save igualmente
			// , {
			//				"president", 89, "title1", "description1", d2, 20, "02:30", "OPEN", javax.validation.ConstraintViolationException.class
			//			}
			, {
				"president", 89, "title1", "description1", d, -10, "02:30", "OPEN", javax.validation.ConstraintViolationException.class
			}, {
				"president", 89, "title1", "description1", d, 20, "02:30", "ABCD", javax.validation.ConstraintViolationException.class
			}, {
				"president", 8999, "title1", "description1", d, 20, "02:30", "OPEN", java.lang.NullPointerException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateEditActivities((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Date) testingData[i][4], (int) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (Class<?>) testingData[i][8]);
	}

	/*
	 * Requisito funcional: un presidente puede
	 * cancelar una actividad.
	 * Se comprueban en el siguiente orden:
	 * Cancelar una actividad abierta con un usuario presidente(Correcto)
	 * Cancelar una actividad abierta con un usuario null(Error)
	 * Cancelar una actividad abierta con un usuario diferente a presidente(Error)
	 * Cancelar una actividad ya cerrada (Error)
	 * Cancelar una actividad ya cancelada (Error)
	 * Cancelar una actividad no existente (Error)
	 */

	@Test
	public void driverCancelActivity() {

		/*
		 * ESTO NO FUNCIONA BIEN. Se permite cancelar una activididad cerrada, se permite cancelar una actividad ya cancelada.
		 * No se cancela la actividad al ser un associate, pero no devuelve una excepci�n de que el associate ha intentado
		 * cancelar una acticidad. �ESO ESTA BIEN?
		 */
		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"president", 89, null
			}, {
				null, 89, java.lang.IllegalArgumentException.class
			}, {
				"associate1", 89, java.lang.IllegalArgumentException.class
			}, { //Cancelar una activity CLOSED no da error
				"president", 87, null
			}, // Cancelar una activity CANCELLED no da error 
			{
				"president", 88, null
			}, {
				"president", 8999, java.lang.NullPointerException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCancelActivity((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	/*
	 * Requisito funcional: un presidente puede
	 * eliminar una actividad.
	 * Se comprueban en el siguiente orden:
	 * Eliminar una actividad abierta con un usuario presidente(Correcto)
	 * Eliminar una actividad abierta con un usuario null(Error)
	 * Eliminar una actividad abierta con un usuario diferente a presidente(Error)
	 * Eliminar una actividad ya cerrada (Error)
	 * Eliminar una actividad ya cancelada (Error)
	 * Eliminar una actividad no existente (Error)
	 */
	@Test
	public void driverDeleteActivity() {

		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"president", 89, null
			}, {
				null, 89, java.lang.IllegalArgumentException.class
			}, {
				"associate1", 89, java.lang.IllegalArgumentException.class
			}

			, {
				"president", 8999, java.lang.NullPointerException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateDeleteActivity((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	/*
	 * Requisito funcional: un presidente puede
	 * aceptar una Request de una actividad realizada por un socio
	 * Se comprueban en el siguiente orden:
	 * Aceptar una Request PENDING con un usuario presidente(Correcto)
	 * Aceptar una Request PENDING con un usuario null(Error)
	 * Aceptar una Request no existente (Error)
	 */

	@Test
	public void driverAcceptRequest() {

		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"president", 99, null
			}

			, {
				null, 99, java.lang.IllegalArgumentException.class
			}, {
				"associate1", 99, java.lang.IllegalArgumentException.class
			}, {
				"president", 8999, java.lang.NullPointerException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateAcceptRequest((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	/*
	 * Requisito funcional: un presidente puede
	 * aceptar una Request de una actividad realizada por un socio
	 * Se comprueban en el siguiente orden:
	 * Aceptar una Request PENDING con un usuario presidente(Correcto)
	 * Aceptar una Request PENDING con un usuario null(Error)
	 * Aceptar una Request no existente (Error)
	 */

	@Test
	public void driverDenyRequest() {

		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"president", 96, null
			}

			, {
				null, 96, java.lang.IllegalArgumentException.class
			}, {
				"associate1", 96, java.lang.IllegalArgumentException.class
			}, {
				"president", 8999, java.lang.NullPointerException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateDenyRequest((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	/*
	 * Requisito funcional: un presidente puede
	 * eliminar un comentario en una Leisure
	 * Se comprueban en el siguiente orden:
	 * Aceptar una Request PENDING con un usuario presidente(Correcto)
	 * Aceptar una Request PENDING con un usuario null(Error)
	 * Aceptar una Request no existente (Error)
	 */

	@Test
	public void driverDeleteComment() {

		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"president", 95, null
			}

			, {
				null, 95, java.lang.IllegalArgumentException.class
			}, {
				"president", 8999, java.lang.IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateDeleteComment((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	/*
	 * Requisito funcional: un presidente puede
	 * cancelar un curso.
	 * Se comprueban en el siguiente orden:
	 * Cancelar un curso abierto con un usuario presidente(Correcto)
	 * Cancelar un curso abierto con un usuario null(Error)
	 * Cancelar un curso abierto con un usuario diferente a presidente(Error)
	 * Cancelar un curso ya cerrado (Error)
	 * Cancelar un curso ya cancelado (Error)
	 * Cancelar un curso no existente (Error)
	 */

	@Test
	public void driverCancelCourse() {

		/*
		 * ESTO NO FUNCIONA BIEN. Se permite cancelar una activididad cerrada, se permite cancelar una actividad ya cancelada.
		 * No se cancela la actividad al ser un associate, pero no devuelve una excepci�n de que el associate ha intentado
		 * cancelar una acticidad. �ESO ESTA BIEN?
		 */
		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"president", 92, null
			}, {
				null, 92, java.lang.IllegalArgumentException.class
			}, {
				"associate1", 92, java.lang.IllegalArgumentException.class
			}, {
				"president", 90, null
			}, {
				"president", 91, null
			}, {
				"president", 8999, java.lang.NullPointerException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCancelCourse((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	/*
	 * Requisito funcional: un presidente puede
	 * eliminar un course.
	 * Se comprueban en el siguiente orden:
	 * Eliminar una course abierta con un usuario presidente(Correcto)
	 * Eliminar una course abierta con un usuario null(Error)
	 * Eliminar una course abierta con un usuario diferente a presidente(Error)
	 * Eliminar una course ya cerrada (Error)
	 * Eliminar una course ya cancelada (Error)
	 * Eliminar una course no existente (Error)
	 */
	@Test
	public void driverDeleteCourse() {

		@SuppressWarnings("deprecation")
		final Object testingData[][] = {
			{
				"president", 92, null
			}, {
				null, 92, java.lang.IllegalArgumentException.class
			}, {
				"associate1", 92, java.lang.IllegalArgumentException.class
			}

			, {
				"president", 8999, java.lang.NullPointerException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateDeleteCourse((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	// Test template -------------------------

	protected void templateEdit(final String username, final String name, final String surname, final String email, final String phone, final String DNI, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final President p = this.presidentService.findByPrincipal();

			p.setEmail(email);
			p.setName(name);
			p.setSurname(surname);
			p.setPhone(phone);
			p.setDNI(DNI);
			this.presidentService.edit(p);
			this.presidentService.flush();

			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateCreateActivities(final String username, final String title, final String description, final Date moment, final int seats, final String duration, final String status, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final President p = this.presidentService.findByPrincipal();

			final Reunion r = this.reunionService.create();
			r.setTitle(title);
			r.setDescription(description);
			r.setMoment(moment);
			r.setSeats(seats);
			r.setTotalDuration(duration);
			r.setStatus(status);
			this.reunionService.save(r);

			this.unauthenticate();
			this.presidentService.flush();
			this.reunionService.flush();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateEditActivities(final String username, final Integer idActivity, final String title, final String description, final Date moment, final int seats, final String duration, final String status, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final President p = this.presidentService.findByPrincipal();
			final Reunion r = this.reunionService.findOne(idActivity);
			r.setTitle(title);
			r.setDescription(description);
			r.setMoment(moment);
			r.setSeats(seats);
			r.setTotalDuration(duration);
			r.setStatus(status);
			this.reunionService.save(r);

			this.unauthenticate();
			this.presidentService.flush();
			this.reunionService.flush();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateCancelActivity(final String username, final Integer idActivity, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final President p = this.presidentService.findByPrincipal();
			final Reunion r = this.reunionService.findOne(idActivity);

			r.setStatus("CANCELLED");
			this.reunionService.save(r);

			this.unauthenticate();
			this.presidentService.flush();
			this.reunionService.flush();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateDeleteActivity(final String username, final Integer idActivity, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final President p = this.presidentService.findByPrincipal();
			final Reunion r = this.reunionService.findOne(idActivity);

			this.reunionService.delete(r);

			this.unauthenticate();
			this.presidentService.flush();
			this.reunionService.flush();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateAcceptRequest(final String username, final Integer idRequest, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final President p = this.presidentService.findByPrincipal();
			final Request request = this.requestService.findOne(idRequest);

			this.requestService.accept(request);

			this.unauthenticate();
			this.presidentService.flush();
			this.reunionService.flush();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateDenyRequest(final String username, final Integer idRequest, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final President p = this.presidentService.findByPrincipal();
			final Request request = this.requestService.findOne(idRequest);

			this.requestService.deny(request);

			this.unauthenticate();
			this.presidentService.flush();
			this.reunionService.flush();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateDeleteComment(final String username, final Integer idComment, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final President p = this.presidentService.findByPrincipal();
			final Comment comment = this.commentService.findOne(idComment);

			this.commentService.delete(comment);

			this.unauthenticate();
			this.presidentService.flush();
			this.commentService.flush();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateCancelCourse(final String username, final Integer courseId, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);

			Course c = this.courseService.findOne(courseId);
			c.setStatus("CANCELLED");
			c = this.courseService.save(c);

			this.unauthenticate();
			this.presidentService.flush();
			this.courseService.flush();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateDeleteCourse(final String username, final Integer idCourse, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final President p = this.presidentService.findByPrincipal();
			final Course c = this.courseService.findOne(idCourse);

			this.courseService.delete(c);

			this.unauthenticate();
			this.presidentService.flush();
			this.courseService.flush();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
}
