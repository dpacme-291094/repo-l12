
package usercases;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import security.Authority;
import security.UserAccount;
import services.AssociateService;
import utilities.AbstractTest;
import domain.Associate;
import domain.CreditCard;
import domain.Message;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class AssociateCaseTest extends AbstractTest {

	@Autowired
	private AssociateService	associateService;


	/*
	 * Requisito funcional: Registro como un/a socio
	 * Se comprueba en el siguiente orden:
	 * Registrarse con todos los datos correctos (Correcto)
	 * Registrarse con un username que ya existe (Error)
	 * Registrarse sin seguir el patron de telefono ni email (Error)
	 * Registrarse con un DNI inv�lido (Error)
	 * Registrarse sin coincidir las contrase�as (Erroneo)
	 * Registrarse con numero de tarjeta erronea ( Erroneo)
	 */

	@Test
	public void driverRegistrarSocio() {

		final Object testingData[][] = {
			{
				"associate10", "123456", "123456", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "75893212A", "Juana", "VISA", "4584863272547693", 10, 2019, 312, null

			}, {
				"associate1", "123456", "123456", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79029894Y", "Juana", "VISA", "4584863272547693", 10, 2019, 312, DataIntegrityViolationException.class
			}, {
				"associate10", "123456", "123456", "Juana", "Martinez", "jmartines.com", "123", "79029894Y", "Juana", "VISA", "4584863272547693", 10, 2019, 312, DataIntegrityViolationException.class
			}, {
				"associate10", "123456", "123456", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79YSA894Y", "Juana", "VISA", "4584863272547693", 10, 2019, 312, DataIntegrityViolationException.class
			}, {
				"associate10", "123456", "mal", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79029894Y", "Juana", "VISA", "4584863272547693", 10, 2019, 312, IllegalArgumentException.class

			}, {
				"associate10", "123456", "s", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79029894Y", "Juana", "VISA", "4584863", 10, 2019, 312, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateRegister((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (String) testingData[i][8], (String) testingData[i][9], (String) testingData[i][10], (int) testingData[i][11], (int) testingData[i][12], (int) testingData[i][13], (Class<?>) testingData[i][14]);
	}
	/*
	 * Requisito funcional: Associate change his or her profile
	 * Se comprueba en el siguiente orden:
	 * El socio "associate1" edita sus datos introduciendolos correctos (correcto)
	 * "null" intenta modificar sus datos (fallo)
	 * El socio "associate1" intenta introducir un DNI err�neo (fallo)
	 */
	@Test
	public void driverEditPersonalData() {

		final Object testingData[][] = {
			{
				"associate1", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79029984Y", null,

			}, {
				null, "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79029984Y", IllegalArgumentException.class
			}, {
				"associate1", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79076W778", ConstraintViolationException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateEdit((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	/*
	 * Requisito funcional: Browse the list of associate who have registered to the system
	 * Se comprueba en el siguiente orden:
	 * Listar la lista registrado como presidente(Correcto)
	 * Listar la lista registrado como asociado(Fallo)
	 * Listar la lista sin estar registrado(Fallo)
	 */
	@Test
	public void driverListAssociate() {

		final Object testingData[][] = {
			{
				"president", null
			}, {
				"associate1", IllegalArgumentException.class

			}, {
				null, NullPointerException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateList((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
	protected void templateList(final String username, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			Assert.isTrue(username.equals("president"));
			final Collection<Associate> ca = this.associateService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	protected void templateRegister(final String username, final String password, final String repeatPassword, final String name, final String surname, final String email, final String phone, final String DNI, final String holderName,
		final String brandName, final String number, final int expirationMonth, final int expirationYear, final int codeCVV, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			final Associate s = this.associateService.create();

			final UserAccount ua = new UserAccount();
			final CreditCard cc = new CreditCard();
			ua.setUsername(username);
			ua.setPassword(password);
			final Authority aut = new Authority();
			aut.setAuthority("ASSOCIATE");
			ua.addAuthority(aut);
			s.setUserAccount(ua);
			Assert.isTrue(password == repeatPassword);
			s.setEmail(email);
			s.setName(name);
			s.setSurname(surname);
			s.setPhone(phone);
			s.setDNI(DNI);
			s.setCreditCard(cc);

			cc.setBrandName(brandName);
			cc.setHolderName(holderName);
			cc.setNumber(number);
			cc.setExpirationMonth(expirationMonth);
			cc.setExpirationYear(expirationYear);
			cc.setCodeCVV(codeCVV);
			s.setMessagesIncoming(new HashSet<Message>());
			s.setMessagesOutgoing(new HashSet<Message>());
			s.setLastPayment(new Date());
			this.associateService.save(s);
			this.associateService.flush();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateEdit(final String username, final String name, final String surname, final String email, final String phone, final String DNI, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {

			this.authenticate(username);
			final Associate v = this.associateService.findByPrincipal();

			v.setEmail(email);
			v.setName(name);
			v.setSurname(surname);
			v.setPhone(phone);
			v.setDNI(DNI);
			this.associateService.edit(v);
			this.associateService.flush();

			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

}
