
package usercases;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.AssociateService;
import services.OfferService;
import services.PetitionService;
import services.RoomService;
import services.SecretaryService;
import utilities.AbstractTest;
import domain.Associate;
import domain.Offer;
import domain.Petition;
import domain.Room;
import domain.Secretary;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class OfferCaseTest extends AbstractTest {

	@Autowired
	private AssociateService	associateService;
	@Autowired
	private PetitionService		petitionService;
	@Autowired
	private OfferService		offerService;

	@Autowired
	private SecretaryService	secretaryService;
	@Autowired
	private RoomService			roomService;


	/*
	 * Requisito funcional: Mandar una peticion a una oferta libre para reservar una sala.
	 * Se comprueba en el siguiente orden:
	 * Mandar una peticion para una oferta disponible como asociado(Correcto)
	 * Mandar una peticion para una oferta disponible sin estar logueado(Fallo)
	 * Mandar una peticion para una oferta que no existe(Fallo)
	 * Mandar una peticion para una oferta disponible como presidente(Fallo)
	 * Mandar una peticion para una sala que ya tengo reservada(Fallo)
	 * Mandar una peticion para una oferta ocupada(Fallo)
	 * Mandar una peticion para una oferta null(Fallo)
	 */
	@Test
	public void driverSendPetitionOffer() {

		final Object testingData[][] = {

			{
				"associate1", 110, null
			}, {
				null, 111, IllegalArgumentException.class
			}, {
				"associate1", 99999, ConstraintViolationException.class
			}, {
				"president", 111, ConstraintViolationException.class

			}, {
				"associate1", 65538, ConstraintViolationException.class
			}, {
				"associate1", 65539, ConstraintViolationException.class
			}, {
				"associate1", null, InvalidDataAccessApiUsageException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateSendPetition((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	protected void templateSendPetition(final String username, final Integer offerId, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Associate a = this.associateService.findByPrincipal();
			final Offer o = this.offerService.findOne(offerId);
			final Petition p = this.petitionService.create(o, a);
			p.setDescription("Reunion");
			this.petitionService.save(p);
			this.petitionService.flush();
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Requisito funcional: Cancelar una peticion de una sala que tengo reservada.
	 * Se comprueba en el siguiente orden:
	 * Cancelar una peticion de una sala como asociado(Correcto)
	 * Cancelar una peticion de una sala sin estar logueado(Fallo)
	 * Cancelar una peticion de una sala que no existe(Fallo)
	 * Cancelar una peticion de una sala que no tengo reservada(Fallo)
	 * Cancelar una peticion de una sala que tengo denegada(Fallo)
	 * Cancelar una peticion de una sala como presidente(Fallo)
	 * 
	 * Cancelar una peticion de una sala null(Fallo)
	 */
	@Test
	public void driverCancelPetitionOffer() {

		final Object testingData[][] = {

			{
				"associate2", 108, null
			}, {
				null, 108, IllegalArgumentException.class
			}, {
				"associate1", 99999, NullPointerException.class
			}, {
				"associate1", 111, NullPointerException.class
			}, {
				"associate2", 109, IllegalArgumentException.class
			}, {
				"president", 108, NullPointerException.class
			}, {
				"associate1", null, NullPointerException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCancelPetitionOffer((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	protected void templateCancelPetitionOffer(final String username, final Integer offerId, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Associate a = this.associateService.findByPrincipal();

			final Petition p = this.petitionService.findPetitionOffer(offerId, a.getId());
			Assert.isTrue(p.getStatus().equals("ACCEPTED"));
			this.petitionService.delete(p);

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Requisito funcional:Crear una oferta para una sala.
	 * Se comprueba en el siguiente orden:
	 * Crear una offer de una sala como secretario(Correcto)
	 * Crear una offer de una sala sin estar logueado(Fallo)
	 * Crear una offer de una sala que no existe(Fallo)
	 * Crear una offer de una sala como president(Fallo)
	 * Crear una offer de una sala null(Fallo)
	 */
	@Test
	public void driverCreateOfferRoom() {

		final Object testingData[][] = {

			{
				"secretary", 106, null
			}, {
				null, 106, IllegalArgumentException.class
			}, {
				"secretary", 99999, ConstraintViolationException.class
			}, {
				"president", 106, ConstraintViolationException.class
			}, {
				"secretary", null, InvalidDataAccessApiUsageException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateOfferRoom((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	protected void templateOfferRoom(final String username, final Integer roomId, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Secretary a = this.secretaryService.findByPrincipal();

			final Room r = this.roomService.findOne(roomId);
			final Offer o = this.offerService.create(r, a);
			final Date dd = new Date("2017/04/12 12:00");
			o.setDateRequest(dd);
			o.setTime("03:00");
			o.setRoom(r);
			o.setPetitions(new HashSet<Petition>());
			this.offerService.save(o);
			this.offerService.flush();

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Requisito funcional:Cancelar una oferta de una sala que no haya pasado 24h de su creaci�n.
	 * Se comprueba en el siguiente orden:
	 * Cancelar una offer de una sala como secretario(Correcto)
	 * Cancelar una offer de una sala sin estar logueado(Fallo)
	 * Cancelar una offer de una sala que no existe(Fallo)
	 * Cancelar una offer de una sala como president(Fallo)
	 * Cancelar una offer de una sala pasada(Fallo)
	 * Cancelar una offer de una sala null(Fallo)
	 */
	@Test
	public void driverCancelOfferRoom() {

		final Object testingData[][] = {

			{
				"secretary", 111, null
			}, {
				null, 111, NullPointerException.class
			}, {
				"secretary", 99999, NullPointerException.class
			}, {
				"president", 111, NullPointerException.class
			}, {
				"secretary", 110, IllegalArgumentException.class
			}, {
				"secretary", null, InvalidDataAccessApiUsageException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCancelOffer((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	protected void templateCancelOffer(final String username, final Integer offerId, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);

			final Offer o = this.offerService.findOne(offerId);
			this.offerService.delete(o);

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Requisito funcional:Aceptar una peticion de una oferta de una sala.
	 * Se comprueba en el siguiente orden:
	 * Aceptar una peticion de una offer de una sala como secretario(Correcto)
	 * Aceptar una peticion de una offer de una sala sin estar logueado(Fallo)
	 * Aceptar una peticion de una offer que no existe(Fallo)
	 * Aceptar una peticion de una offer como president(Fallo)
	 * Aceptar una peticion de una offer pasada(Fallo)
	 * Aceptar una peticion de una offer null(Fallo)
	 */
	@Test
	public void driverAcceptPetitionOffer() {

		final Object testingData[][] = {

			{
				"secretary", 111, null
			}, {
				null, 111, IllegalArgumentException.class
			}, {
				"secretary", 99999, ConstraintViolationException.class
			}, {
				"president", 111, IllegalArgumentException.class
			}, {
				"secretary", 110, DataIntegrityViolationException.class
			}, {
				"secretary", null, InvalidDataAccessApiUsageException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateAcceptPetition((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	protected void templateAcceptPetition(final String username, final Integer offerId, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			Assert.notNull(this.secretaryService.findByPrincipal());
			final Associate a = this.associateService.findOne(73);
			final Offer o = this.offerService.findOne(offerId);
			final Petition pe = this.petitionService.create(o, a);
			pe.setDescription("Cumplea�os");
			this.petitionService.save(pe);
			this.petitionService.flush();
			this.petitionService.accept(pe);
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Requisito funcional:Aceptar una peticion de una oferta de una sala.
	 * Se comprueba en el siguiente orden:
	 * Aceptar una peticion de una offer de una sala como secretario(Correcto)
	 * Aceptar una peticion de una offer de una sala sin estar logueado(Fallo)
	 * Aceptar una peticion de una offer que no existe(Fallo)
	 * Aceptar una peticion de una offer como president(Fallo)
	 * Aceptar una peticion de una offer pasada(Fallo)
	 * Aceptar una peticion de una offer null(Fallo)
	 */
	@Test
	public void driverDenyPetitionOffer() {

		final Object testingData[][] = {

			{
				"secretary", 112, null
			}, {
				null, 112, IllegalArgumentException.class
			}, {
				//				"secretary", 99999, NullPointerException.class
				//			}, {
				"president", 112, NullPointerException.class
			}, {
				"secretary", 113, IllegalArgumentException.class
			}, {
				"secretary", null, InvalidDataAccessApiUsageException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateDenyPetition((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	protected void templateDenyPetition(final String username, final Integer petitionId, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Secretary s = this.secretaryService.findByPrincipal();
			final Collection<Petition> cp = this.petitionService.findPetitions(s.getId());
			final Petition p = this.petitionService.findOne(petitionId);

			if (cp.contains(p))
				this.petitionService.deny(p);
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
}
