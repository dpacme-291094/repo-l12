
package usercases;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ActorService;
import services.MessageService;
import utilities.AbstractTest;
import domain.Actor;
import domain.Message;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class MessageCaseTest extends AbstractTest {

	@Autowired
	private MessageService	messageService;
	@Autowired
	private ActorService	actorService;


	//Requisito Funcional: Exchange messages with another actors
	/*
	 * En el siguiente orden comprueba:
	 * Envio de un mensaje con todos los datos correctos.(correcto)
	 * Envio de un mensaje con todos los datos correctos de un associate a otro. (erroneo)
	 * Envio de un mensaje con un associate erroneo. (erroneo)
	 * Envio de un mensaje a un email que no existe.(erroneo)
	 * Envio de un mensaje con "attachments" con URL mal.(erroneo)
	 * Envio de un mensaje a un email que no cumple el @Email.(erroneo)
	 * Envio de un mensaje con todos los campos en blanco.(erroneo)
	 * Envio de un mensaje sin estar autenticado.(erroneo)
	 * Envio de un mensaje con HTML en alg�n campo.(erroneo)
	 */
	@Test
	public void driverSave() {

		final Object testingData[][] = {
			{
				"associate1", "admin@gmail.com", "Prueba", "prueba", "", null
			}, {
				"associate1", "gabequer@gmail.com", "Prueba", "prueba", "", IllegalArgumentException.class
			}, {
				"associate", "admin@gmail.com", "Prueba", "prueba", "", IllegalArgumentException.class
			}, {
				"associate1", "adminmal@gmail.comadmin@gmail.com", "Prueba", "prueba", "", IllegalArgumentException.class

			}, {
				"associate1", "admin@gmail.com", "Prueba", "prueba", "urlmal", IllegalArgumentException.class

			}, {
				"associate", "admingmail.com", "Prueba", "prueba", "", IllegalArgumentException.class

			}, {
				"associate1", "", "", "", "", IllegalArgumentException.class
			}, {
				"associate1", "admin@gmail.com", "<p>", "<b>", "", ConstraintViolationException.class

			}, {
				null, "admin@gmail.com", "Prueba", "prueba", "", IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateEdit((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (Class<?>) testingData[i][5]);

	}

	//Requisito Funcional: Erase his or her messages
	/*
	 * Se comprueba en el siguiente orden:
	 * Eliminar correctamente un mensaje. (correcto)
	 * Eliminar un mensaje que no existe.(erroneo)
	 * Eliminar un mensaje que no es tuyo.(erroneo)
	 * Eliminar un mensaje sin estar autenticado.(erroneo)
	 */
	@Test
	public void driverDelete() {

		final Object testingData[][] = {

			{
				"associate1", 76, null
			}, {
				"associate1", 1, NullPointerException.class
			}, {
				"associate1", 78, IllegalArgumentException.class
			}, {
				null, 76, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateDelete((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}

	//Requisito Funcional: List the messages that he/she is got
	/*
	 * Se comprueba la lista en el siguiente orden:
	 * Acceder a la lista de mensajes de un usuario logueado (correcto)
	 * Acceder a la lista de mensajes de un usuario no autenticado (erroneo)
	 */
	@Test
	public void driverList() {

		final Object testingData[][] = {

			{
				"associate1", null
			}, {
				null, IllegalArgumentException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateList((String) testingData[i][0], (Class<?>) testingData[i][1]);

	}

	protected void templateEdit(final String username, final String recipient, final String title, final String text, final String attachments, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Message a = this.messageService.create();

			a.setEmailRecipient(recipient);
			a.setAttachments(attachments);
			a.setSubject(title);
			a.setText(text);
			this.messageService.save(a);
			this.messageService.flush();
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	protected void templateList(final String username, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Actor c = this.actorService.findByPrincipal();

			c.getMessagesIncoming();
			c.getMessagesOutgoing();
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateDelete(final String username, final Integer messageId, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Message a = this.messageService.findOne(messageId);
			this.messageService.deleteWR(a);
			this.messageService.flush();
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
}
