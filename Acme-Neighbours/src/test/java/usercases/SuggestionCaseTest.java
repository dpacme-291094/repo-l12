
package usercases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.AnswerService;
import services.SuggestionService;
import services.TreasurerService;
import utilities.AbstractTest;
import domain.Answer;
import domain.Treasurer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class SuggestionCaseTest extends AbstractTest {

	@Autowired
	private SuggestionService	suggestionService;
	@Autowired
	private TreasurerService	treasurerService;
	@Autowired
	private AnswerService		answerService;


	//Requisito Funcional: Gestionar el buz�n de quejas, en la que incluye listar
	/*
	 * En el siguiente orden comprueba:
	 * Se listan todas las quejas identificado como tesorero (correcto)
	 * Se listan todas las quejas identificado como otro actor (erroneo)
	 * Se listan todas las quejas sin identificar (erroneo)
	 */
	@Test
	public void driverList() {

		final Object testingData[][] = {
			{
				"treasurer", null
			}, {
				"associate1", IllegalArgumentException.class
			}, {
				null, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateList((String) testingData[i][0], (Class<?>) testingData[i][1]);

	}

	//Requisito Funcional: Gestionar el buz�n de quejas, en la que incluye eliminar
	/*
	 * En el siguiente orden comprueba:
	 * Se elimina una queja identificado como tesorero (correcto)
	 * Se elimina una queja inexistente identificado como tesorero (erroneo)
	 * Se elimina una queja identificado como otro actor (erroneo)
	 * Se elimina una queja identificado sin identificar (erroneo)
	 */
	@Test
	public void driverErase() {

		final Object testingData[][] = {
			{
				"treasurer", 80, null
			}, {
				"treasurer", 1, IllegalArgumentException.class
			}, {
				"associate1", 80, IllegalArgumentException.class
			}, {
				null, 79, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateErase((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}

	//Requisito Funcional: Responder a las sugerencias enviadas por socios de forma p�blica
	/*
	 * En el siguiente orden comprueba:
	 * Se responde una queja identificado como tesorero (correcto)
	 * Se responde una queja inexistente identificado como tesorero (erroneo)
	 * Se responde una queja identificado como otro actor (erroneo)
	 * Se responde una queja identificado sin identificar (erroneo)
	 */
	@Test
	public void driverResponse() {

		final Object testingData[][] = {
			{
				"treasurer", 81, "Hola", null
			}, {
				"treasurer", 1, "Hola", IllegalArgumentException.class
			}, {
				"associate1", 80, "Hola", NullPointerException.class
			}, {
				null, 79, "Hola", IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateResponse((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (Class<?>) testingData[i][3]);

	}

	protected void templateList(final String username, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			this.suggestionService.findNotAnswered();
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateErase(final String username, final Integer id, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			this.suggestionService.delete(this.suggestionService.findOne(id));
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	protected void templateResponse(final String username, final Integer id, final String text, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			final Treasurer t = this.treasurerService.findByPrincipal();
			final Integer total = t.getAnswers().size();
			final Answer a = this.answerService.create(this.suggestionService.findOne(id), t);
			a.setText(text);
			this.answerService.save(a);
			this.unauthenticate();
			if (username == "treasurer" && expected == IllegalArgumentException.class)
				try {
					Assert.isTrue(total < t.getAnswers().size());
				} catch (final Throwable oops) {
					caught = oops.getClass();
				}

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
}
