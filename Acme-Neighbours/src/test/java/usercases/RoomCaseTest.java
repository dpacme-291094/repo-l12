
package usercases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.RoomService;
import services.SecretaryService;
import utilities.AbstractTest;
import domain.Room;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class RoomCaseTest extends AbstractTest {

	@Autowired
	private RoomService			roomService;
	@Autowired
	private SecretaryService	secretaryService;


	/*
	 * Requisito funcional: Editar una room.
	 * Se comprueba en el siguiente orden:
	 * Un secretario edita una room (Correcto)
	 * Un presidente edita un room (Error)
	 * Null intenta editar un room (Error)
	 * Un secretario intenta editar una room null(Error)
	 */
	@Test
	public void driverEditRoom() {

		final Object testingData[][] = {

			{
				"secretary", 105, null
			}, {
				"president", 105, IllegalArgumentException.class
			}, {
				null, 105, IllegalArgumentException.class
			}, {
				"secretary", null, InvalidDataAccessApiUsageException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateEditRoom((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);

	}

	protected void templateEditRoom(final String username, final Integer roomId, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);
			Assert.notNull(this.secretaryService.findByPrincipal());
			final Room r = this.roomService.findOne(roomId);
			r.setTitle("Prueba");
			r.setDescription("prueba");
			this.roomService.save(r);
			this.roomService.flush();
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

}
