
package usercases;

import java.util.HashSet;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import security.Authority;
import security.UserAccount;
import services.SecretaryService;
import utilities.AbstractTest;
import domain.Message;
import domain.Secretary;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class SecretaryCaseTest extends AbstractTest {

	@Autowired
	private SecretaryService	secretaryService;


	/*
	 * Requisito funcional: Registro como un/a secretario
	 * Se comprueba en el siguiente orden:
	 * Registrarse con todos los datos correctos (Correcto)
	 * Registrarse con un username que ya existe (Error)
	 * Registrarse sin seguir el patron de telefono ni email (Error)
	 * Registrarse con un DNI inv�lido (Error)
	 * Registrarse sin coincidir las contrase�as (Erroneo)
	 * Registrar secretario identificado como tesorero ( Erroneo)
	 */

	@Test
	public void driverRegistrarSecretario() {

		final Object testingData[][] = {
			{
				"president", "secretary1", "123456", "123456", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "75893212A", null
			}, {
				"president", "secretary", "123456", "123456", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79029894Y", DataIntegrityViolationException.class
			}, {
				"president", "secretary1", "123456", "123456", "Juana", "Martinez", "jmartines.com", "123", "79029894Y", DataIntegrityViolationException.class
			}, {
				"president", "secretary1", "123456", "123456", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79YSA894", DataIntegrityViolationException.class
			}, {
				"president", "secretary10", "123456", "mal", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79029894Y", DataIntegrityViolationException.class
			}, {
				"treasurer", "secretary1", "123456", "123456", "Juana", "Martinez", "jmartines@hotmail.com", "954407447", "79029894Y", DataIntegrityViolationException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateRegister((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (String) testingData[i][8], (Class<?>) testingData[i][9]);
	}
	protected void templateRegister(final String username, final String user, final String password, final String repeatPassword, final String name, final String surname, final String email, final String phone, final String DNI, final Class<?> expected) {

		Class<?> caught;

		caught = null;

		try {
			this.authenticate(username);

			final Secretary s = this.secretaryService.create();

			final UserAccount ua = new UserAccount();
			ua.setUsername(user);
			ua.setPassword(password);
			final Authority aut = new Authority();
			aut.setAuthority("SECRETARY");
			ua.addAuthority(aut);
			s.setUserAccount(ua);
			Assert.isTrue(password == repeatPassword);
			s.setEmail(email);
			s.setName(name);
			s.setSurname(surname);
			s.setPhone(phone);
			s.setDNI(DNI);
			s.setMessagesIncoming(new HashSet<Message>());
			s.setMessagesOutgoing(new HashSet<Message>());

			this.secretaryService.save(s);
			this.secretaryService.flush();
			this.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

}
