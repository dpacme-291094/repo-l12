-- C queries
-- Media de peticiones a las actividades de ocio por socio
select avg(a.requests.size) from Associate a;

-- El socio que ha acudido a mas reuniones vs el que menos.
select r.associate,sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) from Request r  where r.activity in (select r2 from Reunion r2) group by r.associate order by sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) desc;
--De la query de arriba solo he pensado en esa forma, para coger el que mas tiene el la posicion 0 y el que menos tiene, la ultima posicion.


-- El socio que ha acudido a mas actividades de ocio vs el que menos.
select r.associate,sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) from Request r  where r.activity in (select l from Leisure l) group by r.associate order by sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) desc;
--Como la anterior.


-- La actividad de ocio que m�s asistentes ha tenido.
select l,sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) from Leisure l join l.requests r group by l order by sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) desc;

-- La actividad de ocio que menos asistentes ha tenido.
select l,sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) from Leisure l join l.requests r group by l order by sum( Case When r.status='ACCEPTED' Then 1 Else 0 End) asc;

--Las dos de arriba se puede hacer la misma tecnica que antes, cogiendo el primero y el ultimo, asi solo se deja una query.

-- El m�nimo, la media y el m�ximo n�mero de asistentes a las reuniones.
select min(r.requests.size),max(r.requests.size),avg(r.requests.size) from Reunion r join r.requests rr where rr.status='ACCEPTED';
-- Media de n�mero de comentarios por socio.
select avg(a.comments.size) from Associate a;
-- El m�nimo, la media y el m�ximo n�mero de mensajes enviados por actor.
select min(a.messagesOutgoing.size),max(a.messagesOutgoing.size),avg(a.messagesOutgoing.size) from Actor a;
-- El m�nimo, la media y el m�ximo n�mero de mensajes recibidos por actor.
select min(a.messagesIncoming.size),max(a.messagesIncoming.size),avg(a.messagesIncoming.size) from Actor a;

-- B queries

-- El voluntario que m�s cursos tiene relacionados.
select v, v.courses.size from Volunteer v where v.courses.size = (select max(v.courses.size) from Volunteer v);

-- El socio que mas salas ha reservado vs el que menos.
-- Ale dijo: Petitions con status accepted.
select a, p.size from Associate a Inner Join a.petitions p where p.status = 'ACCEPTED' AND p.size = 
(select max(p.size) from Associate a Inner join a.petitions p where p.status = 'ACCEPTED');

select a, p.size from Associate a Inner Join a.petitions p where p.status ='ACCEPTED'
AND p.size = (select min(p.size) from Associate a where p.status = 'ACCEPTED');

-- El ratio de solicitudes a salas aceptadas, pendientes y denegadas.
select sum( Case when p.status='ACCEPTED' then 1 Else 0 end)*1.0/ count(p)from Petition p;
select sum( Case when p.status='PENDING' then 1 Else 0 end)*1.0/ count(p)from Petition p;
select sum( Case when p.status='DENIED' then 1 Else 0 end)*1.0/ count(p)from Petition p;

-- El curso al que mas socios han acudido.
-- COGER EL PRIMER ELEMENTO CON JAVA
-- Ale dijo:  los cursos closed con m�s peticiones accepted
select a.title, sum(r.seats) From Request r Inner Join r.activity a 
WHERE TYPE(a) IN Course AND a.status='CLOSED' AND r.status='ACCEPTED' 
Group By r.activity ORDER BY sum(r.seats) DESC

-- El n�mero de actas en borrador VS actas finales.
select count(r) From Record r Where r.isFinal!=1;
select count(r) From Record r Where r.isFinal=1;

-- A queries

-- Cantidad total de dinero de los Leisures que la asociaci�n ha recaudado en el a�o actual vs la cantidad recaudada en el a�o pasado.
-- Ale dijo: Yo solo contar�a los CLOSED
select sum(l.price) * r.seats From Leisure l Inner Join l.requests r 
Where YEAR(l.moment) = YEAR(CURRENT_DATE) AND l.status = 'CLOSED';
select sum(l.price) * r.seats From Leisure l Inner Join l.requests r 
Where YEAR(l.moment) = YEAR(CURRENT_DATE)-1 AND l.status = 'CLOSED';

-- El m�nimo, la media y el m�ximo precio de las actividades de ocio.
select min(l.price), avg(l.price), max(l.price) From Leisure l;

-- La mayor cantidad que un socio se ha gastado en actividades de la asociaci�n.
-- COGER EL PRIMER ELEMENTO CON JAVA
-- Ale dijo: Yo solo contar�a los CLOSED
select r.associate.name, r.associate.surname, sum(l.price) * r.seats 
From Leisure l Inner Join l.requests r Where l.status = 'CLOSED' GROUP BY r.associate 
ORDER BY sum(l.price) * r.seats DESC

-- El n�mero de sugerencias respondidas recibidas vs no respondidas
-- Para saber el n�mero de suggestions respondidas basta hacer un count al answer
select count(a) From Answer a;
-- Para saber el n�mero de suggestions no respondidas, basta hacer un count de Suggestions que no est�n en Answer
select count(s) From Suggestion s Where s NOT IN (Select a.suggestion From Answer a);
